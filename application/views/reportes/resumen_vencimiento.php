<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Resumen de vencimiento</h1>
<form action="<?= base_url('reportes/resumen_vencimiento') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un proveedor</label>
        <?= form_dropdown_from_query('proveedor','proveedores','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>  
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['proveedor']))$proveedor = $this->db->get_where('proveedores',array('id'=>$_POST['proveedor']))->row()->denominacion; ?>
    <h1 align="center"> Resumen de ventas</h1>
    <p><strong>Proveedor: </strong> <?= empty($_POST['proveedor'])?'Todos':$proveedor ?></p>
    <p><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?></p>
    
    <table border="0" cellspacing="18" class="table" width="100%" style="font-size:12px;">
        <thead>
                <tr>
                        <th>Fecha</th>
                        <th>Nro. Factura</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio Costo</th>
                        <th>Total</th>
                </tr>
        </thead>
        <tbody>
            <?php
                $_POST['desde'] = !empty($_POST['desde'])?date("Y-m-d",strtotime(str_replace("/","-",$_POST['desde']))):'';
                $_POST['hasta'] = !empty($_POST['hasta'])?date("Y-m-d",strtotime(str_replace("/","-",$_POST['hasta']))):'';
                if(!empty($_POST['proveedor']))$this->db->where('compras.proveedor',$_POST['proveedor']);
                if(!empty($_POST['desde']))$this->db->where('compras.fecha >=',$_POST['desde']);
                if(!empty($_POST['hasta']))$this->db->where('compras.fecha <=',$_POST['hasta']);
                $this->db->where('status',0);
                $total = 0;
                $this->db->select('compras.fecha,compras.nro_factura,compradetalles.*, productos.nombre_comercial as prod');
                $this->db->join('productos','productos.codigo = compradetalles.producto');
                $this->db->join('compras','compradetalles.compra = compras.id');
                $this->db->order_by('compras.fecha','ASC');
                $compras = $this->db->get('compradetalles');
            ?>
            <?php foreach($compras->result() as $c): ?>
                <tr>
                        <td><?= date("d/m/Y",strtotime($c->fecha)) ?></td>
                        <td><?= $c->nro_factura ?></td>
                        <td><?= $c->prod ?></td>
                        <td align="right"><?= $c->cantidad ?></td>
                        <td align="right"><?= number_format($c->precio_costo,0,',','.') ?> </td>
                        <td align="right"><?= number_format($c->total,0,',','.') ?> </td>
                        <? $total+= $c->total; ?>
                </tr>
            <?php endforeach ?>
                <tr>
                    <th colspan="5">Total Compra</th>
                    <th align="right"><?= number_format($total,0,',','.') ?></th>
                </tr>
        </tbody>
    </table>
<?php endif; ?>