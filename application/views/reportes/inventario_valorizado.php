<?php if(empty($_POST)): ?>
<div class="container">
    <h1 align="center"> Inventario Valorizado</h1>
<form action="<?= base_url('reportes/inventario_valorizado') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <h1 align="center"> Listado de inventario</h1>
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?></p>
    
    <table class="table" width="100%" style="font-size:12px;">
        <thead>
                <tr>
                        <th style="width:100px">Código</th>
                        <th style="width:350px">Nombre</th>
                        <th style="width:100px">Precio Venta</th>
                        <th style="width:100px">Cantidad</th>
                        <th style="width:100px">Total</th>
                </tr>
        </thead>
        <tbody>    
            <?php $total = 0; ?>
            <?php foreach($data->result() as $c): ?>
                <tr>                        
                        <td><?= $c->producto ?></td>
                        <td><?= $c->nombre_comercial ?></td>
                        <td><?= number_format($c->precio_venta,0,',','.') ?></td>
                        <td><?= $c->cantidad ?></td>
                        <td><?= number_format($c->total,0,',','.') ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php endif; ?>