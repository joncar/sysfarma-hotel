<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('admin.php');

class Cajero extends Admin {

    function __construct() {
        parent::__construct();
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=cajero/' . $this->router->fetch_method()));
        elseif (empty($_SESSION['caja']))
            header("Location:" . base_url('panel/selcaja?redirect=cajero/' . $this->router->fetch_method()));
    }

    function index($url = 'main', $page = 0) {
        $this->loadView('panel');
    }

    public function cajadiaria($x = '', $y = '') {
        $crud = parent::cajadiaria($x, $y);
        $crud->field_type('sucursal', 'hidden', $_SESSION['sucursal'])
                ->field_type('caja', 'hidden', $_SESSION['caja'])
                ->field_type('usuario', 'hidden', $_SESSION['user']);
        $crud->where('caja', $_SESSION['caja']);
        $crud->order_by('correlativo', 'desc');
        $crud->unset_columns('sucursal', 'caja', 'usuario');
        $this->loadView($crud->render());
    }

    public function ventas($x = '', $y = '') {
        if ($this->db->get_where('cajas', array('id' => $_SESSION['caja'], 'abierto' => 1))->num_rows == 0) {
            header("Location:" . base_url($this->router->fetch_class() . '/cajadiaria'));
        } else {
            $crud = parent::ventas($x, $y);
            if ($x != 'imprimir' && $x != 'imprimirticket') {
                $this->loadView($crud);
            } else {
                echo $crud;
            }
        }
    }

    public function productosucursal($x = '', $y = '') {
        $crud = parent::productosucursal($x, $y);
        $this->loadView($crud->render());
    }

    public function clientes($x = '', $y = '') {
        $crud = parent::clientes($x, $y);
        $this->loadView($crud->render());
    }

    public function transferencias($x = '', $y = '') {
        $crud = parent::transferencias($x, $y);
        $this->loadView($crud);
    }

    public function productos($x = '', $y = '') {
        $crud = parent::productos($x, $y);
        $crud->unset_add()
                ->unset_edit()
                ->unset_delete()
                ->unset_export()
                ->unset_read();
        $this->loadView($crud->render());
    }
    
    public function salidas($x = '', $y = '') {
        $crud = parent::salidas($x, $y);
        $this->loadView($crud);
    }

    public function saldos($x = '', $y = '') {
        $crud = parent::saldos($x, $y);
        $this->loadView($crud);
    }

    public function pagocliente($x = '', $y = '') {
        if ($this->db->get_where('cajas', array('id' => $_SESSION['caja'], 'abierto' => 1))->num_rows == 0) {
            header("Location:" . base_url($this->router->fetch_class() . '/cajadiaria'));
        } else {
            $crud = parent::pagocliente($x, $y);
            if ($x != 'imprimir' && $x != 'imprimirticket') {
                $this->loadView($crud);
            } else {
                echo $crud;
            }
        }
    }

    public function abonarDeuda($x = '') {
        if (!empty($x) && is_numeric($x)) {
            $this->db->select('ventas.*, clientes.nombres as clientename, clientes.apellidos as apellidos');
            $this->db->join('clientes', 'clientes.id = ventas.cliente');
            $cliente = $this->db->get_where('ventas', array('ventas.id' => $x));
            if ($cliente->num_rows > 0) {
                $nombre = $cliente->row()->clientename . ' ' . $cliente->row()->apellidos;
                $cliente = $cliente->row()->cliente;
                //Total venta
                $this->db->select('SUM(ventas.total_venta) as total_venta');
                $this->db->where('transaccion', '2');
                $total_venta = $this->db->get_where('ventas', array('ventas.cliente' => $cliente));
                $totalVentas = $total_venta->row()->total_venta == null ? 0 : $total_venta->row()->total_venta;
                //Total pagos
                $this->db->select('SUM(pagocliente.totalpagado) as total_pagos');
                $total_pago = $this->db->get_where('pagocliente', array('pagocliente.cliente' => $cliente));
                $totalPagos = $total_pago->num_rows == 0 ? 0 : $total_pago->row()->total_pagos;
                $saldo = $totalVentas - $totalPagos;
                //saldo notas de credito
                $this->db->select('SUM(notas_credito_cliente.total_monto) as notas');
                $this->db->join('ventas', 'ventas.id = notas_credito_cliente.venta');
                $total_nota = $this->db->get_where('notas_credito_cliente', array('ventas.cliente' => $cliente));
                $notas = $total_nota->num_rows == 0 ? 0 : $total_nota->row()->notas;
                //Se resta a las notas de credito las facturas a las cuales se les ha pagado con notas de credito
                $this->db->select('SUM(totalnotacredito) as notasusadas');
                $notasusadas = $this->db->get_where('pagocliente', array('cliente' => $cliente));
                $notasusadas = $notasusadas->num_rows == 0 ? 0 : $notasusadas->row()->notasusadas;
                $notas = $notas - $notasusadas;
                $msj = '';
                if (!empty($_POST)) {
                    $this->form_validation->set_rules('monto', 'Monto', 'required|numeric|greather_than[1]');
                    if ($this->form_validation->run()) {
                        //Traemos saldos
                        if ($saldo < $notas + $_POST['monto']) {
                            $msj = $this->error('El monto ingresado excede de la cantidad total de la deuda');
                        } else {
                            $detalles = array();
                            $notasdisponible = $notas;
                            $saldodisponible = $_POST['monto'];
                            //Traemos las facturas que se deben
                            $facturas = $this->db->get_where('ventas', array('ventas.cliente' => $cliente, 'transaccion' => 2));
                            foreach ($facturas->result() as $factura) {
                                //Cuanto se debe de esta factura
                                $this->db->order_by('id', 'DESC');
                                $deuda = $this->db->get_where('pagoclientesfactura', array('venta' => $factura->id));
                                if ($deuda->num_rows > 0) {
                                    $deuda = $deuda->row()->monto - ($deuda->row()->total + $deuda->row()->nota_credito);
                                } else {
                                    $deuda = $factura->total_venta;
                                }

                                if ($deuda > 0 && ($saldodisponible > 0 || $notasdisponible > 0)) {
                                    $pago = array('venta' => $factura->id, 'nro_factura' => $factura->nro_factura, 'monto' => $deuda);
                                    if ($notasdisponible > 0) {
                                        $pago['nota_credito'] = $notasdisponible > $deuda ? $deuda : $notasdisponible;
                                        $notasdisponible-=$deuda;
                                        $deuda -= $pago['nota_credito'];
                                    } else {
                                        $pago['nota_credito'] = 0;
                                    }
                                    if ($saldodisponible > 0) {
                                        $pago['total'] = $saldodisponible > $deuda ? $deuda : $saldodisponible;
                                        $saldodisponible-= $pago['total'];
                                    }
                                    array_push($detalles, $pago);
                                }
                            }
                            //Se carga la cabecera del pago
                            $this->db->insert('pagocliente', array(
                                'cliente' => $cliente,
                                'fecha' => date("Y-m-d H:i:s"),
                                'totalefectivo' => $_POST['monto'],
                                'totalnotacredito' => $notas,
                                'totalpagado' => $notas + $_POST['monto'],
                                'sucursal' => $_SESSION['sucursal'],
                                'caja' => $_SESSION['caja'],
                                'user' => $_SESSION['user']
                            ));
                            $idpago = $this->db->insert_id();
                            foreach ($detalles as $d) {
                                $d['pagocliente'] = $idpago;
                                $this->db->insert('pagoclientesfactura', $d);
                            }
                            $msj = $this->success('El pago se ha realizado satisfactoriamente <a href="javascript:imprimir(' . $idpago . ')">Imprimir</a> | <a href="javascript:imprimirTicket(' . $idpago . ')">imprimir Ticket</a>');
                        }
                    }
                }


                $this->loadView(array('view' => 'abonarDeuda', 'msj' => $msj, 'totalPagos' => $totalPagos, 'cliente' => $nombre, 'totalVentas' => $totalVentas, 'saldo' => $saldo, 'notas' => $notas));
            } else {
                header("Location:" . base_url('cajero/saldos'));
            }
        } else {
            header("Location:" . base_url('cajero/saldos'));
        }
    }

    /* Cruds */
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
