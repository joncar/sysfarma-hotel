<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Ventas por sucursal</h1>
<form action="<?= base_url('reportes/ventas_sucursal') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>  
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>   
    <h1 align="center">Ventas por sucursal</h1>
    <p><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?></p>
    
    <table border="0" cellspacing="18" class="table" width="100%">
        <thead>
                <tr>
                        <th>Sucursal</th>
                        <th>Tarjeta de credito</th>
                        <th>Nota de credito</th>
                        <th>Debito</th>
                        <th>Cheque</th>
                        <th>Efectivo</th>
                        <th>Total</th>
                </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
                    $this->db->where('Date(ventas.fecha) between \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde']))).'\' AND \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta']))).'\'',null,TRUE);
                }
                $this->db->group_by('ventas.sucursal');
                $this->db->select('sucursales.*, SUM(ventas.total_efectivo) as efectivo, SUM(ventas.total_debito) as debito, SUM(ventas.total_cheque) as cheque, SUM(ventas.total_credito) as credito');
                $this->db->where('status',0);
                $this->db->join('ventas','ventas.sucursal = sucursales.id','inner');
                $ventas = $this->db->get('sucursales');
            ?>
            <?php $total_credito = 0; $total_debito = 0; $total_cheque = 0; $total_efectivo = 0; $total = 0; ?>
            <?php foreach($ventas->result() as $c): ?>
                <tr>
                        <td><?= $c->denominacion ?></td>
                        <td align="right"><?= number_format($c->credito,0,",",".") ?></td>
                        <td align="right">0</td>
                        <td align="right"><?= number_format($c->debito,0,",",".") ?></td>
                        <td align="right"><?= number_format($c->cheque,0,",",".") ?></td>
                        <td align="right"><?= number_format($c->efectivo,0,",",".") ?></td>
                        <td align="right"><?= number_format($c->credito+$c->debito+$c->cheque+$c->efectivo,0,",",".") ?></td>
                        <?php 
                            $total_credito+= $c->credito;
                            $total_debito+= $c->debito;
                            $total_cheque+= $c->cheque;
                            $total_efectivo+= $c->efectivo;
                            $total+= $c->credito+$c->debito+$c->cheque+$c->efectivo;
                        ?>
                </tr>
            <?php endforeach ?>
                <tr>
                        <th>Total</th>
                        <th align="right"><?= number_format($total_credito,0,",",".") ?></th>
                        <th align="right">0</th>
                        <th align="right"><?= number_format($total_debito,0,",",".") ?></th>
                        <th align="right"><?= number_format($total_cheque,0,",",".") ?></th>
                        <th align="right"><?= number_format($total_efectivo,0,",",".") ?></th>
                        <th align="right"><?= number_format($total,0,",",".") ?></th>
                </tr>
        </tbody>
    </table>
<?php endif; ?>