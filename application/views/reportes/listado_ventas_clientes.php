<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Listado de ventas por cliente</h1>
<form action="<?= base_url('reportes/listado_ventas_clientes') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un cliente</label>
        <?php $this->db->order_by('apellidos','asc'); ?>
        <?= form_dropdown_from_query('clientes','clientes','id','apellidos nombres nro_documento',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <h1 align="center"> Listado de ventas por cliente</h1>
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?></p>    
    
    <table border="0" cellspacing="18" class="table" style='width:100%'>
        <thead>
                <tr>
                    <th style="width:100px">Fecha Pago</th>
                    <th style="width:100px">Hora</th>
                    <th style="width:200px">Cliente</th>
                    <th style="width:100px">Cédula</th>
                    <th style="width:100px">Monto Pago</th>                        
                </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
                    $this->db->where('DATE(ventas.fecha) between \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde']))).'\' AND \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta']))).'\'',null,TRUE);
                }
                
                if(!empty($_POST['sucursal']))$this->db->where('ventas.sucursal',$_POST['sucursal']);
                $this->db->where('ventas.status > ',-1);
                $this->db->order_by('fecha','ASC');
                $total = 0;
                $this->db->select('ventas.fecha, clientes.nombres, clientes.apellidos, clientes.nro_documento, ventas.total_venta as total');
                $this->db->join('clientes','clientes.id = ventas.cliente');
                $this->db->where('ventas.status !=',-1);
                $ventas = $this->db->get('ventas');                
            ?>
            <?php foreach($ventas->result() as $c): ?>
                <tr>                        
                        <td><?= date("d-m-Y",strtotime($c->fecha)) ?></td>
                        <td><?= date("H:i:s",strtotime($c->fecha)) ?></td>
                        <td><?= $c->apellidos.' '.$c->nombres ?></td>
                        <td><?= $c->nro_documento ?></td>
                        <td><?= number_format($c->total,0,',','.') ?></td>
                        <?php $total+= $c->total ?>
                </tr>
            <?php flush(); ?>
            <?php endforeach ?>
            <tr>
                    <td colspan="4" align="right" style="font-weight:bold;">Total Pago</td>
                    <td style="font-weight:bold;"><?= number_format($total,0,',','.') ?></td>
            </tr>
        </tbody>
    </table>
<?php endif; ?>