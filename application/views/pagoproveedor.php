<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha" value="<?= empty($pagoproveedor)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime($pagoproveedor->fecha)) ?>" id="fecha" class="datetime-input form-control">
              </div>
            </div>
        </div> 
        <div class="col-xs-4">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">FormaPago: </label>
              <div class="col-sm-8" id="cliente_div">  
                  <? $sel = empty($pagoproveedor)?0:$pagoproveedor->forma_pago; ?>
                  <?= form_dropdown('forma_pago',array('Cheque','Transferencia','Deposito'),$sel,'id="formapago" class="form-control"') ?>
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Proveedor: </label>
              <div class="col-sm-8" id="cliente_div">
                  <?php $selected = empty($pagoproveedor)?0:$pagoproveedor->proveedor ?>
                    <?= form_dropdown_from_query('proveedor','proveedores','id','denominacion',$selected,'id="proveedor"',TRUE); ?>
              </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Nro. Recibo: </label>
              <div class="col-sm-8" id="cliente_div">                  
                  <input type="text" name="nro_recibo" value="<?= empty($pagoproveedor)?'':$pagoproveedor->nro_recibo ?>" id="nro_recibo" class="form-control">
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Comprobante: </label>
              <div class="col-sm-8" id="cliente_div">
                  <input type="text" name="comprobante" value="<?= empty($pagoproveedor)?'':$pagoproveedor->comprobante ?>" id="comprobante" class="form-control">
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Total Abonado: </label>
              <div class="col-sm-8" id="cliente_div">
                  <input type="text" name="total_abonado" value="<?= empty($pagoproveedor)?'0':$pagoproveedor->total_abonado ?>" id="total_abonado" class="form-control">
              </div>
            </div>
        </div>
    </div>
    </div>
    <div class="row">        
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2">
                <thead>
                    <tr>
                        <th style="width:20%">Nro. Factura</th>
                        <th style="width:20%">Fecha Factura</th>
                        <th style="width:20%">MontoFactura</th>
                        <th style="width:20%">MontoNotaCredito</th>
                        <th style="width:20%">TotalaPagar</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($detalles)): foreach($detalles->result() as $d): ?>
                    <tr>
                        <td>
                           <?= form_dropdown_from_query('compra[]','compras','id','nro_factura',$d->compra,'readonly',FALSE,'compra'); ?> 
                        </td>
                        <td><input name="fecha_factura[]" type="text" value='<?= date("d/m/Y",strtotime($d->fecha_factura)) ?>'  class="form-control fecha_factura" readonly placeholder="Fecha factura"></td>
                        <td><input name="monto_factura[]" type="text" value='<?= $d->monto_factura ?>'  class="form-control monto_factura" readonly placeholder="Monto Factura"></td>
                        <td><input name="monto_nota_credito[]" type="text" value='<?= $d->monto_nota_credito ?>'  class="form-control monto_nota_credito" placeholder="Monto Nota de credito"></td>                        
                        <td><input name='total[]' type="text" value='<?= $d->total ?>'  class="form-control total" placeholder="Total"></td>                                                
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                    <?php  endforeach; endif ?>
                    <tr>
                        <td>
                           <?= form_dropdown_from_query('compra[]',$this->db->get_where('compras',array('status'=>0)),'id','nro_factura','','',TRUE,'compra'); ?> 
                        </td>
                        <td><input name="fecha_factura[]" type="text" class="form-control fecha_factura" readonly placeholder="Fecha factura"></td>
                        <td><input name="monto_factura[]" type="text" class="form-control monto_factura" readonly placeholder="Monto Factura"></td>
                        <td><input name="monto_nota_credito[]" type="text" class="form-control monto_nota_credito" placeholder="Monto Nota de credito"></td>                        
                        <td><input name='total[]' type="text" value=''  class="form-control total" placeholder="Total"></td>
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>    
    <div class="row" style="margin-top:40px">                
        <button id="guardar" type="submit" class="btn btn-success">Guardar Entrada</button>
        <a href="#" class="btn btn-default">Imprimir Reporte</a>
    </div>
</form>
<script>
    
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');
            x = $("tbody .chosen-select").parents('td').find('.chzn-container');
            w = x.css('width')
            x.remove();
            $("tbody .chosen-select").removeAttr('id').removeClass('chzn-done');
            $("tbody .chosen-select").chosen({allow_single_deselect:true});
            $("tbody .chzn-container, tbody .chzn-drop").css('width',w);
            $("tbody .chzn-search input").css('width','238px');          
            date_init_calendar();
    }
    function removerow(obj){
        $(obj).parent('td').parent('tr').remove();
        $(document).trigger('total');
    }
    
    //Eventos
    $(document).on('keydown','input',function(event){        
            
        if (event.which == 13){
            if($(this).hasClass('total')){
                addrow($(this));
            }
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } 
            else if($(this).val()!='' && $(this).hasClass('codigo')){
                $(this).trigger('change');                
            }
            else if($(this).val()=='' && $(this).hasClass('codigo')){
                return false;
            }
            else
            {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });
    
    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    })

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    }); 
    
   $("body").on('change','.compra',function(){
       if($(this).val()!=''){
        focused = $(this);
        elements = $(".compra").length;
        focusedcode = $(this).val();        
        $.post('<?= base_url('json/getCompra') ?>',{codigo:$(this).val()},function(data){
            data = JSON.parse(data);
            data = data['producto'];
            if(data!=''){
            focused = focused.parent().parent();
            focused.find('.fecha_factura').val(data['fecha'])
            focused.find('.monto_factura').val(data['total_compra']);                    
            focused.find('.monto_nota_credito').val(0);  
            focused.find('.total').val(data['total_compra']);              
            $(document).trigger('total');
            addrow(focused.find('a'));
            $("tbody tr").last().find('.compra').focus();
            }
            else{
                emergente('El producto ingresado no se encuentra registrado. <a target="_new" href="<?= base_url($this->router->fetch_class().'/productos/add/json') ?>/'+focused.val()+'">¿Desea registrar uno nuevo?</a>');                        
            }                                        
        });        
        }
    });
    
    $("body").on('change','#proveedor',function(){
        $.post('<?= base_url('json/getCompra') ?>',{codigo:'N/A',proveedor:$(this).val()},function(data){
            producto = JSON.parse(data);
            str = '<option value="">Todos</option>';            
            for(i in producto){
                str += '<option value="'+producto[i].id+'">'+producto[i].nro_factura+'</option>';
            }            
            $(".compra").html(str);            
            x = $("tbody .chosen-select").parents('td').find('.chzn-container');
            w = x.css('width')
            x.remove();
            $("tbody .chosen-select").removeAttr('id').removeClass('chzn-done');
            $("tbody .chosen-select").chosen({allow_single_deselect:true});
            $("tbody .chzn-container, tbody .chzn-drop").css('width',w);
            $("tbody .chzn-search input").css('width','238px');
        });
    })
    
    $(document).on('total',function(){      
        $("#total_abonado").val(0);        
        $("tbody tr").each(function(){
            self = $(this);            
            total = parseInt(self.find('.total').val());                                    
            if(!isNaN(total)){
                self.find('.total').val(total);
                /*Total Venta*/
                total_venta = parseInt($("#total_abonado").val());            
                total_venta += total;
                $("#total_abonado").val(total_venta);                
            }
        });
    });
    
    function val_send(form){
         $(".mask").show();
        var data = document.getElementById('formulario');
        $(document).trigger('total')
        data = new FormData(data);
        $("#guardar").attr('disabled','disabled');
        $.ajax({
            url:'<?= empty($pagoproveedor)?base_url('json/pagosproveedor'):base_url('json/pagosproveedor/edit') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);
                if(data['status']){
                    $(".alert").removeClass('alert-danger').addClass('alert-success').html('Se han guardado los datos con exito').show();
                    document.location.reload();
                }
                else{
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
                    $(".mask").hide();
                }
                $("#guardar").removeAttr('disabled');
            }
            });
            return false;
    }
   
</script>
<?php $this->load->view('predesign/datepicker') ?>
<?= $this->load->view('predesign/chosen.php') ?>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>