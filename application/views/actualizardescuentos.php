<?php if(!empty($msj)): ?>
    <?= $msj ?>
<?php endif ?>
<form class="form-horizontal" method="post" id='formulario' onsubmit="return actualizar(this)" role="form">
    <div class="well">
        <div class="row">
            
            <div class="col-xs-12" style="text-align: center">
                <p><b>Actualizar descuento de los siguientes productos</b></p>
            </div>            
            
            <div class="col-xs-12">
                <div class="form-group">
                  <label for="proveedor" class="col-sm-4 control-label">Proveedor</label>
                  <div class="col-sm-8">
                      <?php $this->db->order_by('denominacion','ASC'); echo form_dropdown_from_query('proveedores','proveedores','id','denominacion') ?>
                  </div>
                </div>
            </div>
            
            <div class="col-xs-12">
                <div class="form-group">
                  <label for="proveedor" class="col-sm-4 control-label">Categoría</label>
                  <div class="col-sm-8">
                      <?php $this->db->order_by('denominacion','ASC'); echo form_dropdown_from_query('categoriaproducto','categoriaproducto','id','denominacion') ?>
                  </div>
                </div>
            </div>
            
            <div class="col-xs-12">
                <div class="form-group">
                  <label for="proveedor" class="col-sm-4 control-label">Nacionalidad</label>
                  <div class="col-sm-8">
                        <?php $this->db->order_by('denominacion','ASC'); echo form_dropdown_from_query('paises','paises','id','denominacion') ?>
                  </div>
                </div>
            </div>
            
            <div class="col-xs-12">
                <div class="form-group">
                  <label for="proveedor" class="col-sm-4 control-label">Descuento Máximo</label>
                  <div class="col-sm-8">
                      <input type="number" name="porcentaje" placeholder="%" class="form-control">
                  </div>
                </div>
            </div>
            
            <div class="col-xs-12" style="text-align: center">
                <button type="submit" class="btn btn-success">Actualizar productos</button>
            </div>
            
        </div>
    </div>
</form>
<script>
    function actualizar(){
        return confirm("Estas seguro que deseas actualizar estos productos?. Esta acción no tiene reversa");
    }
</script>