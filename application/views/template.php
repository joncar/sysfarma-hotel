<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?= empty($title)?'SysHotel':$title ?></title>
        <meta charset="UTF-8">	
	<!-- Website's title ( it will be shown in browser's tab ), Change the website's title based on your Hotel information -->
	<meta name="description" content="Colosseum Hotel is a responsive Hotel and Resort HTML template.">
	<!-- Add your Hotel short description -->
	<meta name="keywords" content="Responsive,HTML5,CSS3,XML,JavaScript">
	<!-- Add some Keywords based on your Hotel and your business and separate them by comma -->
	<meta name="author" content="Joseph a, ravistheme@gmail.com">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic%7cPlayfair+Display:400,700%7cGreat+Vibes' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?= base_url('css/template') ?>/styles.css"><!-- Attach the main stylesheet file -->
        <?php if(!empty($myscripts)) echo $myscripts ?>
    </head>

    <body class="<?= empty($bodyclass)?'home-page-2':$bodyclass ?>">
        <div class="main-wrapper">
            <?php $this->load->view('includes/template/header'); ?>
            <?php $this->load->view($view); ?>
        </div>
        <?php $this->load->view('includes/template/scripts'); ?>
    </body>

</html>
