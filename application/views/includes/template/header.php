
<!-- Header Section -->
<header id="main-header">
    <div class="inner-container container">
        <div class="l-sec col-xs-8 col-sm-6 col-md-3">
            <a href="<?= site_url() ?>" id="t-logo">
<!--                <span class="title">cal fuster</span>-->
<!--                <span class="desc">de la Plaça</span>-->
                <div class="r-sec col-md-5">
                    <img src="<?= base_url() ?>img/logo.png" style=" margin-top: 7px; width: 207px" alt="cal fuster de la plaça">
                </div>
            </a>
        </div>
        <div class="r-sec col-xs-4 col-sm-6 col-md-9">
            <nav id="main-menu">
                <ul class="list-inline">
                    <li class="active"><a href="<?= site_url() ?>">Home</a>
                      <!--  <ul>
                            <li><a href="#">Home Page - Version 1</a></li>
                            <li class="active"><a href="index-1.html">Inici</a></li>
                        </ul>-->
                    </li>
                    <li><a href="<?= site_url('p/nosaltres') ?>">Qui som</a>
                        <ul>
                            <li><a href="<?= site_url('p/nosaltres') ?>">Nosaltres</a></li>
                            <li><a href="<?= site_url('p/serveis') ?>">Serveis</a></li>
                        </ul>
                    </li>
                    <li><a href="<?= site_url('habitacions') ?>">Habitacions</a>
                        <ul>
                            <li><a href="<?= site_url('habitacions') ?>">Totes les habitacions</a></li><br>
                            <?php foreach($this->db->get_where('habitaciones',array('disponible'=>1))->result() as $h): ?>
                            <li><a href="<?= site_url('habitacion/'. toURL($h->id.'-'.$h->habitacion_nombre)) ?>"><?= $h->habitacion_nombre ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </li>
                    <li><a href="<?= site_url('p/events') ?>">Events</a>
<!--                        <ul>-->
<!--                            <li><a href="pages/events.html">Event Archive</a></li>-->
<!--                            <li><a href="pages/event-details.html">Event Details</a></li>-->
<!--                        </ul>-->
                    </li>
                    <li><a href="<?= site_url('p/galeria') ?>">Galeria</a>
                       <!-- <ul>
                            <li><a href="pages/gallery-grid.html">Gallery - Grid View</a></li>
                            <li><a href="pages/gallery-masonry.html">Gallery - Masonry View</a></li>
                            <li><a href="pages/gallery-row.html">Gallery - List View</a></li>
                            <li><a href="pages/gallery-slide-show.html">Gallery - Slide Show</a></li>
                        </ul>-->
                    </li>
                  <!--  <li><a href="#">Booking</a>
                        <ul>
                            <li><a href="pages/booking.html">Choose Date</a></li>
                            <li><a href="pages/booking-1.html">Choose Room</a></li>
                            <li><a href="pages/booking-2.html">Make a Reservation</a></li>
                            <li><a href="pages/booking-3.html">Confirmation</a></li>
                        </ul>
                    </li>-->
                    <li>
                        <a href="<?= site_url('blog') ?>">Notícies</a>
                        <!--<ul>
                            <?php $this->db->order_by('id','DESC'); ?>
                            <?php $this->db->limit(4); ?>
                            <?php foreach($this->db->get_where('blog')->result() as $h): ?>
                            <li><a href="<?= site_url('blog/'. toURL($h->id.'-'.$h->titulo)) ?>"><?= cortar_palabras($h->titulo,3) ?></a></li>
                            <?php endforeach ?>
                        </ul>-->
                    </li>
                    <!--<li><a href="#">Pages</a>
                        <ul>
                            <li><a href="pages/guest-book.html">Guest Book</a></li>
                            <li><a href="pages/restaurant.html">Restaurant</a></li>
                            <li><a href="pages/video-tour.html">Video Tour</a></li>
                            <li><a href="pages/staff.html">Our Staff</a></li>
                            <li><a href="pages/packages.html">Packages</a></li>
                            <li><a href="pages/404.html">404</a></li>
                            <li><a href="pages/coming-soon.html">Coming Soon</a></li>
                        </ul>
                    </li>-->
                    <li><a href="<?= site_url('p/contacte') ?>">Contacte</a></li>
                    <?php if(!empty($_SESSION['user'])): ?>
                    <li><a href="<?= base_url('main/unlog') ?>">Salir</a></li>
                    <?php endif ?>
                    <li>
                        <a href="#">
                            <i class="fa fa-globe" style="color: white; font:normal normal normal 18px/1 FontAwesome"></i>
                        </a>
                        <ul>
                            <li><a href="<?= site_url('main/traduccion/ca') ?>">Català</a></li>
                            <li><a href="<?= site_url('main/traduccion/es') ?>">Castellano</a></li>
                            <li><a href="<?= site_url('main/traduccion/en') ?>">English</a></li>

                        </ul>
                    </li>
                </ul>
            </nav>
            <div id="main-menu-handle" class="ravis-btn btn-type-2"><i class="fa fa-bars"></i><i class="fa fa-close"></i></div><!-- Mobile Menu handle -->
            <a href="<?= site_url('iniciar-reserva') ?>" id="header-book-bow" class="ravis-btn btn-type-2"><span>Reserva</span> <i class="fa fa-calendar"></i></a>
        </div>
    </div>
    <div id="mobile-menu-container"></div>
</header>
<!-- End of Header Section -->
