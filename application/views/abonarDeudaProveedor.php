<?php if(!empty($msj)): ?>
<?= $msj ?>
<?php endif ?>
<form class="form-horizontal" id='formulario' method="post" action="" role="form">
    <div class="row well">
        <div class="col-xs-6">
            Proveedor: <b><?= $cliente ?></b>
        </div>
        <div class="col-xs-6" align="right">
            Fecha: <b><?= date("d-m-Y H:i:s") ?></b>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4">
            <div align="right">
                Saldo del proveedor: <span style="font-size:29px"><b><?= number_format($saldo,2,',','.') ?> Gs</b></span>
            </div>
            <?php if($saldo>0): ?>
            <div class="form-group">
                <label for="monto" class="col-sm-4 control-label">Monto</label>
                <div class="col-sm-8">
                    <input type="number" name="monto" id="monto" class="form-control" placeholder="Monto a abonar" required="" min="1">
                </div>
            </div>
            
            <div class="form-group">
                <label for="monto" class="col-sm-4 control-label">Forma de Pago</label>
                <div class="col-sm-8">
                    <input type="text" name="forma_pago" id="forma_pago" class="form-control" placeholder="Forma de Pago" required="" min="1">
                </div>
            </div>
            
            <div class="form-group">
                <label for="monto" class="col-sm-4 control-label">Comprobante</label>
                <div class="col-sm-8">
                    <input type="text" name="comprobante" id="comprobante" class="form-control" placeholder="Comprobante" required="" min="1">
                </div>
            </div>
            
            <div class="form-group">
                <label for="monto" class="col-sm-4 control-label">#Recibo</label>
                <div class="col-sm-8">
                    <input type="text" name="nro_recibo" id="nro_recibo" class="form-control" placeholder="#Recibo" required="" min="1">
                </div>
            </div>
            
            <div align="center">
                <button type="submit" class="btn btn-success">Abonar monto</button>
            </div>
            <?php else: ?>
            Este proveedor no posee deudas pendientes
            <?php endif ?>
        </div>
    </div>
</form>
<script>
    function imprimir(codigo){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimir/'+codigo);
     }
      function imprimirTicket(codigo){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimirticket/'+codigo);
     }
</script>