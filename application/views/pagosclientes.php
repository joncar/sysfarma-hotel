<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">
        <div class="col-xs-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Cliente: </label>
              <div class="col-sm-8" id="cliente_div">
                  <? $sel = empty($pagocliente)?0:$pagocliente->cliente; ?>
                  <?= form_dropdown_from_query('cliente','clientes','id','nro_documento nombres apellidos',$sel,'id="cliente"') ?>
              </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Nro de recibo: </label>
              <div class="col-sm-8" id="cliente_div">
                  <input type="text" name="recibo" value="<?= empty($pagocliente)?0:$pagocliente->recibo ?>" id="recibo" class="form-control" >
              </div>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha" value="<?= empty($pagocliente)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime($pagocliente->fecha)) ?>" id="fecha" class="datetime-input form-control">
              </div>
            </div>
        </div>        
    </div>     
    </div>
    <div class="row">        
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2">
                <thead>
                    <tr>
                        <th style="width:20%">Venta</th>
                        <th style="width:20%">Nro Factura</th>
                        <th style="width:20%">Monto</th>
                        <th style="width:20%">Nota de credito</th>                        
                        <th style="width:20%">Total</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody> 
                    <?php if(!empty($pagocliente)): ?>
                        <?php foreach($detalles->result() as $d): ?>
                            <tr>
                                <td><? $sel = empty($d)?0:$d->venta; ?>
                                    <?= form_dropdown_from_query('venta[]',$this->db->get_where('ventas',array('transaccion'=>2)),'id','nro_factura',$sel,'',TRUE,'venta'); ?>
                                </td>
                                <td><input name="nro_factura[]" type="text" class="form-control nro_factura" placeholder="Nro factura" value="<?= $d->nro_factura ?>"></td>
                                <td><input name="monto[]" type="text" class="form-control monto" placeholder="monto"  value="<?= $d->monto ?>"></td>
                                <td><input name="nota_credito[]" type="text" class="form-control nota_credito" placeholder="Nota de credito" value="<?= $d->nota_credito ?>"></td>
                                <td><input name="total[]" type="text" class="form-control total" placeholder="total"  value="<?= $d->total ?>"></td>
                                <td><p align="center">
                                        <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                        <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                                    </p></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    <tr>
                        <td>
                            <?= form_dropdown_from_query('venta[]',$this->db->get_where('ventas',array('transaccion'=>2)),'id','nro_factura',0,'',TRUE,'venta'); ?>
                        </td>
                        <td><input name="nro_factura[]" type="text" class="form-control nro_factura" placeholder="Nro factura" value=""></td>
                        <td><input name="monto[]" type="text" class="form-control monto" placeholder="Monto"  value=""></td>
                        <td><input name="nota_credito[]" type="text" class="form-control nota_credito" placeholder="Nota de credito" value=""></td>                        
                        <td><input name="total[]" type="text" class="form-control total" placeholder="total"  value=""></td>
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Efectivo: </label>
              <div class="col-sm-7">
                  <input type="text" class="pago form-control" name="totalefectivo" id="total_efectivo" value="<?= empty($pagocliente)?0:$pagocliente->totalefectivo ?>">
              </div>
            </div>
        </div>
    </div>
    <div class="row">        
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Debito: </label>
              <div class="col-sm-7">
                  <input type="text" class="pago form-control" name="totaldebito" id="total_debito" value="<?= empty($pagocliente)?0:$pagocliente->totaldebito ?>">
              </div>
            </div>
        </div>
    </div>
    <div class="row">        
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Credito: </label>
              <div class="col-sm-7">
                  <input type="text" class="pago form-control" name="totalcredito" id="total_credito" value="<?= empty($pagocliente)?0:$pagocliente->totalcredito ?>">
              </div>
            </div>
        </div>
    </div>
    <div class="row">        
        <div class="col-xs-6 col-xs-offset-6">
            <div class="form-group">
              <label for="proveedor" class="col-sm-5 control-label">Cheque: </label>
              <div class="col-sm-7">
                  <input type="text" class="pago form-control" name="totalcheque" id="total_cheque" value="<?= empty($pagocliente)?0:$pagocliente->totalcheque ?>">
              </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:40px">                
        <button id="guardar" type="submit" class="btn btn-success">Guardar Pago</button>
        <a href="javascript:imprimirTicket()" id="buttonticket" style="display:<?= !empty($pagocliente)?'':'none' ?>" class="btn btn-default buttonPrint">Imprimir Ticket</a>        
    </div>
</form>
<?php $this->load->view('predesign/datepicker',array('scripts'=>'')) ?>
<?= $this->load->view('predesign/chosen.php') ?>
<script>    
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');  
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');
            x = $("tbody .chosen-select").parents('td').find('.chzn-container');
            w = x.css('width')
            x.remove();
            $("tbody .chosen-select").removeAttr('id').removeClass('chzn-done');
            $("tbody .chosen-select").chosen({allow_single_deselect:true});
            $("tbody .chzn-container, tbody .chzn-drop").css('width',w);
            $("tbody .chzn-search input").css('width','238px');
            date_init_calendar();
    }
    function removerow(obj){
        $(obj).parent('td').parent('tr').remove();
        $(document).trigger('total');
    }
    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    })

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    }); 
    
    function val_send(form){
        $(".mask").show();
        var data = document.getElementById('formulario');
        data = new FormData(data);
        $("#guardar").attr('disabled','disabled');
        $.ajax({
            url:'<?= empty($pagocliente)?base_url('json/pagocliente'):base_url('json/pagocliente/edit') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);
                if(data['status']){
                    $(".alert").removeClass('alert-danger').addClass('alert-success').html('Se han guardado los datos con exito').show();
                     document.location.reload();
                }
                else{
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
                    $(".mask").hide();
                }
                $("#guardar").removeAttr('disabled');
            }
            });
            return false;
    }
    
    $("body").on('change','#cliente',function(){
        $.post('<?= base_url('json/getVenta') ?>',{codigo:'N/A',cliente:$(this).val()},function(data){
            producto = JSON.parse(data);            
            str = '<option value="">Todos</option>';
            //if(producto.length>0){
                for(i in producto){
                    str += '<option value="'+producto[i].id+'">'+producto[i].nro_factura+'</option>';
                }
            
            $(".venta").html(str);
            x = $("tbody .chosen-select").parents('td').find('.chzn-container');
            w = x.css('width')
            x.remove();
            $("tbody .chosen-select").removeAttr('id').removeClass('chzn-done');
            $("tbody .chosen-select").chosen({allow_single_deselect:true});
            $("tbody .chzn-container, tbody .chzn-drop").css('width',w);
            $("tbody .chzn-search input").css('width','238px');
        });
    });
    
    $(".pago").on('change',function(){
        total = parseInt($("#total_cheque").val())+parseInt($("#total_credito").val())+parseInt($("#total_debito").val())+parseInt($("#total_efectivo").val());
        $("tbody tr").each(function(){
            self = $(this);
            monto = parseInt(self.find('.monto').val());
            
            if(self.find('.venta').val()!=''){
                t = total>monto?monto:total;
                total -= monto; 
                total = total<0?0:total;
                self.find('.total').val(t);               
            }
        });
    });
    
    $("body").on('change','.venta',function(){
       if($(this).val()!=''){
        focused = $(this);
        elements = $(".nro_factura").length;
        focusedcode = $(this).val();        
        $.post('<?= base_url('json/getVenta') ?>',{codigo:$(this).val()},function(data){
            data = JSON.parse(data);
            data = data['producto'];            
            //if(data.length>0){
                focused = focused.parent().parent();
                focused.find('.nro_factura').val(data['nro_factura']);
                focused.find('.monto').val(data['total_venta']);
                focused.find('.nota_credito').val(0);
                focused.find('.total').val(data['total_venta']);
                $(document).trigger('total');
                addrow(focused.find('a'));
                $("tbody tr").last().find('.compra').focus();
            /*}
            else{
                emergente('El producto ingresado no se encuentra registrado. <a target="_new" href="<?= base_url($this->router->fetch_class().'/productos/add/json') ?>/'+focused.val()+'">¿Desea registrar uno nuevo?</a>');                        
            } */                                       
        });        
        }
    });   
    
    <?php if(!empty($pagocliente->id)): ?>
        function imprimir(){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimir/<?= $pagocliente->id ?>');
        }
        function imprimirTicket(){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimirticket/<?= $pagocliente->id ?>');
        }
    <?php else: ?>
    function imprimir(codigo){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimir/'+codigo);
     }
      function imprimirTicket(codigo){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimirticket/'+codigo);
     }
     function showButton(codigo){
            $("#buttonPrint").attr('href','javascript:imprimir('+codigo+')');
            $("#buttonticket").attr('href','javascript:imprimirTicket('+codigo+')');
            $(".buttonPrint").show();
     }
    <?php endif ?>
</script>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>