<?php if(empty($scripts)): ?>
<link type="text/css" rel="stylesheet" href="<?= base_url('assets/grocery_crud/css/ui/simple/jquery-ui-1.10.1.custom.min.css') ?>" />
<link type="text/css" rel="stylesheet" href="<?= base_url('assets/grocery_crud/css/jquery_plugins/jquery-ui-timepicker-addon.css') ?>" />
<script src="<?= base_url('assets/grocery_crud/js/jquery_plugins/ui/jquery-ui-1.10.3.custom.min.js') ?>"></script>
<script src="<?= base_url('assets/grocery_crud/js/jquery_plugins/jquery-ui-timepicker-addon.js') ?>"></script>
<?php endif ?>
<script>
$(function(){    
    date_init_calendar();
});

function date_init_calendar(){
    $('.datetime-input').datepicker({
            format:    "dd/mm/yyyy",
            autoclose: true,
            /*startDate: new Date(),*/
            weekStart: 1
    });
}
</script>