  <ol class="breadcrumb">
    <li><a href="<?= site_url() ?>"><i class="ace-icon fa fa-home home-icon"></i> Inicio</a></li>
    <li><a href="<?= site_url($this->router->fetch_class()) ?>"><?= ucfirst($this->router->fetch_class()) ?></a></li>    
    <li class="active">
        <a href="<?= site_url($this->router->fetch_class().'/'.$this->router->fetch_method()) ?>"><?= !empty($title)?$title:ucfirst($this->router->fetch_method()) ?></a>
    </li>    
  </ol>