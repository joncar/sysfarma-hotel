<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang = array(
    'Home'=>'Inicio',
    'Qui som'=>'Nosotros',
    'Habitacions'=>'Habitaciones',
    'Events'=>'Eventos',
    'Galeria'=>'Galeria',
    'Notícies'=>'Noticias',
    'Contacte'=>'Contacto',
    'Busca habitació'=>'Buscar Habitación',
    'Quan vols allotjar-te amb nosaltres?'=>'Cuantos van a alojarse con nosotros',
    'Entrada'=>'Entrada',
    'Sortida'=>'Salida',
    'Reserva'=>'Reservar',
    'Ara'=>'Ahora',
    'Adults'=>'Adultos',
    'Nens'=>'Niños',
    'Nadons'=>'Bebes',
    'Ofertes Especials'=>'Ofertas especiales',
    'Tria el pack que més et convingui'=>'Prueba el pack que mas te convenga',
    'VEURE MÉS'=>'Ver más',
    'Tot'=>'Todos',
    'Tous'=>'TODOS',
    'Rutes'=>'RUTAS',
    'Compres'=>'COMPRAS',
    'Museus'=>'MUSEOS',
    'Activitats'=>'ACTIVIDADES',
    'Festes'=>'FIESTAS',
    'Fires'=>'FERIAS',
    'Casa'=>'CAZA',
    'ON MENJAR'=>'ON MENJAR',
    'Coneix una comarca plena d\'encant'=>'Conoce una comarca plena de encanto',
    'El nostre entorn'=>'Nuestro entorno',
    'TOTES LES HABITACIONS'=>'TODAS LAS HABITACIONES',    
    'Nosaltres'=>'Nosotros',
    'Serveis'=>'Servicios',
    'Veure Més'=>'Ver más',
    'Nit romàntica'=>'Noche romantica',
    'Grup'=>'Grupal',
    'persones'=>'personas',
    'nits a la Suit'=>'Noches en la Suite',
    'Últimes notícies'=>'Últimas noticias',
    'Contacta\'ns'=>'Contáctanos',
    'Inscriu-te'=>'Inscríbete'
    
);
