<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang = array(
    'Home'=>'Home',
    'Qui som'=>'About us',
    'Habitacions'=>'Rooms',
    'Events'=>'Events',
    'Galeria'=>'Gallery',
    'Notícies'=>'News',
    'Contacte'=>'Contact us',
    'Busca habitació'=>'Search Room',
    'Quan vols allotjar-te amb nosaltres?'=>'how many still with us',
    'Entrada'=>'Check in',
    'Sortida'=>'Check out',
    'Reserva'=>'Reserve',
    'Ara'=>'Now',
    'Adults'=>'Adults',
    'Nens'=>'Kids',
    'Nadons'=>'Baby',
    'Ofertes Especials'=>'special offers',
    'Tria el pack que més et convingui'=>'Try the pack that suits you best',
    'VEURE MÉS'=>'Read More',
    'Tot'=>'All',
    'Tous'=>'Alls',
    'Rutes'=>'Routes',
    'Compres'=>'Shopping',
    'Museus'=>'Museums',
    'Activitats'=>'Activitats',
    'Festes'=>'Parties',
    'Fires'=>'Fairs',
    'Casa'=>'hunting',
    'ON MENJAR'=>'ON MENJAR',
    'Coneix una comarca plena d\'encant'=>'Discover a region full of charm',
    'El nostre entorn'=>'our environment',
    'TOTES LES HABITACIONS'=>'All rooms',    
    'Nosaltres'=>'about us',
    'Serveis'=>'Services',
    'Veure Més'=>'Read More',
    'Nit romàntica'=>'Romantic night',
    'Grup'=>'Group',
    'persones'=>'Persons',
    'nits a la Suit'=>'Nights in the Suite',
    'Últimes notícies'=>'Last news',
    'Contacta\'ns'=>'Contact us',
    'Inscriu-te'=>'Subscribe'
    
);
