<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('panel.php');

class Cron extends Panel {

    function __construct() {
        parent::__construct(); 
        if($_SESSION['cuenta']!=1){
            header("Location:".base_url('main'));
        }
    }
    
    function activarCajeros(){
        $this->db->update('user',array('status'=>1),array('admin'=>2));
        echo 'cajeros activados';
    }
    
    function desactivarCajeros(){
        $this->db->update('user',array('status'=>0),array('admin'=>2));
        echo 'cajeros desactivados';
    }
    /* Cruds */
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
