<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Panel.php');

class Admin extends Panel {

    function __construct() {
        parent::__construct();
        if (empty($_SESSION['user']) || !$this->querys->has_access($this->router->fetch_class()))
            header("Location:" . base_url());
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=cajero/' . $this->router->fetch_method()));
        elseif (empty($_SESSION['caja']))
            header("Location:" . base_url('panel/selcaja?redirect=cajero/' . $this->router->fetch_method()));
        elseif (empty($_SESSION['cajadiaria']) && $this->router->fetch_method()!='cajadiaria' && $this->router->fetch_method()!='index')
            header("Location:" . base_url('panel/selcajadiaria?redirect=cajero/' . $this->router->fetch_method()));
    }

    function loadView($data = array('view' => 'main')) {
        if (!empty($data->output)) {
            $data->view = empty($data->view) ? 'panel' : $data->view;
            $data->crud = empty($data->crud) ? 'user' : $data->crud;
            $data->title = empty($data->title) ? ucfirst($this->router->fetch_method()) : $data->title;
        }
        parent::loadView($data);
    }

    function index($url = 'main', $page = 0) {
        $this->loadView('panel');
    }

    public function controladores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $output = $crud->render();
        $this->loadView($output);
    }

    public function funciones($crud = '',$x = '',$y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $this->loadView($output);
    }

    public function usuarios($crud = '',$x = '',$y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->display_as('admin', 'Tipo de usuario')
                ->field_type('admin', 'dropdown', array('1' => 'Administrador', '2' => 'Cajero', '3' => 'Cliente'));
        $crud->required_fields('usuario','password','nombre','apellido','telefono','status','admin');
        $crud->callback_before_insert(function($post){
            $post['password'] = md5($post['password']);
            return $post;
        });
        $crud->callback_before_update(function($post,$primary){
            if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
            {
                $post['password'] = md5($post['password']);
            }
            return $post;
        });
        $output = $crud->render();
        $this->loadView($output);
    }

    public function roles($crud = '',$x = '',$y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $output->crud = 'roles';
        $this->loadView($output);
    }

    public function paises($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function ciudades($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('pais', 'paises', 'denominacion');
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function reportes($crud = '',$x = '',$y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $output->crud = 'reportes';
        $this->loadView($output);
    }

    public function reports($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        $crud->unset_read();
        $crud->unset_export()
                ->unset_print();
        $output = $crud->render();
        $output->crud = 'reportes';
        $this->loadView($output);
    }

    public function proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('tipo_proveedor', 'tipo_proveedores', 'denominacion');
        $crud->required_fields('denominacion', 'ruc', 'direccion', 'ciudad', 'pais_id', 'telefonofijo', 'telefax', 'celular', 'email');
        $crud->unset_fields('created', 'modified');
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        $crud->columns('denominacion', 'tipo_proveedor', 'ruc', 'telefonofijo', 'celular', 'Observacion');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    public function productos($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $crud->columns('codigo', 'nombre_comercial', 'nombre_generico', 'proveedor_id', 'categoria_id', 'precio_costo', 'precio_venta', 'propiedades', 'descripcion','descmax');
        $crud->set_relation('proveedor_id', 'proveedores', 'denominacion');
        $crud->set_relation('categoria_id', 'categoriaproducto', 'denominacion');
        $crud->set_relation('nacionalidad_id', 'paises', 'denominacion');
        if (empty($x) || $x == 'ajax_list' || $x == 'success')
            $crud->set_relation('usuario_id', 'user', 'nombre');

        //Etiquetas
        $crud->display_as('proveedor_id', 'Proveedor')
                ->display_as('categoria_id', 'Categoria')
                ->display_as('nacionalidad_id', 'Nacionalidad')
                ->display_as('descmin', 'Minimo de descuento')
                ->display_as('descmax', 'Maximo de descuento')
                ->display_as('usuario_id', 'Usuario')
                ->field_type('iva_id', 'dropdown', array('0' => 'Exento', '5' => '5%', '10' => '10%'))
                ->display_as('iva_id', 'IVA');
        $crud->unset_fields('created', 'modified', 'stock');
        //Validaciones
        if (!empty($_POST['codigo']) && (empty($y) || $_POST['codigo']) != $this->db->get_where('productos', array('id' => $y))->row()->codigo)
            $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo]');
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        if (!empty($x) && !empty($y) && $y == 'json' && !empty($return)) {
            $crud->field_type('codigo', 'hidden', $return);
            $crud->unset_back_to_list();
            $crud->set_lang_string('insert_success_message', '<script>window.opener.tryagain(); window.close();</script>');
        }
        $crud->unset_delete();
        if ($this->router->fetch_class() == 'admin') {
            $crud->callback_before_update(function($post,$primary){
                $producto = get_instance()->db->get_where('productos',array('id'=>$primary));
                if($producto->row()->precio_venta!=$post['precio_venta']){
                    get_instance()->db->update('productosucursal',array('precio_venta'=>$post['precio_venta']),array('producto'=>$producto->row()->codigo));
                }
            });
            
            $output = $crud->render();
            if (!empty($x) && !empty($y) && $y == 'json' && !empty($return)) {
                $output->json = 'Activo';
                $output->crud = 'articulos';
            }
            $this->loadView($output);
        } else {
            return $crud;
        }
    }

    public function categorias($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->field_type('control_vencimiento', 'true_false', array(0 => 'No', 1 => 'Si'));
        $output = $crud->render();
        $this->loadView($output);
    }

    public function compras($x = '',$y = '',$z = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->field_type('transaccion', 'dropdown', array('0' => 'ASD'));
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '1':return 'Pagada <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '0':return 'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add')
            $output->output = $this->load->view('compras', null, TRUE);
        if ($x == 'edit') {
            $compra = $this->db->get_where('compras', array('id' => $y));
            if ($compra->num_rows() > 0) {
                $compra = $compra->row();
                $output->output = $this->load->view('compras', array('compra' => $compra, 'detalles' => $this->db->get_where('compradetalles', array('compra' => $y))), TRUE);
            }
        }

        $this->loadView($output);
    }

    public function sucursales($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('ciudad_id', 'ciudades', 'denominacion');
        $crud->set_relation_dependency('ciudad_id', 'pais_id', 'pais');
        $crud->field_type('principal', 'true_false', array('0' => 'No', '1' => 'Si'));

        $crud->callback_before_insert(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $crud->callback_before_update(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $output = $crud->render();
        $this->loadView($output);
    }

    public function cajas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->fields('sucursal', 'denominacion', 'serie');
        $crud->field_type('abierto', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    public function cajadiaria($x = '', $y = '', $z = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=cajero/' . $this->router->fetch_method()));
        elseif (empty($_SESSION['caja']))
            header("Location:" . base_url('panel/selcaja?redirect=cajero/' . $this->router->fetch_method()));

        $crud = parent::crud_function($x, $y);
        $crud->set_relation_dependency('caja', 'sucursal', 'sucursal');
        $crud->unset_delete();
        $crud->required_fields('monto_inicial', 'caja', 'sucursal');
        if ($this->db->get_where('cajadiaria', array('caja' => $_SESSION['caja'], 'sucursal' => $_SESSION['sucursal'], 'abierto' => 1))->num_rows() > 0) {
            $crud->unset_add();
            $crud->where('cajadiaria.abierto', 1);
        }
        $crud->field_type('fecha_apertura', 'invisible')
                ->field_type('efectivoarendir', 'invisible')
                ->field_type('total_descuentos', 'invisible')
                ->field_type('total_debito', 'invisible')
                ->field_type('total_cheque', 'invisible')
                ->field_type('fecha_cierre', 'invisible')
                ->field_type('total_contado', 'invisible')
                ->field_type('total_pago_clientes', 'invisible')
                ->field_type('total_credito', 'invisible')
                ->field_type('total_egreso', 'invisible')
                ->field_type('total_efectivo', 'invisible')
                ->field_type('sucursal', 'hidden', $_SESSION['sucursal'])
                ->field_type('caja', 'hidden', $_SESSION['caja'])
                ->field_type('usuario', 'hidden', $_SESSION['user']);

        if ($crud->getParameters() == 'edit') {
            $crud->field_type('correlativo', 'input');
        }
        $crud->display_as('abierto', 'Tipo de apertura');
        if ($crud->getParameters() == 'add') {
            $crud->field_type('abierto', 'invisible');
        } else {
            $crud->field_type('abierto', 'true_false', array('0' => 'Cerrado', '1' => 'Abierto'));
        }

        $crud->callback_add_field('correlativo', function($val) {
            get_instance()->db->order_by('correlativo', 'DESC');
            $aperturas = get_instance()->db->get_where('cajadiaria', array('sucursal' => $_SESSION['sucursal'], 'caja' => $_SESSION['caja']));
            $correlativo = $aperturas->num_rows() == 0 ? '1' : $aperturas->row()->correlativo;
            return form_input('correlativo', $correlativo, 'id="field-correlativo" class="form-control"');
        });

        $crud->callback_before_insert(function($post) {
            $post['abierto'] = '1';
            $post['total_efectivo'] = $post['monto_inicial'];
            $post['fecha_apertura'] = date("Y-m-d H:i:s");
            get_instance()->db->update('cajas', array('abierto' => 1), array('id' => $post['caja']));
            return $post;
        });

        $crud->callback_before_update(function($post, $id) {
            $p = (array) $this->db->get_where('cajadiaria', array('id' => $id))->row();
            if ($post['abierto'] == 0) {
                get_instance()->db->update('cajas', array('abierto' => 0), array('id' => $p['caja']));
                $post['total_contado'] = 0;
                $post['total_pago_clientes'] = 0;
                $post['total_credito'] = 0;
                $post['total_egreso'] = 0;
                $post['total_efectivo'] = 0;
                foreach ($this->db->get_where('ventas', array('status !='=>-1,'cajadiaria' => $id))->result() as $v) {
                    $post['total_contado'] += $v->transaccion == 1 ? $v->total_venta : 0;
                    $post['total_pago_clientes'] += $v->total_venta;
                    $post['total_credito'] += $v->total_credito;
                    $post['total_descuentos'] += $v->total_descuentos;
                    $post['total_egreso'] += 0;
                    $post['total_efectivo'] += $v->total_efectivo;
                    $post['total_debito'] += $v->total_debito;
                    $post['total_cheque'] += $v->total_cheque;
                }
                $post['efectivoarendir'] = $post['total_efectivo'] + $p['monto_inicial'];
                $post['fecha_cierre'] = date("Y-m-d H:i:s");
            }
            return $post;
        });
        if ($crud->getParameters() == 'list' || $crud->getParameters() == 'ajax_list' || $crud->getParameters() == 'success') {
            $crud->set_relation('sucursal', 'sucursales', 'denominacion');
            $crud->set_relation('caja', 'cajas', 'denominacion', array('abierto' => 0));
            $crud->set_relation('usuario', 'user', '{nombre} {apellido}');
        }
        $crud->callback_add_field('fecha_apertura', function($val, $row) {
            return '<input id="field-fecha_apertura" name="fecha_apertura" type="text" value="' . date("d/m/Y h:i:s") . '" maxlength="19" class="datetime-input">';
        });
        $crud->add_action('Imprimir ventas','',base_url('reportes/detalle_ventas').'/');
        if ($this->router->fetch_class() == 'admin') {
            $output = $crud->render();
        } else
            return $crud;
        $this->loadView($output);
    }

    public function distribucion($x = '', $y = '', $z = '') {
        if (empty($x) || !is_numeric($x) || $x < 0) {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('compras');
            $crud->set_relation('proveedor', 'proveedores', 'denominacion');
            $crud->unset_delete()
                    ->unset_edit()
                    ->unset_add()
                    ->unset_print()
                    ->unset_export()
                    ->unset_read();
            $crud->callback_column('Distribucion', function($val, $row) {
                $disponible = $this->querys->get_disponible($row->id);
                get_instance()->db->select('SUM(compradetalles.cantidad) as p');
                $cantidad = get_instance()->db->get_where('compradetalles', array('compra' => $row->id))->row()->p;
                return $disponible . ' de ' . $cantidad;
            });
            $crud->columns('nro_factura', 'productos', 'fecha', 'proveedor', 'Distribucion');
            $crud->callback_column('nro_factura', function($val, $row) {
                return '<a href="' . base_url('admin/distribucion/' . $row->id) . '">' . $val . '</a>';
            });
            $crud->callback_column('productos', function($val, $row) {
                return get_instance()->db->query('select SUM(cantidad) as productos from compradetalles where compra = ' . $row->id)->row()->productos;
            });
            $crud = parent::get_reportes($crud);
            $output = $crud->render();
            $output->crud = 'distribucion';
            $this->loadView($output);
        } else {
            $compra = $this->db->get_where('compras', array('id' => $x));
            if ($compra->num_rows() > 0) {
                $data = array();
                $data['compra'] = $compra->row();
                $this->db->select('productos.id, productos.nombre_comercial as producto,compradetalles.lote, compradetalles.vencimiento, compradetalles.precio_venta, compradetalles.cantidad, compradetalles.producto as producto_codigo');
                $this->db->join('productos', 'productos.codigo = compradetalles.producto');
                $data['detalles'] = $this->db->get_where('compradetalles', array('compra' => $data['compra']->id));
                foreach ($data['detalles']->result() as $n => $d) {
                    $this->db->select('SUM(distribucion.cantidad) as p');
                    $compradetalle = $this->db->get_where('distribucion', array('compra' => $data['compra']->id, 'producto' => $d->producto_codigo));
                    if ($compradetalle->num_rows() > 0) {
                        $data['detalles']->row($n)->total = $d->cantidad;
                        $data['detalles']->row($n)->cantidad = $d->cantidad - $compradetalle->row()->p;
                        $precio_venta = $this->db->get_where('productos', array('codigo' => $d->producto_codigo))->row()->precio_venta;
                        $data['detalles']->row($n)->precio_venta = $precio_venta != $d->precio_venta ? $precio_venta : $d->precio_venta;
                    }
                }
                $data['sucursales'] = $this->db->get('sucursales');
                $data['view'] = 'distribucion';
                $this->loadView($data);
            } else
                $this->loadView('404');
        }
    }

    public function ventas($x = '', $y = '') {
        $this->load->library('enletras');
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/ventas'));
        else {
            $crud = parent::crud_function($x, $y);
            $crud->set_theme('bootstrap2');
            $crud->unset_read()
                    ->unset_delete();
            $crud->columns('transaccion', 'cliente', 'fecha', 'caja', 'nro_factura', 'sucursal', 'total_venta', 'status');
            $crud->callback_column('status', function($val, $row) {
                switch ($val) {
                    case '0':return 'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                        break;
                    case '-1':return 'Anulada';
                        break;
                    case '-2': return 'Anulada y procesada';
                        break;
                }
            });
            $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
                    ->set_relation('cliente', 'clientes', '{nombres} {apellidos}')
                    ->set_relation('sucursal', 'sucursales', 'denominacion');
            $crud->where('ventas.sucursal', $_SESSION['sucursal']);
            if (!empty($_SESSION['caja']))
                $crud->where('ventas.caja', $_SESSION['caja']);
            $output = $crud->render();
            $output->crud = 'ventas';
            if ($x == 'add') {
                $output->output = $this->load->view('ventas', array(), TRUE);
            }
            if ($x == 'edit' && !empty($y) && is_numeric($y)) {
                $venta = $this->db->get_where('ventas', array('id' => $y));
                $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                $output->output = $this->load->view('ventas', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
            }
            if ($x == 'imprimir' || $x=='imprimir2' || $x == 'imprimirticket') {
                if (!empty($y) && is_numeric($y)) {
                    $this->db->select('ventas.*, sucursales.denominacion, sucursales.telefono, sucursales.direccion, cajas.denominacion as caja, clientes.nombres as clientename, clientes.apellidos as clienteadress');
                    $this->db->join('clientes', 'clientes.id = ventas.cliente');
                    $this->db->join('cajas', 'cajas.id = ventas.caja');
                    $this->db->join('sucursales', 'sucursales.id = ventas.sucursal');
                    $venta = $this->db->get_where('ventas', array('ventas.id' => $y));
                    if ($venta->num_rows() > 0) {
                        $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                        if ($x == 'imprimir' || $x=="imprimir2") {
                            $view = $x=="imprimir"?"imprimir_factura":"imprimir_factura_linea";
                            $margen = $x=="imprimir"?array(5, 1, 5, 8):array(2, 5, 5, 8);
                            $papel = 'L';
                            if($x=="imprimir" || $x=="imprimir2"){
                                if(!isset($_GET['pdf']) || $_GET['pdf']!=0){
                                    $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', $margen);
                                    $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                                    $html2pdf->Output('Factura Legal.pdf', 'D');
                                }else{
                                    $this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles));
                                }
                            } else {
                                $output = $this->load->view('reportes/imprimirTicket', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
                                if ($this->router->fetch_class() == 'admin') {
                                    echo $output;
                                } else {
                                    return $output;
                                }
                            }
                        } else {
                            echo "Factura no encontrada";
                        }
                    } else {
                        echo 'Factura no encontrada';
                    }
                }
            } else {
            if ($this->router->fetch_class() == 'admin')
                $this->loadView($output);
            else    
                return $output;
            }
        }
    }
    

    public function salidas($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/salidas'));
        else {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        //$crud->columns('transaccion','cliente','fecha','caja','nro_factura','sucursal','total_venta'); 
        $crud->set_relation('proveedor', 'proveedores', 'denominacion')
                ->set_relation('motivo', 'motivo_salida', 'denominacion')
                ->set_relation('usuario', 'user', '{nombre} {apellido}');
        $crud->unset_read()
                ->unset_delete();
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add'){
            $output->output = $this->load->view('salidas', array(), TRUE);
        }
        if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $salida = $this->db->get_where('salidas', array('id' => $y));
            $detalles = $this->db->get_where('salida_detalle', array('salida' => $salida->row()->id));
            $output->output = $this->load->view('salidas', array('salida' => $salida->row(), 'detalles' => $detalles), TRUE);
        }
        if ($x == 'imprimir') {
                if (!empty($y) && is_numeric($y)) {
                    $this->db->select('salidas.*, proveedores.denominacion as proveedorname, motivo_salida.denominacion as motivoname, CONCAT(user.nombre," ",user.apellido) as usuario',FALSE);
                    $this->db->join('motivo_salida','motivo_salida.id = salidas.motivo');
                    $this->db->join('proveedores','proveedores.id = salidas.proveedor');
                    $this->db->join('user','user.id = salidas.usuario');
                    $venta = $this->db->get_where('salidas', array('salidas.id' => $y));
                    if ($venta->num_rows() > 0) {
                        $this->db->select('salida_detalle.*, productos.nombre_comercial');
                        $this->db->join('productos', 'salida_detalle.producto = productos.codigo');
                        $detalles = $this->db->get_where('salida_detalle', array('salida' => $venta->row()->id));
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', array(5, 5, 5, 8));
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/imprimir_salida', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                        $html2pdf->Output('Reporte Legal.pdf', 'D');
                        //echo $this->load->view('reportes/imprimir_salida',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                    
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            } else {
                if ($this->router->fetch_class() == 'admin')
                    $this->loadView($output);
                else
                    return $output;
            }
        }
    }

    public function clientes($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('tipo_doc', 'tipodocumento', 'denominacion');
        $crud->field_type('sexo', 'dropdown', array('M' => 'M', 'F' => 'F'));
        $crud->set_relation('pais', 'paises', 'denominacion');
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $crud->set_relation('barrio', 'barrios', 'denominacion');
        $crud->set_relation_dependency('ciudad', 'pais', 'pais');
        $crud->set_relation_dependency('barrio', 'ciudad', 'ciudad');

        $crud->required_fields('nombres', 'apellidos', 'tipo_doc', 'nro_documento');
        $crud->set_rules('email', 'Email', 'valid_email');
        $crud->callback_after_insert(function($post, $id) {
            $data = array();
            $data['usuario'] = $post['nro_documento'];
            $data['password'] = md5('12345678');
            $data['email'] = $post['email'];
            $data['nombre'] = $post['nombres'];
            $data['apellido'] = $post['apellidos'];
            $data['telefono'] = $post['celular1'];
            $data['status'] = 1;
            $data['admin'] = 3;
            get_instance()->db->insert('user', $data);
            return true;
        });
        if ($x == 'add' || $x == 'insert' || $x == 'insert_validation') {
            $crud->set_rules('nro_documento', 'Cedula', 'required|is_unique[clientes.nro_documento]');
        }
        $crud->unset_fields('created', 'modified');

        if ($this->router->fetch_class() == 'admin') {
            $output = $crud->render();
            $output->crud = 'clientes';
            if (!empty($x) && !empty($y) && $y == 'json') {
                $output->json = 'Activo';
            }
            $this->loadView($output);
        } else {
            return $crud;
        }
    }

    public function monedas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->field_type('primario', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'user';
        $this->loadView($output);
    }

    public function productosucursal($x = '', $y = '') {
        $this->norequireds = array('lote', 'vencimiento');
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $crud->unset_read()
             ->unset_export()
             ->unset_print()
             ->unset_add();
        $crud->set_relation('proveedor_id', 'proveedores', 'denominacion');
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        if ($crud->getParameters() == 'list' || $crud->getParameters() == 'success' || $crud->getParameters() == 'ajax_list' || $crud->getParameters() == 'ajax_list_info') {
            $crud->set_relation('producto', 'productos', '{codigo} | {nombre_comercial} | {precio_venta}');
            $crud->set_relation('j286e18ee.proveedor_id', 'proveedores', 'denominacion');
        }
        $crud->display_as('producto', 'Codigo del producto')
                ->display_as('sc6944f59', 'Laboratorio');
        $crud->set_primary_key('codigo', 'productos');
        $crud->columns('codigo', 'sc6944f59', 'proveedor_id', 'fechaalta', 'nombre_comercial', 'sucursal', 'lote', 'vencimiento', 'stock', 'precio_costo', 'precio_venta');
        $crud->callback_column('codigo', function($val, $row) {
            $x = explode("|", $row->s286e18ee);
            return $x[0];
        });
        $crud->callback_column('nombre_comercial', function($val, $row) {
            return explode("|", $row->s286e18ee)[1];
        });
        $crud->callback_column('precio_venta', function($val, $row) {
            return explode("|", $row->s286e18ee)[2];
        });
        $crud->callback_column('vencimiento', function($val, $row) {
            return $val == '31/12/1969' || $val == '1969-12-31' ? 'N/A' : $val;
        });

        $crud->callback_column('scb5fc3d6', function($val, $row) {
            return '<a href="' . base_url($this->router->fetch_class() . '/productosucursal/' . $row->sucursal) . '">' . $val . '</a>';
        });
        if (is_numeric($x)) {
            $crud->where('productosucursal.sucursal', $x);
        }
        /*$crud->callback_after_insert(array($this, 'productosucursal_ainsert'));
        $crud->callback_after_update(array($this, 'productosucursal_ainsert'));*/
        //$crud->where('productosucursal.stock >',0);         
        if ($crud->getParameters() == 'add' || $crud->getParameters() == 'insert' || $crud->getParameters() == 'insert_validation')
            $crud->set_rules('producto', 'Producto', 'required|callback_lote_not_exist');
        if ($this->router->fetch_class() == 'admin') {
            $output = $crud->render();
            $output->crud = 'productosucursal';
            $this->loadView($output);
        } else
            return $crud;
    }

    public function productosucursal_ainsert($post, $id) {
        $this->db->update('productosucursal', array('precio_venta' => $post['precio_venta']), array('producto' => $post['producto']));
        $this->db->update('productos', array('precio_venta' => $post['precio_venta']), array('codigo' => $post['producto']));
    }

    public function lote_not_exist($id) {
        if (!empty($_POST['lote']) && $this->db->get_where('productosucursal', array('lote' => $_POST['lote'], 'producto' => $_POST['producto']))->num_rows() > 0) {
            $this->form_validation->set_message('lote_not_exist', 'El producto ya tiene ese lote registrado');
            return false;
        } else
            return true;
    }

    public function transferencias($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/ventas'));
        else {
            $crud = parent::crud_function($x, $y);
            $crud->set_theme('bootstrap2');
            $crud->unset_delete();
            $crud->set_relation('sucursal_origen', 'sucursales', 'denominacion')
                    ->set_relation('sucursal_destino', 'sucursales', 'denominacion');
            $crud->display_as('procesado', 'Status');
            $crud->field_type('procesado', 'dropdown', array('0' => 'No procesado', '-1' => 'Rechazado', '2' => 'Aprobado', '3' => 'Entregado'));
            if ($this->router->fetch_class() != 'admin') {
                $crud->where('sucursal_destino', $_SESSION['sucursal']);
                $crud->or_where('sucursal_origen', $_SESSION['sucursal']);
            }

            $crud->callback_column('procesado', function($val, $row) {
                if ($row->sucursal_origen == $_SESSION['sucursal'] && $val == 0){
                    return form_dropdown('procesado', array('0' => 'Sin procesar', '-1' => 'Rechazado', '1' => 'Aprobado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
                }
                elseif ($row->sucursal_destino == $_SESSION['sucursal'] && $val == 1){
                    return form_dropdown('procesado', array('1' => 'Aprobado', '2' => 'Entregado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
                }
                else {
                    switch ($val) {
                        case '0': return 'No procesado';
                        case '-1': return 'Rechazado';
                        case '1': return 'Aprobado';
                        case '2': return 'Entregado';
                    }
                }
            });
            $output = $crud->render();
            $output->crud = 'transferencias';
            if ($x == 'add'){
                $output->output = $this->load->view('transferencias', array(), TRUE);
            }
            if ($x == 'edit' && !empty($y) && is_numeric($y)) {
                $transferencia = $this->db->get_where('transferencias', array('id' => $y));
                $detalles = $this->db->get_where('transferencias_detalles', array('transferencia' => $transferencia->row()->id));
                $output->output = $this->load->view('transferencias', array('transferencia' => $transferencia->row(), 'detalles' => $detalles), TRUE);
            }
            if ($x == 'imprimir') {
                if (!empty($y) && is_numeric($y)) {
                    $this->db->select('transferencias.*,suco.id as sucursalOrigenId, suco.denominacion as sucursalOrigen, sucd.denominacion as sucursalDestino');
                    $this->db->join('sucursales suco', 'suco.id = transferencias.sucursal_origen');
                    $this->db->join('sucursales sucd', 'sucd.id = transferencias.sucursal_destino');
                    $venta = $this->db->get_where('transferencias', array('transferencias.id' => $y));
                    if ($venta->num_rows() > 0) {
                        $this->db->join('productos', 'transferencias_detalles.producto = productos.codigo');
                        $detalles = $this->db->get_where('transferencias_detalles', array('transferencia' => $venta->row()->id));
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', array(20, 5, 5, 8));
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/imprimir_transferencia', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                        $html2pdf->Output('Reporte Transferencia.pdf', 'D');
                        //echo $this->load->view('reportes/imprimir_transferencia',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                    
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            } else {
                if ($this->router->fetch_class() == 'admin')
                    $this->loadView($output);
                else
                    return $output;
            }
        }
    }

    public function motivo_salida($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function motivo_entrada($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function tipo_proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function entrada_productos($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/entrada_productos'));
        else {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('motivo', 'motivo_entrada', 'denominacion')
                ->set_relation('proveedor', 'proveedores', 'denominacion')
                ->set_relation('usuario', 'user', '{nombre} {apellido}');
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add')
            $output->output = $this->load->view('entradas', null, TRUE);
        if ($x == 'edit') {
            $compra = $this->db->get_where('entrada_productos', array('id' => $y));
            if ($compra->num_rows() > 0) {
                $compra = $compra->row();
                $output->output = $this->load->view('entradas', array('entrada' => $compra, 'detalles' => $this->db->get_where('entrada_productos_detalles', array('entrada_producto' => $y))), TRUE);
            }
        }

        $this->loadView($output);
        }
    }

    public function notas_credito($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add')
            $output->output = $this->load->view('notas_credito', null, TRUE);
        if ($x == 'edit') {
            $compra = $this->db->get_where('notas_credito', array('id' => $y));
            if ($compra->num_rows() > 0) {
                $compra = $compra->row();
                $output->output = $this->load->view('notas_credito', array('nota' => $compra, 'detalles' => $this->db->get_where('notas_credito_detalles', array('nota_credito' => $y))), TRUE);
            }
        }

        $this->loadView($output);
    }

    public function notas_credito_cliente($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add')
            $output->output = $this->load->view('notas_credito_cliente', null, TRUE);
        if ($x == 'edit') {
            $compra = $this->db->get_where('notas_credito_cliente_detalles', array('id' => $y));
            if ($compra->num_rows() > 0) {
                $compra = $compra->row();
                $output->output = $this->load->view('notas_credito_cliente', array('nota' => $compra, 'detalles' => $this->db->get_where('notas_credito_cliente_detalle', array('nota_credito' => $y))), TRUE);
            }
        }

        $this->loadView($output);
    }

    public function pagocliente($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/pagocliente'));
        if (empty($_SESSION['caja']))
            header("Location:" . base_url('panel/selcaja?redirect=admin/pagocliente'));
        else {
            $crud = parent::crud_function($x, $y);
            $crud->unset_read()
                    ->unset_delete();
            $crud->set_relation('cliente', 'clientes', 'nombres');
            $crud->field_type('anulado', 'true_false', array(0 => 'No', 1 => 'Si'));
            $crud->where('sucursal', $_SESSION['sucursal'])
                    ->where('caja', $_SESSION['caja']);
            $crud->columns('cliente', 'recibo', 'fecha', 'totalefectivo', 'totaldebito', 'totalnotacredito', 'totalcheque', 'totalcredigo', 'totalpagado', 'saldo', 'anulado');
            $crud->callback_column('saldo', function($val, $row) {
                get_instance()->db->select('SUM(total_venta) as total');
                return number_format(get_instance()->db->get_where('ventas', array('cliente' => $row->cliente, 'transaccion' => 2, 'status' => 0))->row()->total, 2, ',', '.');
            });
            $output = $crud->render();
            $output->crud = 'ventas';
            if ($x == 'add')
                $output->output = $this->load->view('pagosclientes', array(), TRUE);
            if ($x == 'edit' && !empty($y) && is_numeric($y)) {
                $venta = $this->db->get_where('pagocliente', array('id' => $y));
                $detalles = $this->db->get_where('pagoclientesfactura', array('pagocliente' => $venta->row()->id));
                $output->output = $this->load->view('pagosclientes', array('pagocliente' => $venta->row(), 'detalles' => $detalles), TRUE);
            }
            if ($x == 'imprimir' || $x == 'imprimirticket') {
                if (!empty($y) && is_numeric($y)) {
                    $this->db->select('pagocliente.*, sucursales.denominacion, sucursales.telefono, sucursales.direccion, cajas.denominacion as caja, clientes.nombres as clientename, clientes.apellidos as clienteadress');
                    $this->db->join('clientes', 'clientes.id = pagocliente.cliente');
                    $this->db->join('cajas', 'cajas.id = pagocliente.caja');
                    $this->db->join('sucursales', 'sucursales.id = pagocliente.sucursal');
                    $venta = $this->db->get_where('pagocliente', array('pagocliente.id' => $y));
                    if ($venta->num_rows() > 0) {
                        $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                        $this->db->select('SUM(total_venta) as total');
                        $venta->row()->saldo = $this->db->get_where('ventas', array('cliente' => $venta->row()->cliente, 'transaccion' => 2, 'status' => 0))->row()->total;
                        if ($venta->row()->saldo > 0) {
                            $total = 0;
                            $this->db->select('pagoclientesfactura.*');
                            $this->db->join('pagoclientesfactura', 'pagoclientesfactura.pagocliente = pagocliente.id');
                            foreach ($this->db->get_where('pagocliente', array('cliente' => $venta->row()->cliente))->result() as $p) {
                                $total+=$p->total + $p->nota_credito;
                            }
                            $venta->row()->saldo-=$total;
                        } else {
                            $venta->row()->saldo = 0;
                        }
                        $output = $this->load->view('reportes/imprimirTicketPago', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
                        if ($this->router->fetch_class() == 'admin') {
                            echo $output;
                        } else {
                            return $output;
                        }
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            } else {
                if ($this->router->fetch_class() == 'admin')
                    $this->loadView($output);
                else
                    return $output;
            }
        }
    }
    
    public function cuentas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        if ($this->router->fetch_class() == 'admin'){
            $output = $crud->render();
            $this->loadView($output);
        }
        else
            return $output;
    }
    
    public function gastos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y); 
        $crud->field_type('cajadiaria','hidden',$_SESSION['cajadiaria']);
        $crud->field_type('user_created','hidden',$_SESSION['user']);
        $crud->field_type('user_modified','hidden',$_SESSION['user']);
        $crud->field_type('fecha_created','hidden',date("Y-m-d"));
        $crud->field_type('fecha_modified','hidden',date("Y-m-d"));
        if ($this->router->fetch_class() == 'admin'){
            $output = $crud->render();
            $this->loadView($output);
        }
        else
            return $output;
    }

    public function pagoproveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_read()
                ->unset_delete();
        $output = $crud->render();
        $output->crud = 'ventas';
        if ($x == 'add')
            $output->output = $this->load->view('pagoproveedor', array(), TRUE);
        if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $venta = $this->db->get_where('pagoproveedores', array('id' => $y));
            $detalles = $this->db->get_where('pagoproveedor_detalles', array('pagoproveedor' => $venta->row()->id));
            $output->output = $this->load->view('pagoproveedor', array('pagoproveedor' => $venta->row(), 'detalles' => $detalles), TRUE);
        }
        if ($this->router->fetch_class() == 'admin')
            $this->loadView($output);
        else
            return $output;
    }

    public function saldos($x = '', $y = '') {
        $this->as = array('saldos' => 'ventas');
        $crud = parent::crud_function($x, $y);
        $crud->unset_read()
                ->unset_delete()
                ->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_export()
                ->unset_print();
        $crud->set_relation('cliente', 'clientes', '{nro_documento}|{nombres}|{apellidos}');
        $crud->where('ventas.transaccion', 2);
        $crud->where('ventas.status != ', -1);
        $crud->group_by('ventas.cliente');
        $crud->columns('nro_documento', 'nombres', 'apellidos', 'total_pagos', 'total_venta', 'ultima_compra', 'ultimo_pago', 'saldo');
        $crud->callback_column('nro_documento', function($val, $row) {
            return explode('|', $row->s4983a0ab)[0];
        });
        $crud->callback_column('nombres', function($val, $row) {
            return explode('|', $row->s4983a0ab)[1];
        });
        $crud->callback_column('apellidos', function($val, $row) {
            return explode('|', $row->s4983a0ab)[2];
        });

        $crud->callback_column('total_venta', function($val, $row) {
            get_instance()->db->select('SUM(ventas.total_venta) as total_venta');
            get_instance()->db->where('transaccion', '2');
            get_instance()->db->where('ventas.status != ', -1);
            $p = get_instance()->db->get_where('ventas', array('ventas.cliente' => $row->cliente));
            return $p->row()->total_venta == null ? (string) 0 : $p->row()->total_venta;
        });

        $crud->callback_column('total_pagos', function($val, $row) {
            get_instance()->db->select('SUM(pagocliente.totalpagado) as total_pagos');
            $p = get_instance()->db->get_where('pagocliente', array('pagocliente.cliente' => $row->cliente));
            return $p->num_rows() == 0 ? (string) '0' : (string) $p->row()->total_pagos;
        });

        $crud->callback_column('ultima_compra', function($val, $row) {
            get_instance()->db->select('ventas.fecha');
            get_instance()->db->where('transaccion', '2');
            get_instance()->db->order_by('id', 'DESC');
            $p = get_instance()->db->get_where('ventas', array('ventas.cliente' => $row->cliente));
            return date("d-m-Y H:i:s", strtotime($p->row()->fecha));
        });

        $crud->callback_column('ultimo_pago', function($val, $row) {
            get_instance()->db->select('pagocliente.fecha');
            $p = get_instance()->db->get_where('pagocliente', array('pagocliente.cliente' => $row->cliente));
            return $p->num_rows() == 0 ? (string) 'N/A' : date("d-m-Y H:i:s", strtotime($p->row()->fecha));
        });
        $crud->add_action('Abonar deuda', '', base_url('cajero/abonarDeuda/') . '/');
        $crud->callback_column('saldo', function($val, $row) {
            return number_format($row->total_venta - $row->total_pagos, 0, ',', '.');
        });
        $output = $crud->render();
        $output->title = 'Saldos';
        if ($this->router->fetch_class() == 'admin') {
            $this->loadView($output);
        } else {
            return $output;
        }
    }
    
    public function sendSMS(){
        $log = '';
        //$this->form_validation->set_rules('sms','Mensaje','required');        
        if(!empty($_GET['msj']) && isset($_GET['lote'])){
            $_POST['sms'] = $_GET['msj'];            
            $this->db->limit('300',$_GET['lote']);
            $clientes = $this->db->get_where('clientes',array('celular1 != '=>0,'LENGTH(celular1)'=>10));                        
            if($clientes->num_rows()>0){                
                $this->load->library('nexmo');
                $n = 0;
                foreach($clientes->result() as $c){
                    $cell = '+595'.substr($c->celular1,1);                    
                    $resp = $this->nexmo->send_message('TELEFARMA',$cell,array('text'=>$this->input->post('sms')));                   
                    $error = !empty($resp->messages[0]->{'error-text'})?$resp->messages[0]->{'error-text'}:'';
                    $class = !empty($error)?'alert alert-danger':'';                    
                    $l = '<p class="'.$class.'">Envio de mensaje a '.$cell.' regreso status '.$resp->messages[0]->status.' error: '.$error.'</p>';
                    $log.= '<p class="'.$class.'">Envio de mensaje a '.$cell.' regreso status '.$resp->messages[0]->status.' error: '.$error.'</p>';
                    echo $l;
                    flush();
                    ob_flush();
                    ob_clean();                    
                    $n++;
                    if($n==5){
                        sleep(2.5);
                        $n = 0;
                    }
                }
                //echo "<script>window.opener.showLog('".$log."'); window.close();</script>";
            }
        }else{
           $this->loadView(array('view'=>'sms','log'=>$log,'clientes'=>$this->db->get_where('clientes',array('celular1 != '=>0,'LENGTH(celular1)'=>10))->num_rows()));        
        }
    }
    
    public function actualizardescuentos(){
        if(!empty($_POST)){
            $this->form_validation->set_rules('porcentaje','Porcentaje','required|numeric');
            $this->form_validation->set_rules('proveedores','Proveedor','required|numeric');
            $this->form_validation->set_rules('categoriaproducto','Categoria del producto','required|numeric');
            if($this->form_validation->run()){
                $where = array('categoria_id'=>$this->input->post('categoriaproducto'),'proveedor_id'=>$this->input->post('proveedores'));
                if(!empty($_POST['paises'])){
                    $where['nacionalidad_id'] = $_POST['paises'];
                }
                $this->db->update('productos',array('descmax'=>$this->input->post('porcentaje')),$where);
                $msj = $this->success('Los datos se han actualizado con éxito');
            }else{
                $msj = $this->error($this->form_validation->error_string());
            }
        }else{
            $msj = '';
        }
        $this->loadView(array('view'=>'actualizardescuentos','msj'=>$msj));
    }
    
    public function saldos_proveedores($x = '', $y = '') {
            $this->as = array('saldos_proveedores' => 'compras');
            $crud = parent::crud_function($x, $y);
            $crud->unset_read()
                    ->unset_delete()
                    ->unset_add()
                    ->unset_edit()
                    ->unset_read()
                    ->unset_export()
                    ->unset_print();           
            $crud->set_subject('Pago a Proveedores');
            $crud->where('compras.status != ', -1);
            $crud->group_by('compras.proveedor');
            $crud->columns('sucursal', 'proveedor', 'total_pagos', 'total_compra', 'ultima_compra', 'ultimo_pago', 'saldo');
            $crud->set_relation('sucursal','sucursales','denominacion');
            $crud->set_relation('proveedor','proveedores','denominacion');
            
            $crud->callback_column('total_compra', function($val, $row) {
                get_instance()->db->select('SUM(compras.total_compra) as total_compra');
                $p = get_instance()->db->get_where('compras', array('compras.proveedor' => $row->proveedor,'compras.status >='=>0,'transaccion'=>2));
                return $p->row()->total_compra == null ? (string) 0 : $p->row()->total_compra;
            });

            $crud->callback_column('total_pagos', function($val, $row) {
                get_instance()->db->select('SUM(pagoproveedores.total_abonado) as total_pagos');
                $p = get_instance()->db->get_where('pagoproveedores', array('pagoproveedores.proveedor' => $row->proveedor));
                ($p->num_rows()>0 && $p->row()->total_pagos==null)?$p->row()->total_pagos = 0:$p->row()->total_pagos;
                return $p->num_rows() == 0 ? (string) '0' : (string) $p->row()->total_pagos;
            });

            $crud->callback_column('ultima_compra', function($val, $row) {
                get_instance()->db->select('compras.fecha');                
                get_instance()->db->order_by('id', 'DESC');
                $p = get_instance()->db->get_where('compras', array('compras.proveedor' => $row->proveedor,'compras.status >='=>0,'transaccion'=>2));
                return $p->num_rows()>0?date("d-m-Y H:i:s", strtotime($p->row()->fecha)):'N/A';
            });

            $crud->callback_column('ultimo_pago', function($val, $row) {
                get_instance()->db->select('pagoproveedores.fecha');
                $p = get_instance()->db->get_where('pagoproveedores', array('pagoproveedores.proveedor' => $row->proveedor,'pagoproveedores.anulado !='=>1));
                return $p->num_rows() == 0 ? (string) 'N/A' : date("d-m-Y H:i:s", strtotime($p->row()->fecha));
            });
            $crud->add_action('Abonar deuda', '', base_url('cajero/abonarDeudaProveedor/') . '/');
            $crud->callback_column('saldo', function($val, $row) {
                return number_format($row->total_compra - $row->total_pagos, 0, ',', '.');
            });
            $output = $crud->render();
            $output->title = 'Saldos';
            if ($this->router->fetch_class() == 'admin') {
                $this->loadView($output);
            } else {
                return $output;
            }
        }

    /* Cruds */
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
