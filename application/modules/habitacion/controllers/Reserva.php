<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Reserva extends Panel{
        function __construct() {
            parent::__construct();
             if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
                header("Location:".base_url('panel/selsucursal'));
                die();
            }
            if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
                header("Location:".base_url('panel/selcaja'));
                die();
            }
            if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
                header("Location:".base_url('panel/selcajadiaria'));
                die();
            }            
        }
        
        public function reservas($x = '',$y = ''){            
            $crud = $this->crud_function('','');             
            $crud->display_as('id','#Reserva')
                 ->display_as('clientes_id','Huesped');            
            $crud->field_type('ventas_id','hidden');
            $crud->columns('id','nombre','apellido');              
            $crud->callback_column('se8701ad4',function($val,$row){
                return '<a href="'.base_url('seguridad/user/edit/'.$row->clientes_id).'"><i class="fa fa-search"></i> '.$val.'</a>';
            });
            if($x=='listado' && !empty($y)){
                $crud->where('desde >=',$y);                
            }
            $crud->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export();
                     
            if($crud->getParameters()=='add'){                
                $crud->field_type('desde','hidden')
                     ->field_type('hasta','hidden')
                     ->field_type('fecha_reserva','hidden',date("Y-m-d"))
                     ->field_type('precio','hidden')
                     ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
                     ->field_type('abonado','hidden');
            }            
            $crud->callback_before_insert(array($this,'ainsert'));
            $crud->callback_before_update(array($this,'ainsert'));
            $crud->set_relation('tipo_doc','tipodocumento',"denominacion");
            if($crud->getParameters()!='list'){
                $habid = is_numeric($x)?$x.'/':'';
                $crud->set_lang_string('insert_success_message','Su reserva se ha almacenado <script>document.location.href="'.base_url('habitacion/reserva/reservas_habitaciones/{id}/'.$habid.'add').'";</script>');
                $crud->set_relation('clientes_id','clientes',"{nro_documento} {nombres} {apellidos}");
            }
            $action = $crud->getParameters();
            $crud = $crud->render();
            if($action!='list'){
                if($action=='edit' && is_numeric($y)){
                    $this->as['reservas'] = 'reservas_habitaciones';
                    $habitaciones = $this->reservas_habitaciones($y,'get_header');
                    $habitaciones->unset_read()->unset_print()->unset_export();
                    $habitaciones->set_url('habitacion/reserva/reservas_habitaciones/'.$y.'/');
                    $habitaciones = $habitaciones->render(1);   
                    $crud->js_files = array_merge($crud->js_files,$habitaciones->js_files);
                    $habitaciones = $habitaciones->output;
                }else{
                    $habitaciones = '<div class="alert alert-warning">Primero ingrese la ficha para poder asignar las habitaciones</div>';
                }
                $crud->output = $this->load->view('crud_habitaciones',array('output'=>$crud->output,'id'=>$y,'accion'=>$action,'habitaciones'=>$habitaciones),TRUE);
            }else{
                $crud->output = $this->load->view('listado_reservas',array('output'=>$crud->output),TRUE);
            }

            $this->loadView($crud);
        }
        
        public function ainsert($post){
            $this->db = get_instance()->db;
            if(empty($post['clientes_id'])){
                $user = $this->db->get_where('clientes',array('email'=>$post['email']));
                if($user->num_rows()==0){
                    $this->db->insert('clientes',
                            array(
                                'nombres'=>$post['nombre'],
                                'apellidos'=>$post['apellido'],
                                'celular1'=>$post['telefono'],
                                'email'=>$post['email'],
                                'tipo_doc'=>$post['tipo_doc_id'],
                                'nro_documento'=>$post['nro_documento'],
                                'direccion'=>$post['direccion'],
                                'created'=>date("Y-m-d"),
                                'modified'=>date("Y-m-d")
                            ));
                    $userid = $this->db->insert_id();
                }else{
                    $userid = $user->row()->id;
                    $this->db->update('clientes',
                            array(
                                'nombres'=>$post['nombre'],
                                'apellidos'=>$post['apellido'],
                                'celular1'=>$post['telefono'],
                                'tipo_doc'=>$post['tipo_doc'],
                                'nro_documento'=>$post['nro_documento'],
                                'direccion'=>$post['direccion'],
                                'modified'=>date("Y-m-d"),
                            ),array('id'=>$userid));
                }
                $post['clientes_id'] = $userid;
            }else{
                $user = $this->db->get_where('clientes',array('id'=>$post['clientes_id']))->row();
                 $post['nombre'] = $user->nombres;
                 $post['apellido'] = $user->apellidos;
                 $post['telefono'] = $user->celular1;
                 $post['direccion'] = $user->direccion;
                 $post['tipo_doc'] = $user->tipo_doc;
                 $post['nro_documento'] = $user->nro_documento;
            }
            //Si se recibio un abono se pone, pagado = 1
            if(is_numeric($post['abonado']) && $post['abonado']>0){
                $post['pagado'] = 1;
            }
            return $post;
        }
        
        function reservas_habitaciones($x = "",$y = ''){
            if(is_numeric($x)){                
                $crud = $this->crud_function('','');                   
                $crud->field_type('reservas_id','hidden',$x);
                if(is_numeric($x) && is_numeric($y)){
                    $crud->field_type('habitaciones_id','hidden',$y);
                }else{
                    $crud->set_relation('habitaciones_id','habitaciones','habitacion_nombre');
                }
                $crud->where('reservas_id',$x);
                $crud->set_subject('habitaciones');                
                $crud->display_as('habitaciones_id','Habitacion');                        
                $crud->display_as('reservas_id','#Reserva');    
                $crud->set_rules('desde','DESDE','required|callback_not_has_reserved');
                $crud->set_lang_string('insert_success_message','Su habitacion se ha almacenado <script>document.location.href="'.base_url('habitacion/reserva/reservas/edit/'.$x).'";</script>');
                $crud->callback_after_insert(array($this,'ainserth'));
                //$crud->callback_after_update(array($this,'ainserth'));
                $crud->unset_edit()->unset_read()->unset_print()->unset_export();
                if($y!='get_header'){
                    $crud = $crud->render();
                    $crud->output = $this->load->view('crud_detalles',array('output'=>$crud->output),TRUE);
                    $this->loadView($crud);
                }else{
                    return $crud;
                }
            }
        }

        function modReservas(){            
            $crud = new ajax_grocery_crud();
            $crud->set_table('reservas_habitaciones');
            $crud->set_theme('bootstrap2');
            $crud->set_subject('reserva');
            $crud->callback_after_update(array($this,'ainserth'));
            $crud->unset_add()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_list();
            $crud = $crud->render();
            $this->loadView($crud);            
        }
        
        function not_has_reserved(){
            $this->load->model('querys');         
            $tienereserva = $this->querys->hasReserva($_POST['habitaciones_id'],$_POST['desde'],$_POST['hasta']);            
            if($tienereserva){
                $this->form_validation->set_message('not_has_reserved','Habitación no disponible para la fecha solicitada ');
                return true;
            }            
            $this->form_validation->set_message('not_has_reserved','Habitación no disponible para la fecha solicitada');
            return false;
        }
        
        function ainserth($post,$primary){
            $this->db = get_instance()->db;            
            $reserva = $this->db->get_where('reservas',array('id'=>$post['reservas_id']))->row();
            $precio = $this->db->query('select SUM(precio) as total from reservas_habitaciones where reservas_id = '.$reserva->id)->row()->total;
            /*$precio += !empty($post['desayuno'])?$post['adultos']*5:0;
            $precio += !empty($post['desayuno'])?$post['infantes']*5:0;*/
            $this->db->update('reservas',array(
                'precio'=>$precio,
                'desde'=>$this->db->query('select MAX(desde) as desde from reservas_habitaciones where reservas_id = '.$post['reservas_id'])->row()->desde,
                'hasta'=>$this->db->query('select MAX(hasta) as hasta from reservas_habitaciones where reservas_id = '.$post['reservas_id'])->row()->hasta,
            ),array('id'=>$post['reservas_id']));
            //Ver si tiene replica
            $habitacion = $this->db->get_where('habitaciones',array('id'=>$post['habitaciones_id']))->row();
            if(!empty($habitacion->comparte_con)){
                $data = $post;
                $data['precio'] = 0;
                $data['habitaciones_id'] = $habitacion->comparte_con;
                $data['desde'] = date("Y-m-d",strtotime(str_replace('/','-',$post['desde'])));
                $data['hasta'] = date("Y-m-d",strtotime(str_replace('/','-',$post['hasta'])));
                $this->db->delete('reservas_habitaciones',array('habitaciones_id'=>$habitacion->comparte_con,'precio'=>0,'reservas_id'=>$post['reservas_id']));
                $this->db->insert('reservas_habitaciones',$data);
            }
        }
        
        public function pagos(){
            $crud = $this->crud_function('','');   
            $crud->set_relation('reservas_id','reservas','id');
            $crud->display_as('reservas_id','#Reserva');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function recordatorios(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function ofertas(){
            $crud = $this->crud_function('','');      
            $crud->set_relation('habitaciones_id','habitaciones','habitacion_nombre');
            $crud->field_type('detalle','tags');
            $crud->set_field_upload('fondo','img/entorno');
            $crud->columns('titulo','subtitulo','precio');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function ventas($reserva){
            if(is_numeric($reserva)){
                $this->db->select('reservas.*,reservas_id, habitaciones.habitacion_nombre, habitaciones.precio_desde,  reservas_habitaciones.habitaciones_id, reservas_habitaciones.dias, reservas_habitaciones.precio');
                $this->db->join('reservas','reservas.id = reservas_habitaciones.reservas_id');
                $this->db->join('habitaciones','habitaciones.id = reservas_habitaciones.habitaciones_id');
                $reserva = $this->db->get_where('reservas_habitaciones',array('reservas_id'=>$reserva));
                
                //Concat Restaurant
                foreach($reserva->result() as $n=>$r){
                    $this->db->join('productos','pedidos_detalles.productos_id = productos.id');
                    $this->db->join('pedidos','pedidos_detalles.pedidos_id = pedidos.id');
                    $this->db->where('habitaciones_id = '.$r->habitaciones_id.' AND (facturado = 0 OR facturado IS NULL) AND (liquidar IS NULL OR liquidar = 0)',NULL,TRUE);
                    $restaurant = $this->db->get_where('pedidos_detalles');
                    $reserva->row($n)->pedido = $restaurant;
                }
                if($reserva->num_rows()>0){
                    $iva_habitaciones = $this->db->get_where('productos',array('id'=>4))->row()->iva_id;
                    $output = $this->load->view('facturar',array('reserva'=>$reserva,'iva_habitaciones'=>$iva_habitaciones),TRUE);
                    $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$output));
                }else{
                    redirect('habitacion/reservas');
                }
            }else{
                redirect('habitacion/reservas');
            }
        }
        
    }
?>
