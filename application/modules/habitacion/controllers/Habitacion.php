<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Habitacion extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function habitaciones(){
            $crud = $this->crud_function('','');
            $crud->field_type('servicios','tags');
            $crud->field_type('vistas','tags');
            $crud->field_type('desayuno','checkbox');
            $crud->display_as('precio_desde','Precio/Noche')
                 ->display_as('max_personas','Adults')
                 ->display_as('max_infantes','Nens')
                 ->display_as('max_bebes','Nadons');
            $crud->set_field_upload('foto','img/habitaciones');
            $crud->set_field_upload('foto_main','img/habitaciones');
            $crud->columns('foto','habitacion_nombre','precio_desde');
            $crud->add_action('<i class="fa fa-image"></i> Banner','',base_url().'habitacion/habitaciones_fotos/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function habitaciones_fotos(){
            $this->load->library('image_crud');
            
            $crud = new image_CRUD();
            $crud->set_table('habitaciones_fotos')
                 ->set_url_field('foto')
                 ->set_relation_field('habitaciones_id')
                 ->set_ordering_field('priority')
                 ->set_image_path('img/habitaciones');
            $crud->module = 'habitacion';
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
