<?php 
    require_once APPPATH.'controllers/Main.php';    
    class ReservaFrontend extends Main{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }
        
        
        function iniciar_oferta($id){
            if(is_numeric($id)){
                $oferta = $this->db->get_where('ofertas',array('id'=>$id));
                if($oferta->num_rows()>0){
                    $oferta = $oferta->row();
                    $_GET = array(
                        'room-type'=>$oferta->cantidad_personas,
                        'guest'=>$oferta->cantidad_infantes,
                        'bebes'=>$oferta->cantidad_bebes
                    );
                    $this->loadView(array('title'=>'Iniciar Reservas','view'=>'iniciar-oferta','bodyclass'=>'booking','reservas'=>$this->querys->get_reservas(1),'oferta'=>$oferta));
                }else{
                    redirect(base_url());
                }
            }else{
                redirect(base_url());
            }
        }
        
        function iniciar_reserva($ajax = ''){            
            if(!empty($ajax)){
                echo json_encode($this->querys->get_reservas($ajax,true));
            }else{
                $this->loadView(array('title'=>'Iniciar Reservas','view'=>'iniciar-reserva','bodyclass'=>'booking','reservas'=>$this->querys->get_reservas(1)));
            } 
        }
        
        function habitacion($ajax = ''){
            if(isset($_GET['desayuno']) && isset($_GET['bebes']) && isset($_GET['room-type']) && isset($_GET['guest']) && is_numeric($_GET['room-type']) && is_numeric($_GET['guest']) && !empty($_GET['start']) && !empty($_GET['end'])){
                $this->load->model('querys');
                $this->loadView(array('title'=>'Seleccio de habitacio','view'=>'selHabitacion','bodyclass'=>'booking','reservas'=>$this->querys->get_reservas(1)));
            }else{
                $_SESSION['msj'] = "sis plau completi les dades solicitades per realitzar la seva solicitut";
                redirect('iniciar-reserva');
            }
        }
        
        public function confirmar(){
            if(isset($_GET['desayuno']) && empty($_GET['oferta']) && isset($_GET['room-type']) && isset($_GET['guest']) && is_numeric($_GET['room-type']) && is_numeric($_GET['guest']) && !empty($_GET['start']) && !empty($_GET['end']) && !empty($_GET['habitacion']) && is_numeric($_GET['habitacion'])){
                $habitacion = $this->db->get_where('habitaciones',array('id'=>$_GET['habitacion']));
                if($habitacion->num_rows()>0){
                    $datetime1 = new DateTime(date("Y-m-d",strtotime($_GET['start'])));
                    $datetime2 = new DateTime(date("Y-m-d",strtotime($_GET['end'])));
                    $interval = $datetime1->diff($datetime2);
                    $dias = $interval->format('%a días');
                    if(empty($_SESSION['reserva'])){
                        $_SESSION['reserva'] = array();
                    }
                     
                    $_SESSION['reserva'][] = array('desayuno'=>$_GET['desayuno'],'bebes'=>$_GET['bebes'],'habitacion'=>$habitacion->row(),'desde'=>$_GET['start'],'hasta'=>$_GET['end'],'dias'=>$dias,'adultos'=>$_GET['room-type'],'infantes'=>$_GET['guest']);
                    redirect('reservar/confirmar');         
                }else{
                    throw new Exception("La seva reserva no s'ha pogut fer amb éxit");
                }
            }elseif(!empty($_GET['oferta']) && is_numeric($_GET['oferta'])){
                $oferta = $this->db->get_where('ofertas',array('id'=>$_GET['oferta']));
                if($oferta->num_rows()>0){
                    $oferta = $oferta->row();
                    $habitacion = $this->db->get_where('habitaciones',array('id'=>$oferta->habitaciones_id));
                    $dias = $oferta->cantidad_noches;
                    //Cambiamos el precio
                    $habitacion->precio_desde = $oferta->precio;                    
                    $_SESSION['reserva'][] = array('oferta'=>$oferta,'bebes'=>$_GET['bebes'],'habitacion'=>$habitacion->row(),'desde'=>$_GET['start'],'hasta'=>$_GET['end'],'dias'=>$dias,'adultos'=>$_GET['room-type'],'infantes'=>$_GET['guest']);
                    redirect('reservar/confirmar');
                }else{
                    redirect('iniciar-reserva');
                }
            }elseif(!empty($_SESSION['reserva'])){
                $desde = date("Y-m-d");
                $hasta = '1969-'.date("m-d");
                $n = 0;
                foreach($_SESSION['reserva'] as $r){ 
                    if($n==0){
                        $desde = $r['desde'];
                        $n = 1;
                    }                    
                    if(strtotime($desde)>=strtotime($r['desde'])){
                        $desde = date("d-m-Y",strtotime($r['desde']));
                    }
                }
                foreach($_SESSION['reserva'] as $r){
                    if(strtotime($hasta)<strtotime($r['hasta'])){
                        $hasta = date("d-m-Y",strtotime($r['hasta']));
                    }
                }
                $this->loadView(array('title'=>'Confirmar la teu Reserva','view'=>'confirmar_reserva','reserva'=>$_SESSION['reserva'],'desde'=>$desde,'hasta'=>$hasta));                
            }else{
                redirect('iniciar-reserva');
            }
        }
        
        
        function remove($id){
            if(is_numeric($id)){
                if(isset($_SESSION['reserva'][$id])){
                    unset($_SESSION['reserva'][$id]);
                }
            }
            redirect('reservar/confirmar');
        }
        
        function reservar(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('apellido','Apellido','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('telefono','Telefono','required');
            $this->form_validation->set_rules('terms','Terminos','required');
            //$this->form_validation->set_rules('cond','Condiciones','required');
            $this->form_validation->set_rules('abono','Abono','required');
            $this->form_validation->set_rules('total','Total','required|numeric'); 
            if($this->form_validation->run()){
                
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    $_SESSION['msj'] = $this->error('Captcha introduit incorrectament');
                    redirect('reservar/confirmar');
                }else{
                    $_POST['abono'] = str_replace(',','.',$_POST['abono']);
                    $ajustes = $this->db->get('ajustes')->row();
                    //Sacamos el id del usuario
                    $user = $this->db->get_where('clientes',array('email'=>$_POST['email']));
                    if($user->num_rows()==0){
                        $this->db->insert('clientes',
                                array(
                                    'nombres'=>$_POST['nombre'],
                                    'apellidos'=>$_POST['apellido'],
                                    'celular1'=>$_POST['telefono'],
                                    'email'=>$_POST['email'],
                                    'tipo_doc'=>$_POST['tipo_doc_id'],
                                    'nro_documento'=>$_POST['nro_documento'],
                                    'direccion'=>$_POST['direccion'],
                                    'created'=>date("Y-m-d"),
                                    'modified'=>date("Y-m-d")
                                ));
                        $userid = $this->db->insert_id();
                    }else{
                        $userid = $user->row()->id;
                        $this->db->update('clientes',
                                array(
                                    'nombres'=>$_POST['nombre'],
                                    'apellidos'=>$_POST['apellido'],
                                    'celular1'=>$_POST['telefono'],
                                    'tipo_doc'=>$_POST['tipo_doc_id'],
                                    'nro_documento'=>$_POST['nro_documento'],
                                    'direccion'=>$_POST['direccion'],                                    
                                    'modified'=>date("Y-m-d")
                                ),array('id'=>$userid));
                    }
                    echo $userid;
                    //Añadimos la reserva
                    $this->db->insert('reservas',
                            array(
                                'user_id'=>$userid,
                                'fecha_reserva'=>date("Y-m-d"),
                                'desde'=>date("Y-m-d",strtotime($_POST['desde'])),
                                'hasta'=>date("Y-m-d",strtotime($_POST['hasta'])),
                                'nombre'=>$_POST['nombre'],
                                'apellido'=>$_POST['apellido'],
                                'telefono'=>$_POST['telefono'],
                                'email'=>$_POST['email'],
                                'precio'=>$_POST['total'],
                                'abonado'=>$_POST['abono'],
                                'requerimiento'=>$_POST['requerimiento'],
                                'pagado'=>0
                            ));
                    $reservaid = $this->db->insert_id();
                    //Añadimos detalle
                    foreach($_SESSION['reserva'] as $r){
                        $insertdata = array(
                            'reservas_id'=>$reservaid,
                            'habitaciones_id'=>$r['habitacion']->id,
                            'desde'=>date("Y-m-d",strtotime($r['desde'])),
                            'hasta'=>date("Y-m-d",strtotime($r['hasta'])),
                            'precio'=>($r['habitacion']->precio_desde*(int)$r['dias'])+($r['bebes']*$ajustes->precio_bebe)+($r['infantes']*$ajustes->precio_infante),
                            'adultos'=>$r['adultos'],
                            'bebes'=>$r['bebes'],
                            'dias'=>$r['dias'],
                            'infantes'=>$r['infantes'],
                            'desayuno'=>$r['desayuno']
                        );
                        $this->db->insert('reservas_habitaciones',$insertdata);
                        if(!empty($r['habitacion']->comparte_con)){
                            $insertreplica = $insertdata;
                            $insertreplica['habitaciones_id'] = $r['habitacion']->comparte_con;
                            $insertreplica['precio'] = 0;
                            $this->db->insert('reservas_habitaciones',$insertreplica);
                        }
                    }
                    unset($_SESSION['reserva']);
                    //redirect('reserva_realizada/'.$reservaid);
                    if(!empty($_POST['cond']) && $this->db->get_where('subscritos',array('email'=>$_POST['email']))->num_rows()==0){
                        $this->db->insert('subscritos',array('email'=>$_POST['email']));
                    }
                    redirect('habitacion/reservaFrontend/gotpv/'.$reservaid);
                }
            }else{
                $_SESSION['msj'] = $this->form_validation->error_string();
                redirect('reservar/confirmar');
            }
        }
        
        function gotpv($reservaid){
            if(is_numeric($reservaid)){
                $reserva = $this->db->get_where('reservas',array('id'=>$reservaid,'pagado'=>0));
                if($reserva->num_rows()>0){
                    $this->load->view('tpv',array('reserva'=>$reserva->row()));    
                }else{
                    //redirect('iniciar-reserva');
                }
            }else{  
                //redirect('iniciar-reserva');
            }
        }
        
        function reserva_realizada($reserva = ''){
            if(is_numeric($reserva)){
                $reserva = $this->db->get_where('reservas',array('id'=>$reserva));
                if($reserva->num_rows()>0){
                    $reserva = $reserva->row();                    
                    $reserva->habitaciones = $this->db->get_where('reservas_habitaciones',array('reservas_id'=>$reserva->id));
                    foreach($reserva->habitaciones->result() as $n=>$h){
                        $reserva->habitaciones->row($n)->habitacion = $this->db->get_where('habitaciones',array('id'=>$h->habitaciones_id))->row();
                    }
                    $this->loadView(array('title'=>'Reserva realitzada','view'=>'reserva_realizada','reserva'=>$reserva));
                }else{
                    redirect('iniciar-reserva');
                }
                
            }else{  
                redirect('iniciar-reserva');
            }
        }
        
        function pagoKo(){
            $this->loadView(array('title'=>'Error en la teu Reserva','view'=>'reserva_no_realizada'));
        }
        
        function procesar($reserva){            
            if(is_numeric($reserva)){
                $reserva = $this->db->get_where('reservas',array('id'=>$reserva));
                if($reserva->num_rows()>0){
                    $this->load->library('redsysapi');
                    $miObj = new RedsysAPI; 
                    $this->db->select('reservas_habitaciones.*, habitaciones.*, reservas.email, reservas.nombre, reservas.apellido, reservas.abonado, reservas.precio as total');
                    $this->db->join('habitaciones','habitaciones.id = reservas_habitaciones.habitaciones_id');
                    $this->db->join('reservas','reservas.id = reservas_habitaciones.reservas_id');                    
                    $_user = $this->db->get_where('reservas_habitaciones',array('reservas_id'=>$reserva->row()->id));
                    $user = $_user->row();
                    $user->reserva = $this->get_reserva_message($_user);
                    //$_POST = (array)json_decode('{"Ds_SignatureVersion":"HMAC_SHA256_V1","Ds_Signature":"O5HrW7_oQhwvqXmegTe8jRU7Z2DUZioahrb7HNbQhCE=","Ds_MerchantParameters":"eyJEc19EYXRlIjoiMTJcLzEyXC8yMDE3IiwiRHNfSG91ciI6IjIwOjI5IiwiRHNfU2VjdXJlUGF5bWVudCI6IjEiLCJEc19DYXJkX0NvdW50cnkiOiI3MjQiLCJEc19BbW91bnQiOiI4NzAwIiwiRHNfQ3VycmVuY3kiOiI5NzgiLCJEc19PcmRlciI6IjEyMTIwODI4NTcyNSIsIkRzX01lcmNoYW50Q29kZSI6IjM0NDUxMTk4NSIsIkRzX1Rlcm1pbmFsIjoiMDAxIiwiRHNfUmVzcG9uc2UiOiIwMDAwIiwiRHNfTWVyY2hhbnREYXRhIjoiIiwiRHNfVHJhbnNhY3Rpb25UeXBlIjoiMCIsIkRzX0NvbnN1bWVyTGFuZ3VhZ2UiOiIxIiwiRHNfQXV0aG9yaXNhdGlvbkNvZGUiOiI1NzgxNjQiLCJEc19DYXJkX0JyYW5kIjoiMSJ9"} ');
                    if (!empty($_POST)){
                        //correo('joncar.c@gmail.com','POST',json_encode($_POST));
                        $datos = $_POST["Ds_MerchantParameters"];
                        $decodec = json_decode($miObj->decodeMerchantParameters($datos));                
                        if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){                    
                            //Aprobado
                            $this->db->update('reservas',array('pagado'=>1),array('id'=>$reserva->row()->id));
                            $this->enviarcorreo($user, 2,'info@calfusterdetous.com');
                            $this->enviarcorreo($user, 3);
                        }else{
                            /*$this->db->delete('reservas',array('id'=>$reserva->row()->id));
                            $this->db->delete('reservas_habitaciones',array('habitaciones_id'=>$reserva->row()->id));*/
                            $this->enviarcorreo($user, 4,'info@calfusterdetous.com');
                            $this->enviarcorreo($user, 4);
                        }                                                
                    }else{
                        $this->enviarcorreo($user, 4,'info@calfusterdetous.com');
                        $this->enviarcorreo($user, 4);
                    }
                }   
            }
        }
        
        function get_reserva_message($user){
            $str = '
            <table width="540" height="35" style="border:1px solid gray">
            <tbody>
            <tr style="border:1px solid gray">
            <td style="width: 480px; text-align: center;"><strong>Habitaci&oacute;n</strong></td>
            <td style="width: 60px; text-align: center;"><strong>Precio</strong></td>
            </tr>';
            foreach($user->result() as $u){
            $str.= '
                <tr>
                    <td style="border-right:1px solid gray">
                        '.img('img/habitaciones/'.$u->foto,'vertical-align:middle; width:80px').'
                        <span style="vertical-align:middle">
                            '.$u->habitacion_nombre.' Por '.$u->dias.' Noches 
                            '.date("d/m/Y",strtotime($u->desde)).' - '.date("d/m/Y",strtotime($u->hasta)).'
                        </span>
                    </td>
                    <td style="text-align:right">'.str_replace('.00','',$u->precio).' €</td>
                </tr>';
            }
            $porcentaje = $u->abonado!=$u->total?'50%':'100%';
            $pendiente = $u->total-$u->abonado;
            $str.= '
            <tr>
                <td style="border-top:1px solid gray; width: 480px; text-align: left;">Cantitat adultos</td>
                <td style="border-top:1px solid gray; width: 60px; text-align: right;">'.$u->adultos.'</td>
            </tr>
            <tr>
                <td style="border-top:1px solid gray; width: 480px; text-align: left;">Cantitat Nens</td>
                <td style="border-top:1px solid gray; width: 60px; text-align: right;">'.$u->infantes.'</td>
            </tr>
            <tr>
                <td style="border-top:1px solid gray; width: 480px; text-align: left;">Cantitat nadons</td>
                <td style="border-top:1px solid gray; width: 60px; text-align: right;">'.$u->bebes.'</td>
            </tr>
            <tr>
                <td style="border-top:1px solid gray; width: 480px; text-align: left;">Preu per esmorzar</td>
                <td style="border-top:1px solid gray; width: 60px; text-align: right;">'.str_replace('.00','',($u->adultos*5)).'€</td>
            </tr>
            <tr>
                <td style="border-top:1px solid gray; width: 480px; text-align: left;">Abono ('.$porcentaje.')</td>
                <td style="border-top:1px solid gray; width: 60px; text-align: right;">'.str_replace('.00','',$u->abonado).'€</td>
            </tr>
            <tr style="border:1px solid gray">
                <td style="width: 480px; text-align: left;">Pendiente por cancelar</td>
                <td style="width: 60px; text-align: right;">'.str_replace('.00','',$pendiente).'€</td>
            </tr>
            <tr>
                <td style="border-top:1px solid gray; width: 480px; text-align: left;"><strong>Total Reserva</strong></td>
                <td style="border-top:1px solid gray; width: 60px; text-align: right;"><strong>'.str_replace('.00','',$u->total).'€</strong></td>
            </tr>';
            $str.= '</tbody>
            </table>
            <p></p>';
            return $str;
        }
        
    }
?>
