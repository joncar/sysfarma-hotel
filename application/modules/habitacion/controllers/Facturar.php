<?php

require_once APPPATH . '/controllers/Panel.php';

class Facturar extends Panel {

    function __construct() {
        parent::__construct();
        $this->load->model("querys");
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }            
    }
    //Una vez realizado la vista de carga de facturas con sus operaciones logicas, usar esta funcion para almacenar las ventas
    function ventas($x, $unset_delete = FALSE){        
        $crud = new ajax_grocery_CRUD($this);
        $crud->set_table("ventas");
        $crud->set_subject("Facturas");
        $crud->set_theme("bootstrap2");
        $crud->required_fields_array();
        $crud->set_rules('cliente','Cliente','required');
        $crud->set_rules('transaccion','Transaccion','required');
        $crud->set_rules('fecha','Fecha','required');
        $crud->set_rules('forma_pago','Forma de pago','required');
        $crud->set_rules('nro_factura','#Factura','required|is_unique[ventas.nro_factura]|callback_validate_pago');
        $crud->callback_after_insert(function($post,$primary){            
            $codigos = $_POST['codigo'];
            $cantidades = $_POST["cantidad"];
            $totales = $_POST['total'];            
            foreach($codigos as $n=>$v){  
                if(!empty($v)){
                    $data = array('venta'=>$primary);
                    $data['producto'] = $v;
                    $data['lote'] = '';
                    $data['cantidad'] = $cantidades[$n];
                    $data['pordesc'] = 0;
                    $data['precioventa'] = $totales[$n];
                    $data['precioventadesc'] = $totales[$n];
                    $data['totalsindesc'] = 0;
                    $data['totalcondesc'] = $totales[$n];
                    $data['iva'] = 0;
                    $this->db->insert('ventadetalle',$data);
                    //Sacamos los datos de la habitación
                    $producto = $this->db->get_where('productos',array('codigo'=>$v))->row();
                    if($v=='h24'){
                        $id = explode('-',$_POST['producto'][$n]);
                        if(count($id)==3 && is_numeric($id[0])){
                            $habid = $id[0];
                            //Ya tenemos la habitacion que sacamos del detalle del producto
                            $this->db->select('pedidos.id');
                            $this->db->join('productos','pedidos_detalles.productos_id = productos.id');
                            $this->db->join('pedidos','pedidos_detalles.pedidos_id = pedidos.id');
                            $this->db->where('habitaciones_id = '.$habid.' AND (facturado = 0 OR facturado IS NULL) AND (liquidar IS NULL OR liquidar = 0)',NULL,TRUE);
                            $restaurant = $this->db->get_where('pedidos_detalles');
                            //Liquidar
                            foreach($restaurant->result() as $p){
                                get_instance()->db->update("pedidos_detalles",array("liquidar"=>1),array('pedidos_id'=>$p->id));
                                get_instance()->db->update("pedidos",array("facturado"=>1),array('id'=>$p->id));
                                get_instance()->db->update("ventas",array("pedidos_id"=>$p->id),array('id'=>$primary));
                            }
                            //Habitacion ya pagada
                            $precio = $this->db->get_where('reservas',array('id'=>$_POST['reserva']))->row()->precio;
                            get_instance()->db->update("reservas",array("pagado"=>1,'abonado'=>$precio,'ventas_id'=>$primary),array('id'=>$_POST['reserva']));
                        }
                    }
                }
                $caja = $this->db->get_where('cajadiaria',array('id'=>$_SESSION['cajadiaria']));
                $correlativo = $caja->row()->correlativo+1;
                $this->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1)); 
                //Se actualiza el stock
                if(!$producto->no_inventariable){
                    $producto->cantidad = $cantidades[$n];
                    get_instance()->actualizar_stock($producto);
                }
                $id = $primary;
                $_SESSION['msj'] = 'Se han guardado los datos con exito <a href="javascript:imprimir(\''.$id.'\')">Imprimir Factura</a> | <a href="javascript:imprimir2(\''.$id.'\')">Imprimir Factura | Lineas</a> | <a href="javascript:imprimirTicket(\''.$id.'\')">Imprimir Ticket</a> <script>$(document).ready(function(){showButton('.$id.')});</script>';
            }
        });
        $crud = $crud->render();
    }
    
    function validate_pago(){
        $pago = $_POST['total_efectivo'];
        $pago+= $_POST['total_debito'];
        $pago+= $_POST['total_credito'];
        $pago+= $_POST['total_cheque'];
        if($pago<$_POST['total_venta']){
            $this->form_validation->set_message("validate_pago","Por favor asegurese de cargar el pago de la factura");
            return false;
        }
        return true;
    }
    
    function actualizar_stock($post){
        //Actualizar stock
        $this->db->select("productosucursal.*");
        $this->db->join("productos","productos.codigo = productosucursal.producto");
        $where = array('productos.id'=>$post->id,'productosucursal.sucursal'=>$_SESSION['sucursal'],'productosucursal.stock >'=>0,'productos.no_inventariable'=>0);        
        $stock = $this->db->get_where('productosucursal',$where);
        $st = $post->cantidad;        
        foreach($stock->result() as $s){  
            if($st<0)$st = $st*-1;
            $st = $s->stock-$st;
            $st2 = $st<0?0:$st;
            $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$s->id));                            
        }
    }
    
    function reversar_stock($post){
        $post = (array)$post;
        //Actualizar stock
        $this->db->select("productosucursal.*");
        $this->db->join("productos","productos.codigo = productosucursal.producto");
        $where = array('productos.id'=>$post['productos_id'],'productosucursal.sucursal'=>$_SESSION['sucursal']);
        $stock = $this->db->get_where('productosucursal',$where);
        $st = $post['cantidad'];
        if($stock->num_rows()>0){
            $st2 = $stock->row()->stock+$st;
            $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$stock->row()->id));
        }
    }

}

?>
