<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="../assets/img/breadcrumb.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Reserva no realitzada</div>
                <div class="sub-title">la seva reserva no s'ha realitzat amb èxit</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="current"><a href="#">Reserva</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<section id="booking-section" class="step-3">
    <div class="inner-container container">
        
        <div class="col-md-12 r-sec">
            <div class="inner-box">
                <div class="steps">
                    <ul class="list-inline">
                        <li>Tria data</li>
                        <li>Tria habitació</li>
                        <li>Fer la reserva</li>
                        <li class="active">Confirmació</li>
                    </ul>
                </div>

                <div id="confirmation-message">
                    <div class="ravis-title-t-2">
                        <div class="title"><span>Reservation no Complete</span></div>
                        <div class="sub-title">Su reserva no se ha podido realizar con éxito, existe un error con su tarjeta o el banco emisor no aprobó esta transacción </div>
                    </div>
                    <div class="desc">
                        For more information you can contact us via <a href="<?= site_url('p/contacte') ?>">contact form</a> of website
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
