<?php if(!empty($list)): ?>    
    <?php foreach($list as $num_row => $p): ?>
        <div class="room-box row animated-box" data-animation="fadeInUp">
            <div class="col-md-4 room-img" data-bg-img="<?= base_url() ?>img/habitaciones/<?= $p->foto ?>" style="background-repeat: no-repeat;">
                <a href="<?= base_url('habitacion/'.toURL($p->id.'-'.$p->habitacion_nombre)) ?>" class="more-info-url"></a>
            </div>
            <div class="r-sec col-md-8">
                <div class="col-md-6 m-sec">
                    <div class="title-box">
                        <div class="title"><?= $p->habitacion_nombre ?></div>
                        <div class="price">
                            <div class="title">Des de:</div>
                            <div class="value"><?= str_replace('.00','',$p->precio_desde) ?>€</div>
                        </div>
                    </div>
                    <div class="amenities">
                        <ul class="list-inline clearfix">
                            <li class="col-md-6">
                                <div class="title">Esmorzar:</div>
                                <div class="value"><?= $p->desayuno?'Opcional':'Opcional' ?></div>
                            </li>
<!--                            <li class="col-md-6">-->
<!--                                <div class="title">Tamany:</div>-->
<!--                                <div class="value">--><?//= $p->tamano ?><!--</div>-->
<!--                            </li>-->
                            <li class="col-md-6">
                                <div class="title">Adults:</div>
                                <div class="value"><?= $p->max_personas ?></div>
                            </li>
                            <li class="col-md-6">
                                <div class="title">Nen +30€:</div>
                                <div class="value"><?= $p->max_infantes ?></div>
                            </li>
                            <li class="col-md-6">
                                <div class="title">Nadó +10€:</div>
                                <div class="value"><?= $p->max_bebes ?></div>
                            </li>
                            <li class="col-md-12">
                                <div class="title">Ambientació:</div>
                                <div class="value"><?= $p->vistas ?></div>
                            </li>
                            <li class="col-md-12">
                                <div class="title">Serveis:</div>
                                <div class="value"><?= $p->servicios ?></div>
                            </li>
                        </ul>
                    </div>
                    <a href="<?= base_url('habitacion/'.toURL($p->id.'-'.$p->habitacion_nombre)) ?>" class="more-info">Més informació</a>
                </div>
                <div class="col-md-6 desc">
                    <?= $p->historia ?>
                </div>
            </div>
        </div>
    <?php endforeach ?>    
<?php else: ?>
    No hi ha habitacions disponibles
<?php endif; ?>
