<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/maletas.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Iniciar la Reserva</div>
                <div class="sub-title">Comproba la disponibilitat de les habitacions</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Reserva</a></li>
                <li class="current"><a href="">Iniciar reserva</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<section id="booking-section" class="step-1">
    <div class="inner-container container">
        <div class="col-md-4 l-sec">
            <div class="ravis-title-t-2">
                <div class="title"><span>Informació de la Reserva</span></div>
            </div>

            <div class="field-row room-field" id="room-field-tmpl">                
                <?php $sel = empty($_GET['room-type'])?'':$_GET['room-type']; ?>
                <?= form_dropdown('room-type',array(''=>'Nº Adults','1'=>'1','2'=>'2'),$sel,'class="adult-field disable-select2"'); ?>
                <?php $sel = empty($_GET['guest'])?'':$_GET['guest']; ?>
                <?= form_dropdown('guest',array('0'=>'Nº Nens','1'=>'1','2'=>'2'),$sel,'class="adult-field disable-select2"'); ?>
                <?php $sel = empty($_GET['bebes'])?'':$_GET['bebes']; ?>
                <?= form_dropdown('bebes',array('0'=>'Nº Nadons','1'=>'1','2'=>'2'),$sel,'class="adult-field disable-select2"'); ?>
            </div>
            <form id="room-information-form" method="get" action="<?= base_url('reservar/habitacion') ?>"><!-- Do Not remove the classes -->
                <div class="input-daterange">
                    <div class="field-row check-in">
                        <input type="hidden" name="start" value='<?= empty($_GET['start'])?'':$_GET['start'] ?>'/>
                        <div class="check-in-box">
                            <div class="title">Entrada :</div>
                            <div class="value"><?= empty($_GET['start'])?'Selecciona un dia':$_GET['start']; ?></div>
                        </div>
                    </div> 
                    <div class="field-row check-out">
                        <input type="hidden" name="end" value='<?= empty($_GET['end'])?'':$_GET['end'] ?>'/>
                        <div class="check-out-box">
                            <div class="title">Sortida :</div>
                            <div class="value"><?= empty($_GET['end'])?'Selecciona un dia':$_GET['end']; ?></div>
                        </div>
                    </div>
                    <div class="field-row duration">
                        <input type="hidden" name="duration"/>
                        <div class="duration-box">
                            <div class="title">Duració :</div>
                            <div class="value">
                                <?php 
                                    if(!empty($_GET['start']) && !empty($_GET['end'])){
                                        $datetime1 = new DateTime(date("Y-m-d",strtotime($_GET['start'])));
                                        $datetime2 = new DateTime(date("Y-m-d",strtotime($_GET['end'])));
                                        $interval = $datetime1->diff($datetime2);
                                        $dias = $interval->format('%a días');
                                        echo $dias.' Nits';
                                    }
                                    else{
                                        echo 'Nº de Nits';
                                    }
                                ?>                                
                            </div>
                        </div>
                    </div>
                    <div class="field-row duration">                        
                        <div class="duration-box">
                            <div class="title">Esmorzars:</div>
                            <div style="width:50%">
                                <?= form_dropdown('desayuno',array('0'=>'NO','1'=>'SI (5€/persona)'),$sel,'class=""'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="room-field-container">
                    <?php 
                        if(!empty($_SESSION['msj'])){
                            echo "<div class='alert alert-danger'>";
                            echo $_SESSION['msj'];
                            unset($_SESSION['msj']);
                            echo "</div>";
                        }                        
                    ?>
                </div>
                <div class="field-row btn-container">
                    <input type="submit" value="Reserva Ara">
                </div>
            </form>
        </div>
        <div class="col-md-8 r-sec">
            <div class="inner-box">
                <div class="steps">
                    <ul class="list-inline">
                        <li class="active">Tria data</li>
                        <li>Tria habitació</li>
                        <li>Fer la reserva</li>
                        <li>Confirmació</li>
                    </ul>
                </div>
                <div id="booking-date-range-inline" class="clearfix">
                    <div class="check-in col-md-6" name="start" value='<?= empty($_GET['start'])?'':$_GET['start'] ?>'></div>
                    <div class="check-out col-md-6" name="end" value='<?= empty($_GET['end'])?'':$_GET['end'] ?>'></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer Section-->
<?php $this->load->view('includes/template/footer'); ?>
<!--End of Footer Section-->
<script>
    var reservas = <?php 
        $re = array();
        foreach($reservas->result() as $r){
            $datetime1 = new DateTime($r->desde);
            $datetime2 = new DateTime($r->hasta);
            $interval = $datetime1->diff($datetime2);
            $dias = $interval->format('%a días');
            for($i=0;$i<$dias;$i++){
                $fecha = date("d-m-y",strtotime("+$i days ".$r->desde));
                if(!in_array($fecha, $re)){
                    $re[] = $fecha;
                }
            }
        }
        
        //Concatenamos las reservas locales
        if(!empty($_SESSION['reserva'])){
            foreach($_SESSION['reserva'] as $r){
                $datetime1 = new DateTime($r['desde']);
                $datetime2 = new DateTime($r['hasta']);
                $interval = $datetime1->diff($datetime2);
                $dias = $interval->format('%a días');
                for($i=0;$i<$dias;$i++){
                    $fecha = date("d-m-y",strtotime("+$i days ".$r['desde']));
                    if(!in_array($fecha, $re)){
                        $re[] = $fecha;
                    }
                }
            }
        }
        echo json_encode($re);
    ?>;
</script>
