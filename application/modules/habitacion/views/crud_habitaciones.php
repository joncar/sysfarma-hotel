<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="<?= $accion!='edit'?'active':'' ?>"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Ficha</a></li>
    <li role="presentation" class="<?= $accion=='edit'?'active':'' ?>"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Habitaciones</a></li>
    <?php if($accion=='edit'): ?>
        <li><a href="<?= base_url("habitacion/reserva/ventas/".$id) ?>">Facturar</a></li>
    <?php endif ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?= $accion!='edit'?'active':'' ?>" id="home"><?= $output ?></div>
    <div role="tabpanel" class="tab-pane <?= $accion=='edit'?'active':'' ?>" id="profile"><?= $habitaciones ?></div>
  </div>

</div>
<script>
    $(document).on('ready',function(){
        $(".chosen-container").css('width','100%');
        var datos = $("#direccion_field_box,#nombre_field_box,#apellido_field_box,#email_field_box,#telefono_field_box,#telefono_field_box,#tipo_doc_field_box,#nro_documento_field_box");
        var cliente = $("#field-clientes_id");
        cliente.on('change',function(){
            if($(this).val()!==''){
                datos.hide();
            }else{
                datos.show();
            }
        });
    });
</script>