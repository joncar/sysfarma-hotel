<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/maletas.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Iniciar la Reserva</div>
                <div class="sub-title">Comproba la disponibilitat de les habitacions</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="current"><a href="#">Reserva</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<section id="booking-section" class="step-2">
    <div class="inner-container container">
        <div class="col-md-4 l-sec">
            <div class="ravis-title-t-2">
                <div class="title"><span>Informació de la Reserva</span></div>
            </div>
            <div class="check-in-out-container">
                <div class="check-in-out-box">
                    <div class="title">Entrada :</div>
                    <div class="value"><?= strftime("%d-%m-%Y",strtotime($_GET['start'])) ?></div>
                </div>
                <div class="check-in-out-box">
                    <div class="title">Sortida :</div>
                    <div class="value"><?= strftime("%d-%m-%Y",strtotime($_GET['end'])) ?></div>
                </div>
            </div>
            <?php 
                if(!empty($_GET['start']) && !empty($_GET['end'])){
                    $datetime1 = new DateTime(date("Y-m-d",strtotime($_GET['start'])));
                    $datetime2 = new DateTime(date("Y-m-d",strtotime($_GET['end'])));
                    $interval = $datetime1->diff($datetime2);
                    $dias = $interval->format('%a');                    
                }
            ?>   
            <div class="selected-room-container">
                <div class="selected-room-box">                    
                    <div class="room-title">
                        <div class="title">Duració :</div>
                        <div class="value"><?= (int)$dias+1 ?> Dies y <?= $dias ?> Nits</div>
                    </div>
                    <div class="adult-count">
                        <div class="title">Adults :</div>
                        <div class="value"><?= $_GET['room-type'] ?></div>
                    </div>
                    <div class="child-count">
                        <div class="title">Nens :</div>
                        <div class="value"><?= $_GET['guest'] ?></div>
                    </div>
                    <div class="child-count">
                        <div class="title">Nadons :</div>
                        <div class="value"><?= $_GET['bebes'] ?></div>
                    </div>
                    <div class="child-count">
                        <div class="title">Esmorzars :</div>
                        <div class="value"><?= $_GET['desayuno']==0?'NO':'SI' ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 r-sec">
            <div class="inner-box">
                <div class="steps">
                    <ul class="list-inline">
                        <li>Tria data</li>
                        <li class="active">Tria habitació</li>
                        <li>Fer la reserva</li>
                        <li>Confirmació</li>
                    </ul>
                </div>

                <div id="booking-room-container">
                    
                    
                    <?php 
                        $habitaciones = array();
                        foreach($this->db->get_where('habitaciones')->result() as $h):
                        if($h->max_bebes>=$_GET['bebes'] && $h->max_personas>=$_GET['room-type'] && $h->max_infantes>=$_GET['guest'] && $this->querys->hasReserva($h->id,$_GET['start'],$_GET['end'])):
                            $habitaciones[] = $h;
                        endif;
                        endforeach;
                        foreach($habitaciones as $h):
                    ?>
                        <div class="room-box">
                            <div class="col-md-5 room-img" data-bg-img="<?= base_url('img/habitaciones/'.$h->foto) ?>">
                                <div class="select-room-box">
                                    <a href="<?= base_url('reservar/confirmar') ?>?desayuno=<?= $_GET['desayuno'] ?>&bebes=<?= $_GET['bebes'] ?>&habitacion=<?= $h->id ?>&end=<?= $_GET['end'] ?>&start=<?= $_GET['start'] ?>&guest=<?= $_GET['guest'] ?>&room-type=<?= $_GET['room-type'] ?>">selecciona habitació</a>
                                </div>
                            </div>
                            <div class="r-sec col-md-7">
                                <div class="title-box">
                                    <div class="title"><?= $h->habitacion_nombre ?></div>
                                    <!--<div class="price">
                                        <div class="title">Preu :</div>
                                        <div class="value"><?= $h->precio_desde ?>€</div>
                                    </div>
                                    <!--<a href="#price-break-down-1" class="price-breakdown"><i class="fa fa-caret-right"></i>Price Breakdown</a>
                                    <div id="price-break-down-1" class="price-breakdown-popup mfp-hide">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="title">Base Price</div>
                                                    <div class="duration">x 7 Nights (Weekday)</div>
                                                </td>
                                                <td class="price">$630.00</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="title">Base Price</div>
                                                    <div class="duration">x 2 Nights (Weekend)</div>
                                                </td>
                                                <td class="price">$180.00</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="title">Total</div>
                                                    <div class="duration">vat is not included yet</div>
                                                </td>
                                                <td class="price">$810.00</td>
                                            </tr>
                                        </table>
                                    </div>-->
                                </div>
                                <div class="amenities">
                                    <ul class="list-inline clearfix">
                                        <li class="col-md-6">
                                            <div class="title">Esmorzar :</div>
                                            <div class="value"><?= $h->desayuno?'Inclòs':'Opcional' ?></div>
                                        </li>
<!--                                        <li class="col-md-6">-->
<!--                                            <div class="title">Tamany :</div>-->
<!--                                            <div class="value">--><?//= $h->tamano ?><!--</div>-->
<!--                                        </li>-->
                                        <li class="col-md-6">
                                            <div class="title">Adults :</div>
                                            <div class="value"><?= $h->max_personas ?></div>
                                        </li>
                                        <li class="col-md-6">
                                            <div class="title">Nens +30€ :</div>
                                            <div class="value"><?= $h->max_infantes ?></div>
                                        </li>
                                        <li class="col-md-6">
                                            <div class="title">Nadó+10€ :</div>
                                            <div class="value"><?= $h->max_bebes ?></div>
                                        </li>
                                        <li class="col-md-12">
                                            <div class="title">Ambientació :</div>
                                            <div class="value"><?= $h->vistas ?></div>
                                        </li>
                                        <li class="col-md-12">
                                            <div class="title">Serveis :</div>
                                            <div class="value"><?= $h->servicios ?></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>                    
                    <?php endforeach ?>
                    <?php if(count($habitaciones)==0): ?>
                        No hi ha habitacions per a les dades seleccionades. <a href="<?= base_url('iniciar-reserva') ?>">tornar</a>
                    <?php endif ?>
                    
                </div>
            </div>
        </div>
    </div>

</section>
