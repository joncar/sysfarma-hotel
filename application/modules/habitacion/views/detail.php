<section id="room-top-section">
    <!-- Event Slider -->
    <section id="room-slider">
        <?php if($detail->fotos->num_rows()==0): ?>
           <div class="items">
                <div class="img-container" data-bg-img="<?= base_url() ?>img/slider/room/17.jpg"></div>
            </div>
        <?php endif ?>
        <?php foreach($detail->fotos->result() as $f): ?>
            <div class="items">
                <div class="img-container" data-bg-img="<?= base_url('img/habitaciones/'.$f->foto) ?>"></div>
            </div>
        <?php endforeach ?>
    </section>
    <!-- End of Event Slider -->
    <div class="inner-container container">
        <div class="room-title-box">
            <h1 class="title"><?= $detail->habitacion_nombre ?></h1>
            <div class="price">
                <div class="title">Des de:</div>
                <div class="value"><?= str_replace('.00','',$detail->precio_desde) ?>€</div>
            </div>
        </div>
    </div>
</section>

<section class="room-desc">
    <div class="inner-container container">
        <div class="l-sec col-md-8">
            <div class="amenities">
                <ul class="list-inline clearfix">
                    <li class="col-xs-12 col-md-6">
                        <div class="title">Esmorzar:</div>
                        <div class="value"><?= $detail->desayuno?'Inclòs':'Opcional (+5€ per persona)' ?></div>
                    </li>
<!--                    <li class="col-md-6">-->
<!--                        <div class="title">Tamany:</div>-->
<!--                        <div class="value">--><?//= $detail->tamano ?><!--</div>-->
<!--                    </li>-->
                    <li class="col-xs-12 col-md-6">
                        <div class="title">Adults:</div>
                        <div class="value"><?= $detail->max_personas ?></div>
                    </li>
                    <li class="col-xs-12 col-md-6">
                        <div class="title">Nen +30€:</div>
                        <div class="value"><?= $detail->max_infantes ?></div>
                    </li>
                    <li class="col-xs-12 col-md-6">
                        <div class="title">Nadó +10€:</div>
                        <div class="value"><?= $detail->max_bebes ?></div>
                    </li>
                    <li class="col-xs-12 col-md-6">
                        <div class="title">Ambientació:</div>
                        <div class="value"><?= $detail->vistas ?></div>
                    </li>
                    <li class="col-xs-12 col-md-12">
                        <div class="title">Serveis:</div>
                        <div class="value"><?= $detail->servicios ?></div>
                    </li>
                </ul>
            </div>
            <div class="icons-container">
                <ul class="list-inline">
                    <li><i class="ravis-icon-computer-with-wifi-signal"></i></li>
                    <li><i class="ravis-icon-towel-on-hanger"></i></li>
                    <li><i class="ravis-icon-swimming-pool-sign"></i></li>
                    <li><i class="ravis-icon-bathtub-with-water-dropping"></i></li>
                    <li><i class="ravis-icon-sweeping-broom"></i></li>
                    <li><i class="ravis-icon-thermometer-high-temperature"></i></li>
                    <li><i class="ravis-icon-no-smoking-sign"></i></li>
                </ul>
            </div>
            <div class="description">
                <?= $detail->descripcion ?>
                <?= $detail->historia ?>
            </div>
        </div>
        <div class="r-sec col-md-4">

            <form id="room-booking-form" method="get" action="<?= base_url('reservar/confirmar') ?>"><!-- Do Not remove the classes -->
                <div class="input-daterange">
                    <div class="field-row">
                        <input placeholder="Entrada" class="datepicker-fields check-in" type="text"
                               name="start"/>
                        <!-- Date Picker field ( Do Not remove the "datepicker-fields" class ) -->
                        <i class="fa fa-calendar"></i><!-- Date Picker Icon -->
                    </div>
                    <div class="field-row">
                        <input placeholder="Sortida" class="datepicker-fields check-out" type="text"
                               name="end"/>
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
                <div class="field-row">
                    <!-- Select boxes ( you can change the items and its value based on your project's needs ) -->
                    <select name="room-type">
                        <option value="">Nº Adults</option>
                        <!-- Select box items ( you can change the items and its value based on your project's needs ) -->
                        <?php for($i=1;$i<=$detail->max_personas;$i++): ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor ?>
                    </select>
                    <!-- End of Select boxes -->
                    <i class="fa fa-caret-down"></i>
                </div>
                <div class="field-row">
                    <select name="guest">
                        <option value="0">Nº Nens</option>
                        <?php for($i=1;$i<=$detail->max_infantes;$i++): ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor ?>
                    </select>
                    <i class="fa fa-caret-down"></i>
                </div>
                <div class="field-row">
                    <select name="bebes">
                        <option value="0">Nº Nadons</option>
                        <?php for($i=1;$i<=$detail->max_bebes;$i++): ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor ?>
                    </select>
                    <i class="fa fa-caret-down"></i>
                </div>
                <div class="field-row">
                    <input type="hidden" name="habitacion" value="<?= $detail->id ?>">
                    <input type="submit" value="Reserva Ara">
                </div>
            </form>

            <div class="room-rating">
                <div class="ravis-title-t-2">
                    <div class="title"><span>L'habitació</span></div>
                    <div class="sub-title">Els nostres clients la valoren</div>
                </div>

                <div class="rate-box-container">
                    <div class="rate-box">
                        <div class="title">En general</div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="88" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 88%"><span>88%</span></div>
                        </div>
                    </div>
                    <div class="rate-box">
                        <div class="title">Neteja</div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 95%"><span>95%</span></div>
                        </div>
                    </div>
                    <div class="rate-box">
                        <div class="title">Lluminositat</div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="78" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 78%"><span>78%</span></div>
                        </div>
                    </div>
                    <div class="rate-box">
                        <div class="title">Vistes</div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 60%"><span>60%</span></div>
                        </div>
                    </div>
                    <div class="rate-box">
                        <div class="title">Serveis</div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="97" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 97%"><span>97%</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<?php if($relacionados->num_rows()>0): ?>
<!--Other Rooms Section-->
<section id="other-rooms">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Altres habitacions</div>
                <div class="sub-title">Llista d'altres habitacions que us poden interessar</div>
            </div>
        </div>
        <div class="room-container clearfix">
            <?php foreach($relacionados->result() as $r): ?>
                <div class="room-box col-xs-6 col-md-4 animated-box" data-animation="fadeInUp">
                    <div class="inner-box" data-bg-img="<?= $r->foto ?>">
                        <a href="<?= $r->link ?>" class="more-info"></a>
                        <div class="caption">
                            <div class="title"><?= $r->habitacion_nombre ?></div>
                            <div class="price">
                                <div class="title">Des de:</div>
                                <div class="value"><?= str_replace('.00','',$r->precio_desde) ?>€</div>
                            </div>
                            <div class="desc">
                                <div class="inner-box">
                                    <?= cortar_palabras(strip_tags($r->descripcion),18) ?>...
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endforeach ?>            
        </div>
    </div>
</section>
<?php endif ?>
<?php $this->load->view('includes/template/footer'); ?>


<!--End of Room Section-->
<script>
    var reservas = <?php 
        $re = array();
        foreach($reservas->result() as $r){
            $datetime1 = new DateTime($r->desde);
            $datetime2 = new DateTime($r->hasta);
            $interval = $datetime1->diff($datetime2);
            $dias = $interval->format('%a días');
            for($i=0;$i<$dias;$i++){
                $fecha = date("Y-m-d",strtotime("+$i days ".$r->desde));
                if(!in_array($fecha, $re)){
                    $re[] = $fecha;
                }
            }
        }
        
        //Concatenamos las reservas locales
        if(!empty($_SESSION['reserva'])){
            foreach($_SESSION['reserva'] as $r){
                $datetime1 = new DateTime($r['desde']);
                $datetime2 = new DateTime($r['hasta']);
                $interval = $datetime1->diff($datetime2);
                $dias = $interval->format('%a días');
                for($i=0;$i<$dias;$i++){
                    $fecha = date("Y-m-d",strtotime("+$i days ".$r['desde']));
                    if(!in_array($fecha, $re)){
                        $re[] = $fecha;
                    }
                }
            }
        }
        echo json_encode($re);
    ?>;
</script>
