<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb3.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Habitacions</div>
                <div class="sub-title">Descripció de les nostres estances</div>
            </div>
        </div>
        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Home</a></li>
                <li class="current"><a href="#">Habitacions</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Room Section-->
<section id="rooms-section" class="row-view">
    <div class="inner-container container">
        <div class="ravis-title-t-2">
            <div class="title"><span>Les nostres habitacions</span></div>
        </div>
        <div class="desc">
            La casa consta de 5 habitacions. Cada dormitori és diferent, té una història i unes particularitats.<br> Troba aquella que s'adequa més al que tu busques i gaudeix la teva estança.
        </div>

        <?= $output ?>
        <!-- Pagination -->
   <!--     <div class="pagination-box">
            <ul class="list-inline">
                <li class="active"><a href="#"><span>1</span></a></li>
                <li><a href="#"><span>2</span></a></li>
                <li><a href="#"><span>3</span></a></li>
            </ul>
        </div>-->
        <!-- End of Pagination -->

    </div>
</section>
<!--Footer Section-->
<?php $this->load->view('includes/template/footer'); ?>
<!--End of Footer Section-->

