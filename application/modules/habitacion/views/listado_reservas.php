<script src="<?= base_url() ?>js/daypilot-all.min.js?v=3091" type="text/javascript"></script>  
<style type="text/css">
    .scheduler_default_corner > div:last-child{
        display:none !important;
    }
</style>


<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class=""><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-list"></i> Listado</a></li>
    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-calendar"></i> Calendario</a></li>    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane " id="home">
    	<?= $output ?>
    </div>
    <div role="tabpanel" class="tab-pane active" id="profile">
    	<div id="dp"></div>
    </div>    
  </div>

</div>

<script type="text/javascript">
    var dp = new DayPilot.Scheduler("dp");

    // view
    dp.startDate = new DayPilot.Date("<?= date("Y-m-d") ?>");  // or just dp.startDate = "2013-03-25";
    dp.days = 365;
    dp.scale = "Day"; // one day
    dp.timeHeaders = [
        { groupBy: "Month"},
        { groupBy: "Day", format: "d"}
    ]

    // bubble, sync loading
    // see also DayPilot.Event.data.staticBubbleHTML property
    dp.bubble = new DayPilot.Bubble({
        onLoad: function(args) {
            var ev = args.source;
            args.html = "Reserva " + ev.text();
        }
    });

    dp.treeEnabled = true;
    dp.rowHeaderWidth = 150;
    <?php
    	$hab = array();
    	foreach($this->db->get_where('habitaciones')->result() as $h){
    		$hab[] = array('name'=>$h->habitacion_nombre,'id'=>$h->id);
    	}   
    ?>
    dp.resources = <?= json_encode($hab) ?>;

    var reservas = <?php 
    	$reservas = array();
    	$this->db->select('reservas_habitaciones.*,clientes.nombres, clientes.apellidos');
    	$this->db->join('reservas','reservas.id = reservas_habitaciones.reservas_id');
    	$this->db->join('clientes','clientes.id = reservas.clientes_id'); 
    	foreach($this->db->get_where('reservas_habitaciones')->result() as $r){
    		$reservas[] = array(
    			'adultos'=>$r->adultos,
    			'precio'=>$r->costo_habitacion,
    			'primary'=>$r->id,
    			'text'=>'#'.$r->reservas_id.'-'.$r->nombres.' '.$r->apellidos,
                'reserva'=>$r->reservas_id,
    			'id'=>$r->reservas_id,
    			'relation'=>$r->habitaciones_id,
    			'desde'=>$r->desde.'T12:00:00',
    			'hasta'=>$r->hasta.'T00:00:00'
    		);
    	}
    	echo json_encode($reservas);
    ?>;
    for(var i in reservas){
    	var e = new DayPilot.Event({
    		start:new DayPilot.Date(reservas[i].desde),
    		end:new DayPilot.Date(reservas[i].hasta),
    		id:reservas[i].id,
    		resource:reservas[i].relation,
    		text:reservas[i].text,
    		primary:reservas[i].primary,
    		precio:reservas[i].precio,
    		adultos:reservas[i].adultos,
            reserva:reservas[i].reserva
    	});
    	dp.events.add(e);
    }

    dp.eventHoverHandling = "Bubble";
        
    // event moving
    
    // event resizing

    function getDiffDays(desde,hasta){
    	var oneDay     = 24 * 60 * 60 * 1000;
    	var newDate    = new Date(desde),
            newDateStr = ("0" + newDate.getDate()).slice(-2) + '-' + ("0" + (newDate.getMonth() + 1)).slice(-2) + '-' + newDate.getFullYear();
            checkInDate = newDate.getTime();

            newDate    = new Date(hasta),
            newDateStr = ("0" + newDate.getDate()).slice(-2) + '-' + ("0" + (newDate.getMonth() + 1)).slice(-2) + '-' + newDate.getFullYear();            
            checkOutDate = newDate.getTime();
            var diffDays = Math.round(Math.abs((checkInDate - checkOutDate) / (oneDay)));
            return diffDays;
    }

    function send(args){        
		var desde = args.e.data.start.value;
    	var hasta = args.e.data.end.value;
    	var dias = getDiffDays(desde,hasta);
    	var adultos = args.e.data.adultos;
    	var precio = (parseFloat(args.e.data.precio)*adultos)*dias;
        var data = {
            reservas_id:args.e.data.reserva,
            desde:desde,
            hasta:hasta,
            precio:precio,
            dias:dias,
            habitaciones_id:args.e.data.resource
        };        
    	$.post('<?= base_url('habitacion/reserva/modReservas/update/') ?>/'+args.e.data.primary,data,function(data){});
    }

    dp.onEventResized = function (args) {
    	console.log(args.e);
    	send(args);
        dp.message("Los dias de la reserva se han modificado");
    };

    // event creating
    /*dp.onTimeRangeSelected = function (args) {
        var name = prompt("New event name:", "Event");
        dp.clearSelection();
        if (!name) return;
        var e = new DayPilot.Event({
            start: args.start,
            end: args.end,
            id: DayPilot.guid(),
            resource: args.resource,
            text: name
        });
        dp.events.add(e);
        dp.message("Created");
    };*/
    
    dp.onEventClicked = function(args) {
    	if(confirm("¿Desea editar esta reserva?")){
    		document.location.href="<?= base_url('habitacion/reserva/reservas/edit/') ?>/"+args.e.id();
    	}        
    };

    dp.onEventMoved = function (args) {
        send(args);
        dp.message("La reserva se ha movido");
    };
    /*dp.onTimeHeaderClick = function(args) {
        alert("clicked: " + args.header.start);
    };*/
    
    dp.init();

</script>