<?php $ajustes = $this->db->get('ajustes')->row(); ?>
<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/maletas.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Iniciar la Reserva</div>
                <div class="sub-title">Comproba la disponibilitat de les habitacions</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="current"><a href="">Confirmar Reserva</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<section id="booking-section" class="step-3">
    <div class="inner-container container">
        <div class="col-md-4 l-sec">
            <div class="ravis-title-t-2">
                <div class="title"><span>Informació de la Reserva</span></div>
            </div>
            <div class="check-in-out-container">
                <div class="check-in-out-box">
                    <div class="title">Entrada :</div>
                    <div class="value"><?= $desde ?></div>
                </div>
                <div class="check-in-out-box">
                    <div class="title">Sortida :</div>
                    <div class="value"><?= $hasta ?></div>
                </div>
            </div>

            <div class="selected-room-container">
                <?php $total = 0; ?>
                <?php $bebes = 0; $infantes = 0; $desayuno = 0; ?>
                <?php foreach($reserva as $n=>$r): ?> 
                    <?php 
                        $habitacion = $r['habitacion']; 
                        /*if(empty($r['oferta'])){*/
                            $dias = (int)$r['dias']; 
                            $total+= $dias*($r['adultos']*$habitacion->precio_desde);
                        /*}else{
                            $total+= $r['oferta']->precio;
                            $habitacion->precio_desde = $r['oferta']->precio;
                            $dias = $r['oferta']->cantidad_noches;
                        }
                        //Sumar infantes y bebes
                        if(empty($r['oferta']) && $r['bebes']>0){
                            $bebes+= $r['bebes'];
                        }
                        if(empty($r['oferta']) && $r['infantes']>0){
                            $infantes+= $r['infantes'];
                        }
                        if(!empty($r['desayuno'])){
                            $desayuno+= $r['adultos']*5;
                            $desayuno+= $r['infantes']*5;
                        }*/
                        //echo $r['desde'].' '.$r['hasta'];
                    ?>
                    <div class="selected-room-box">
                        <div class="room-title">
                            <div class="title"><a href="<?= base_url('habitacion/reservaFrontend/remove/'.$n) ?>"><i class="fa fa-remove"></i><br></a>   Habitació:</div>
                            <div class="value"><?= $habitacion->habitacion_nombre ?></div>
                        </div>
                        <div class="room-title">
                            <div class="title">Duració :</div>
                            <div class="value"><?= $dias+1 ?> Dies y <?= $dias ?> Nits</div>
                        </div>
                        <div class="adult-count">
                            <div class="title">Adults :</div>
                            <div class="value"><?= $r['adultos'] ?></div>
                        </div>
                        <div class="child-count">
                            <div class="title">Nens :</div>
                            <div class="value"><?= $r['infantes'] ?></div>
                        </div>
                        <div class="child-count">
                            <div class="title">Nadons :</div>
                            <div class="value"><?= $r['bebes'] ?></div>
                        </div>
                        <div class="child-count">
                            <div class="title">Esmorzar :</div>
                            <div class="value"><?= !empty($r['desayuno'])?'SI':'NO' ?></div>
                        </div>
                        <div class="price">
                            <?php
                                /*if(empty($r['oferta'])){*/
                                    //echo ($dias*$habitacion->precio_desde)+($r['bebes']*$ajustes->precio_bebe)+($r['infantes']*$ajustes->precio_infante);
                            echo ($habitacion->precio_desde*$r['adultos'])*$dias;
                        /*        }else{
                                    echo $habitacion->precio_desde;
                                }*/
                            ?>$
                        </div>
                    </div>
                <?php endforeach ?>
                <div class="selected-room-box">
                    <a href="<?= base_url('iniciar-reserva') ?>" class="price" style="position:static">Afegir habitacions</a>
                </div>
                
            </div>

            <div class="price-details-container">
                
                <div class="price-detail-box">
                    <div class="title">Habitacions i Serveis:</div>
                    <div class="value"><?= $total ?>€</div>
                </div>
                <div class="price-detail-box">
                    <div class="title">Preu suplement nen:</div>
                    <div class="value"><?= $infantes*$ajustes->precio_infante ?>€</div>
                </div>
                <div class="price-detail-box">
                    <div class="title">Preu suplement nadó:</div>
                    <div class="value"><?= $bebes*$ajustes->precio_bebe ?>€</div>
                </div>
                <div class="price-detail-box">
                    <div class="title">Preu per esmorzars:</div>
                    <div class="value"><?= $desayuno ?>€</div>
                </div>
                <?php $total+= ($infantes*$ajustes->precio_infante)+($bebes*$ajustes->precio_bebe)+$desayuno ?>
                <!--<div class="price-detail-box">
                    <div class="title">Vat 8% :</div>
                    <div class="value">$117.00</div>
                </div>-->
                <div class="price-detail-box total">
                    <div class="title">Preu total :</div>
                    <div class="value"><?= $total ?>€</div>
                </div>
                <div class="payment-method">
                    <div class="ravis-radio">
                        <label for="full-payment">
                            <input type="radio" name="payment-method" value="<?= $total ?>" id="full-payment">
                            <span></span>
                            Total Import
                        </label>
                    </div>
                    <div class="ravis-radio">
                        <label for="deposit">
                            <input type="radio" name="payment-method" value="<?= $total/2 ?>" id="deposit" checked>
                            <span></span>
                            50% Import
                        </label>
                    </div>
                </div>
                <div class="deposit-price">
                    <div class="title-box">
                        <div class="title">50% de l'import</div>
                        <div class="sub-title">A pagar al fer la reserva</div>
                    </div>
                    <div class="value"><?= $total/2 ?>€</div>
                </div>
            </div>
        </div>
        <div class="col-md-8 r-sec">
            <div class="inner-box">
                <div class="steps">
                    <ul class="list-inline">                        
                        <li>Tria data</li>
                        <li>Tria habitació</li>
                        <li class="active">Fer la reserva</li>
                        <li>Confirmació</li>
                    </ul>
                </div>

                <div id="booking-guest-info-form">
                    <form action="<?= base_url('reservar/checkout') ?>" method="post">
                        <div class="field-row clearfix">
                            <div class="col-md-6">
                                <input type="text" name="nombre" placeholder="Nom:*" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="apellido" placeholder="Cognoms:*" required>
                            </div>
                        </div>
                        <div class="field-row clearfix">
                            <div class="col-md-6">
                                <input type="text" name="telefono" placeholder="Telèfon:*" required>
                            </div>
                            <div class="col-md-6">
                                <input type="email" name="email" placeholder="Email:*" required>
                            </div>
                        </div>
                        <div class="field-row clearfix">
                            <div class="col-md-6">
                                <?= form_dropdown_from_query('tipo_doc_id', 'tipodocumento', 'id', 'denominacion') ?>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="nro_documento" placeholder="#Documento" required>
                            </div>
                        </div>
                        <div class="field-row clearfix">
                            <input type="text" name="direccion" placeholder="Adreça:*">
                        </div>
                        <div class="field-row clearfix">
                            <textarea name="requerimiento" placeholder="Requeriments especials:"></textarea>
                            <div style=" margin-left: 488px">*Obligatori</div>
                        </div>
                        <div class="field-row clearfix" style="margin-bottom:0px">
                            <div class="ravis-checkbox">
                                <label for="terms-condition">
                                    <input type="checkbox" name="terms" value="1" id="terms-condition" required>
                                    <span></span>
                                    He lleguit i acepto els <a href="<?= base_url() ?>p/condicions.html" target="_new">Termins i condicions.* </a>
                                </label>
                            </div>
                        </div>
                        <div class="field-row clearfix">
                            <div class="ravis-checkbox">
                                <label for="cond">
                                    <input type="checkbox" name="cond" value="1" id="cond">
                                    <span></span>
                                    No vul rebre informació de Cal Fuster de la Plaça.
                                </label>
                            </div>
                        </div>
                        <div class="g-recaptcha" data-sitekey="6LcO_jwUAAAAAPCXx3Q5ngnMxgRTF91-l0rG1LV_"></div>
                        <div class="field-row btn-container clearfix">
                            <input type="hidden" name="desde" value="<?= date("Y-m-d",strtotime($desde)) ?>">
                            <input type="hidden" name="hasta" value="<?= date("Y-m-d",strtotime($hasta)) ?>">
                            <input type="hidden" name="total" value="<?= $total ?>">
                            <input type="hidden" id="abono" name="abono" value="<?= $total/2 ?>">
                            <button class="by-email" type="submit">Fer Pagament</button>
                        </div>
                        <?php if(!empty($_SESSION['msj'])): ?>
                        <div class="alert alert-danger"><?php echo $_SESSION['msj']; unset($_SESSION['msj']); ?></div>
                        <?php endif ?>
                    </form>
                </div>


            </div>
        </div>
    </div>

</section>
