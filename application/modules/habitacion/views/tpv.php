<?php 
    // Se crea Objeto
    $this->load->library('redsysAPI');
    $miObj = new RedsysAPI;    
    $merchantCode 	="344511985";
    $key = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';
    $terminal 		="001";
    $amount 		=$reserva->abonado*100;
    $currency 		="978";
    $transactionType    ="0";
    $merchantURL 	=base_url('reservar/procesarPago/'.$reserva->id);
    $urlOK 		=base_url('reserva_realizada/'.$reserva->id);
    $urlKO 		=base_url('reservar/pagoKo');
    $order 		=date("dmhis").$reserva->id;    
    $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
    $miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
    $miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
    $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
    $miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);		
    $miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
    $version="HMAC_SHA256_V1";   
    $request = "";
    $params = $miObj->createMerchantParameters();
    $signature = $miObj->createMerchantSignature($key);
    //$url = "https://sis.redsys.es/sis/realizarPago";
    $url = "https://sis-t.redsys.es:25443/sis/realizarPago";
    /*
        Tarjeta de pruebas: 4548 8120 4940 0004
     *  Venc: 12/20
     *  CVV2: 285
     *  Auth Bancaria: 123456
    */
?>
<form id="myForm" action="<?= $url ?>" method="post">
                                           
    <input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/>
    <input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/>
    <input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>" />
    <div class="form-row place-order" align="center">
        <?= img('img/pay.jpg') ?><br/>
        Redireccionando a el banco por favor espere...        
    </div>
</form>
<script>
    document.getElementById('myForm').submit();
</script>
