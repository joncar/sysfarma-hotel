<?php

require_once APPPATH.'/controllers/Panel.php';    

class Compras extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
        }                
    }
    
    public function compras($x = '',$y = '',$z = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->field_type('transaccion', 'dropdown', array('0' => 'ASD'));
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '1':return 'Pagada <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '0':return 'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add')
            $output->output = $this->load->view('compras', null, TRUE);
        if ($x == 'edit') {
            $compra = $this->db->get_where('compras', array('id' => $y));
            if ($compra->num_rows() > 0) {
                $compra = $compra->row();
                $output->output = $this->load->view('compras', array('compra' => $compra, 'detalles' => $this->db->get_where('compradetalles', array('compra' => $y))), TRUE);
            }
        }

        $this->loadView($output);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
