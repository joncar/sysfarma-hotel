<?php

require_once APPPATH.'/controllers/Panel.php';    

class Entradas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
        }                
    }
    
    public function entrada_productos($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/entrada_productos'));
        else {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('motivo', 'motivo_entrada', 'denominacion')
                ->set_relation('proveedor', 'proveedores', 'denominacion')
                ->set_relation('usuario', 'user', '{nombre} {apellido}');
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add')
            $output->output = $this->load->view('entradas', null, TRUE);
        if ($x == 'edit') {
            $compra = $this->db->get_where('entrada_productos', array('id' => $y));
            if ($compra->num_rows() > 0) {
                $compra = $compra->row();
                $output->output = $this->load->view('entradas', array('entrada' => $compra, 'detalles' => $this->db->get_where('entrada_productos_detalles', array('entrada_producto' => $y))), TRUE);
            }
        }

        $this->loadView($output);
        }
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
