<?php

require_once APPPATH.'/controllers/Panel.php';    

class Salidas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
        }                
    }
    
    public function salidas($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/salidas'));
        else {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        //$crud->columns('transaccion','cliente','fecha','caja','nro_factura','sucursal','total_venta'); 
        $crud->set_relation('proveedor', 'proveedores', 'denominacion')
                ->set_relation('motivo', 'motivo_salida', 'denominacion')
                ->set_relation('usuario', 'user', '{nombre} {apellido}');
        $crud->unset_read()
                ->unset_delete();
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add'){
            $output->output = $this->load->view('salidas', array(), TRUE);
        }
        if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $salida = $this->db->get_where('salidas', array('id' => $y));
            $detalles = $this->db->get_where('salida_detalle', array('salida' => $salida->row()->id));
            $output->output = $this->load->view('salidas', array('salida' => $salida->row(), 'detalles' => $detalles), TRUE);
        }
        if ($x == 'imprimir') {
                if (!empty($y) && is_numeric($y)) {
                    $this->db->select('salidas.*, proveedores.denominacion as proveedorname, motivo_salida.denominacion as motivoname, CONCAT(user.nombre," ",user.apellido) as usuario',FALSE);
                    $this->db->join('motivo_salida','motivo_salida.id = salidas.motivo');
                    $this->db->join('proveedores','proveedores.id = salidas.proveedor');
                    $this->db->join('user','user.id = salidas.usuario');
                    $venta = $this->db->get_where('salidas', array('salidas.id' => $y));
                    if ($venta->num_rows() > 0) {
                        $this->db->select('salida_detalle.*, productos.nombre_comercial');
                        $this->db->join('productos', 'salida_detalle.producto = productos.codigo');
                        $detalles = $this->db->get_where('salida_detalle', array('salida' => $venta->row()->id));
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', array(5, 5, 5, 8));
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/imprimir_salida', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                        $html2pdf->Output('Reporte Legal.pdf', 'D');
                        //echo $this->load->view('reportes/imprimir_salida',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                    
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            } else {
                $this->loadView($output);
            }
        }
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
