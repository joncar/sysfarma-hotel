<?php              
    switch($pedidos->tipo_pedidos_id){
         case 1://Mesa
             $label =  '<i class="fa fa-cutlery"></i> '.$this->db->get_where('mesas',array('id'=>$pedidos->mesas_id))->row()->mesa_nombre.'';
         break;
         case 2://deliverys
            $label = '<i class="fa fa-motorcycle"></i> '.$this->db->get_where('deliverys',array('id'=>$pedidos->deliverys_id))->row()->nombre_delivery.'';
         break;
         case 3://Barra
            $label = '<i class="fa fa-beer"></i> '.$this->db->get_where('barras',array('id'=>$pedidos->barras_id))->row()->nombre_barra.'';
         break;
         case 4://Habitación
            $label = '<i class="fa fa-bed"></i> '.$this->db->get_where('habitaciones',array('id'=>$pedidos->habitaciones_id))->row()->habitacion_nombre.'';
         break;
    }
?>
<!--<div class="alert alert-info">
    <i class="fa fa-question-circle"></i> Marca los productos de la pestaña <b>Pedido</b> antes de facturarlos en la pestaña de <b>Facturar</b>
</div>-->
<?php if($pedidos->facturado==1): ?>
    <div class="alert alert-warning">
        <i class="fa fa-exclamation-triangle"></i> Pedido ya facturado por lo que no se pueden realizar cambios en el mismo.
    </div>
<?php endif ?>
<div>  
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?= $label ?></a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Facturar</a></li>
    <li role="presentation"><a href="#profile2" aria-controls="profile" role="tab" data-toggle="tab">Facturas</a></li>
    <li role="presentation"><a href="#profile3" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-question-circle"></i></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <?php if($pedidos->tipo_pedidos_id==2){$this->load->view('delivery_datos_primarios',array('pedidos'=>$pedidos));} ?>
        <p>
            <input type="checkbox" id="todos"> <label for="todos">Marcar todos los productos</label>            
        </p>
        <?php if($pedidos->tipo_pedidos_id!=2): ?>
            <div style="position: absolute;right: 12px;top: 13px; text-align: right;">Total Pedido<input type="text" id="totaldelivery" value="0" readonly=""></div>
        <?php endif ?>
        <?= $output ?>
        <p align="right">
            <a href="javascript:imprimirCuenta()" class="btn btn-primary"><i class="fa fa-print"></i> Imprimir Cuenta</a>
        </p>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
        <?php if($pedidos->facturado==1): ?>
            <div class="alert alert-warning">
                <i class="fa fa-exclamation-triangle"></i> Pedido ya facturado por lo que no se pueden realizar cambios en el mismo.
            </div>
        <?php else: ?>        
            <form action="" onsubmit="return send(this)">
                <div>
                    <div class="row" style="margin-bottom:30px;">
                        <div class="col-xs-12 col-sm-4">
                            <?php $this->db->order_by("nombres"); ?>
                            <b>Cliente: </b> <a href="#" title="Añadir cliente" data-toggle="modal" data-target="#frameModal"><i class="fa fa-plus-circle"></i></a>
                            <?php $cliente = empty($venta->clientes_id)?1:$venta->clientes_id; ?>
                            <?= form_dropdown_from_query('cliente','clientes','id','nro_documento nombres apellidos',$cliente,'id="cliente"',FALSE) ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <b>Transacción: </b><?= form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',1,"",FALSE); ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <?php $factura = $this->querys->get_nro_factura(); ?>
                            <input type="hidden" name="nro_factura" value="">
                            <p align="right"><b>Factura #</b><span id="nrofactura"></span></p>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr><th>Producto</th><th>Cantidad</th><th>Importe</th></tr>
                        </thead>
                        <tbody id="listado">
                            <tr id="emptylist" class=""><td colspan="3">Seleccione los productos en la pestaña <b>Pedido</b></td></tr>
                            <tr id="productlist" class="hide">
                                <td>{nombre}</td>
                                <td>                            
                                    <input type="text" id="cantidadInput" class="form-control retrato" value="0">
                                </td>
                                <td>
                                    <input type="text" id="totalInput" class="form-control retrato" value="0" readonly=""> 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                            <p><b>Detalle del pedido</b></p>
                            <table class="table table-bordered">                
                                <tr><th>Productos</th><td id="productos">0</td></tr>
                                <tr><th>Productos elegidos</th><td id="productosElegidos">0</td></tr>
                                <tr><th>Importe total</th><td><span id="importeTotal"></span> Gs</td></tr>
                                <tr><th>Importe a facturar</th><td><b><span id="importeAFacturar">0</span> Gs</b></td></tr>
                                <tr><th>Importe pendiente</th><td><span id="importePendiente">0</span> Gs</td></tr>
                                <tr><th>Pendiente por cancelar</th><td><span id="PendientePorCancelar">0</span> Gs</td></tr>
                            </table>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                            <p><b>Detalle del pago</b></p>
                            <table class="table table-bordered">                
                                <tr><th>Efectivo</th><td><input type="text" name="total_efectivo" class="form-control pago" value="0"></td></tr>
                                <tr><th>Vuelto</th><td><input type="text" name="vuelto" class="form-control vuelto" value="0" readonly=""></td></tr>
                                <tr><th>Débito</th><td><input type="text" name="total_debito" class="form-control pago" value="0"></td></tr>
                                <tr><th>Crédito</th><td><input type="text" name="total_credito" class="form-control pago" value="0"></td></tr>
                                <tr><th>Cheque</th><td><input type="text" name="total_cheque" class="form-control pago" value="0"></td></tr>
                            </table>
                    </div>
                </div>
                <div style="text-align: right">
                    <input type="hidden" name="total_venta" value="0">
                    <input type="hidden" name="total_iva" value="0">
                    <input type="hidden" name="iva" value="0">
                    <input type="hidden" name="iva2" value="0">
                    <input type="hidden" name="exenta" value="0">
                    <input type="hidden" name="total_dolares" value="0">
                    <input type="hidden" name="total_reales" value="0">
                    <input type="hidden" name="sucursal" value="<?= $this->user->sucursal ?>">
                    <input type="hidden" name="caja" value="<?= $this->user->caja ?>">
                    <input type="hidden" name="cajadiaria" value="<?= $this->user->cajadiaria ?>">
                    <input type="hidden" name="usuario" value="<?= $this->user->id ?>">
                    <input type="hidden" name="status" value="0">
                    <input type="hidden" name="pedidos_id" value="<?= $pedido ?>">
                    <input type="hidden" name="fecha" value="<?= date("Y-m-d H:i:s") ?>">
                    <div id="messagebox" style="text-align:left;"></div>
                    <button class="btn btn-success" type="submit" id='cargarFactura'>Cargar Factura</button>
                </div>
            </form>
        <?php endif ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile2">        
        <?= $facturas->output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile3">        
        <ul class="list-group">
            <li class="list-group-item active">Facturación</li>
            <li class="list-group-item">Para poder facturar debes ir a la pestaña 1 (Mesa, delivery, barra) y seleccionar los productos que deseas facturar</li>
            <li class="list-group-item">Ir a la pestaña facturación, cargar los pagos y procesar el mismo</li>
            <li class="list-group-item active">Teclas rapidas</li>
            <li class="list-group-item">F3 - Para realizar busqueda avanzada de productos</li>
            <li class="list-group-item">F4 - Para añadir producto al pedido</li>
            <li class="list-group-item">F9 - Imprimir cuenta</li>
        </ul>
    </div>
     <?= $this->load->view('includes/modals/frame') ?>
     <?= $this->load->view('includes/modals/productosframe') ?>
  </div>

</div>
<?php $this->load->view('predesign/chosen'); ?>
<script src="<?= base_url('assets/grocery_crud/js/jquery_plugins/jquery.mask.js') ?>"></script>
<script>


    $(document).on('ready',function(){        
        $(".pago, input[name='total[]'], #totaldelivery").mask("000.000.000", {reverse: true});
    });

    var total = 0;
    var productos = 0;
    var totalProductos = 0;
    var totalizado = 0;
    var itenes = [];
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {        
        $("#profile select").chosen();
        fillNroFactura();
    }); 
    
    $("#productosModal").on("shown.bs.modal",function(){
        $("#ProductosFrame").attr("src","<?= base_url('movimientos/productos/buscador_productos') ?>");
    });
    $("#productosModal").on("hide.bs.modal",function(){
        $("#ProductosFrame").attr("src","");
    });
    $("#frameModal").on("shown.bs.modal",function(){
        $("#Frame").attr("src","<?= base_url('maestras/clientes/add/json') ?>");
    });
    $("#frameModal").on("hide.bs.modal",function(){
        $.get("<?= base_url("maestras/clientes/json_list") ?>",{},function(data){
                data = JSON.parse(data);
                clientes = $("#cliente");
                clientes.html('');
                var selected = 0;
                var selectedName = "Seleccione una opcion";
                for(var i in data){                    
                    clientes.append('<option value="'+data[i].id+'">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>');
                    if(parseInt(data[i].id)>selected){
                        selected = data[i].id;      
                        selectedName = data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos;
                    }
                }
                clientes.find('option[value="'+selected+'"]').attr('selected',true);
                clientes.chosen().trigger('liszt:updated');
                clientes.parent().find('.chosen-single span').html(selectedName);
        });
    });
    
    $(document).on("completeRefreshList",function(){
        fillProducts();
    });
    
    $(document).on('click',"tr input, tr select, tr textarea",function(e){       
        e.stopPropagation();
    });
    
    $(document).on("change",".ajax_list input",function(){        
        if($(this).attr('type')!=='checkbox' && $(this).attr('type')!=='hidden' && !$(this).parent().hasClass('chosen-search') && !$(this).hasClass('pro')){
            $(this).parents('table').find('input').css('background','gray');
            $(this).parents('table').find('input').readonly;
            var form = $(this).parents('tr');
            var id = $(this).data('rowid');
            var data = {};
            data['detalle'] = form.find('.detalle').val();
            data['cantidad'] = form.find('.cantidad').val();
            data['pedidos_id'] = <?= $pedido ?>;
            data['productos_id'] = form.find('.producto').val();
            data['precio_venta'] = form.find('.precio_venta').val();
            data['precio_costo'] = form.find('.precio_costo').val();
            data['total'] = parseFloat(data.cantidad)*parseFloat(data.precio_venta);
            $.post('<?= base_url('pedidos/admin/pedidos_detalles/'.$pedido.'/update/') ?>/'+id,data,function(data){
                $('.flexigrid').on('success',function(){
                    setTimeout(function(){                        
                        sumartodoPedido();
                    },1000);                    
                    $('.flexigrid').unbind('success');
                });
                $(".ajax_refresh_and_loading").trigger('click');
            });
        }
    });
    
    $(document).on("change",".producto",function(){        
        $(this).parents('table').find('input').css({'color':'white','background':'gray'});
            $(this).parents('table').find('input').readonly;
            var form = $(this).parents('tr');
            var data = {};
            var producto = $(this).val();
            data['detalle'] = "";
            data['cantidad'] = 1;
            data['pedidos_id'] = <?= $pedido ?>;
            data['productos_id'] = producto;
            data['precio_venta'] = form.find('.producto').data('precio_venta');
            data['precio_costo'] = form.find('.producto').data('precio_costo');
            data['total'] = form.find('.producto').data('precio_venta');
            $.post('<?= base_url('pedidos/admin/pedidos_detalles/'.$pedido.'/insert/') ?>',data,function(data){
                $('.flexigrid').on('success',function(){
                    setTimeout(function(){
                        $("#c"+producto).focus();              
                        sumartodoPedido();
                    },1000);                    
                    $('.flexigrid').unbind('success');
                });
                $(".ajax_refresh_and_loading").trigger('click');
                
            });
    });
    
    $(document).on('click',"tr",function(e){       
        var chk = $(this).find(".chk");
        if(typeof(chk)!=='undefined' && chk.length>0){
            var checked = chk.prop('checked');
            chk.prop('checked',checked?false:true);
            chk.trigger('clickchk');
        }
    });
    
    $(document).on("click clickchk",".chk",function(){
        if(!$(this).prop('checked')){
            $("#todos").prop('checked',false);
        }
        var cantidad = parseFloat($(this).data('cantidad'));
        if(cantidad>0 && parseFloat($(this).data('precio'))!==0){
            fillProducts();
            $(".pago,.vuelto").val(0);
        }else{
            alert("El producto elegido ya fue liquidado");
            $(this).prop('checked',false);
        }
    });
    
    $(document).on("click","#todos",function(){
        $(".chk").prop('checked',$(this).prop('checked'));
        fillProducts();
    });
    
    $(document).on("change","input[name='cantidad[]']",function(){
        var max = parseFloat($(this).data('cantidad'));
        var val = parseFloat($(this).val());
        if(max<val || val<=0){
            alert("El numéro máximo permitido para este campo es "+max);
            $(this).val(max);
        }else{
            totalizar();
        }
    });
    
    $(document).on("change",".pago",function(){
        var val = parseFloat($(this).val());
        if(isNaN(val)){
            $(this).val(0);
        }
        relacionDeudaPago();
    });    
    
    $(document).on("change","input[name='total_efectivo']",function(){        
        var val = parseFloat($(this).val().replace(/\./g,''));
        var vuelto = val-total;
        vuelto = vuelto<0?0:vuelto;
        $("input[name='vuelto']").val(vuelto.formatMoney(0,',','.'));
        relacionDeudaPago();
    });
    
    /*$(document).on("change","input[name='total[]']",function(){
        var max = parseFloat($(this).data('total'));
        var val = parseFloat($(this).val().replace(/\./g,''));
        if(max<val || val<=0){
            alert("El numéro máximo permitido para este campo es "+max);
            $(this).val(max);
        }
        var cantidad =  $(this).parents("tr").find('input[name="cantidad[]"]');
        var cantidad_max = parseFloat(cantidad.data('cantidad'));
        var cantidad_val = parseFloat(cantidad.val());
        if(val<max && cantidad_max === cantidad_val){
            alert("Por favor elija un monto proporcional a la cantidad elegida a cancelar");
            $(this).val(max);
        }
        var cantidades = $("input[name='total[]']");
        total = 0;
        if(cantidades.length===0){ 
            mostrarPrecios();
        }
        cantidades.each(function(){
            if(!$(this).hasClass("retrato")){                    
                total+= parseFloat($(this).val());
            }
            mostrarPrecios();
        });
        
    });*/

    Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
        c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "," : t, 
        s = n < 0 ? "-" : "", 
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
     };
    
    function relacionDeudaPago(){
        var deuda = total;
        $(".pago").each(function(){           
            var val = $(this).val();
            val = val.replace(/\./g,''); 
            val = parseFloat(val);            
            if($(this).attr('name')=='total_efectivo'){ 
                var vuelto = $("input[name='vuelto']").val();
                vuelto = vuelto.replace(/\./g,'');
                val-= parseFloat(vuelto);
            }
            deuda-=val;            
            $("#PendientePorCancelar").html(deuda.formatMoney(0,',','.'));
        });
    }
    
    function mostrarPrecios(){
        $("#productos").html(totalProductos);
        $("#productosElegidos").html(productos);
        $("#importeTotal").html(totalizado);
        $("#importeAFacturar").html(total.formatMoney(0,',','.'));
        $("input[name='total_venta']").val(total);
        $("#importePendiente").html(totalizado-total);
        relacionDeudaPago();
    }
    function totalizar(){
        productos = 0;
        total = 0;
        var cantidades = $("input[name='cantidad[]']");
        if(cantidades.length===0){
            mostrarPrecios();
        }
        cantidades.each(function(){
            if(!$(this).hasClass("retrato")){
                var o = $(this);            
                productos+= parseFloat(o.val());
                var importe = parseFloat(o.val())*parseFloat(o.data("precio"));            
                total+= importe;
                o.parents("tr").find("input[name='total[]']").val(importe);                
            }
            mostrarPrecios();
        });
    }
    
    function fillProducts(){
        $("#emptylist").show();
        $(".productlist").remove();
        var inputs = $(".chk").length;
        var carrete = 0;                
        totalProductos = 0;
        totalizado = 0;
        itenes = [];
        $(".chk").each(function(){
            carrete++;
            totalProductos+= parseFloat($(this).data("cantidad"));
            totalizado+=parseFloat($(this).data("total"));
            if($(this).prop('checked')){
                itenes.push($(this).val());
                $("#emptylist").hide();
                var fila = $("#productlist").clone();
                fila.removeAttr('id');
                fila.addClass("productlist");
                fila.removeClass("hide");
                var n = fila.html();
                n = n.replace("{nombre}",$(this).data("nombre"));                                
                fila.html(n);                
                var inputcantidad = fila.find("#cantidadInput");
                inputcantidad.removeAttr('id');
                inputcantidad.attr('name',"cantidad[]");
                inputcantidad.val($(this).data("cantidad"));
                inputcantidad.attr("data-precio",$(this).data("precio"));
                inputcantidad.attr("data-cantidad",$(this).data("cantidad"));
                var inputtotal = fila.find("#totalInput");
                inputtotal.removeAttr('id');
                inputtotal.attr('name','total[]');
                inputtotal.val($(this).data("total"));
                inputtotal.attr("data-total",$(this).data("total"));
                fila.find(".retrato").removeClass("retrato");
                $("#listado").append(fila);
            }
            if(carrete===parseFloat(inputs)){
                totalizar();
            }
        });
    }
    
    function fillNroFactura(){
        $.post("<?= base_url("movimientos/ventas/next_nro_factura") ?>",{},function(data){
            $("input[name='nro_factura']").val(data);
            $("#nrofactura").html(data);
        });
    }    
    function send(form){        
        $("#cargarFactura").attr('disabled',true);
        var f = new FormData(form);
        for(var i in itenes){
            f.append('codigo[]',itenes[i]); 
        }
        $.ajax({
            url:'<?= base_url("pedidos/admin/ventas/insert_validation") ?>',
            type:'POST',
            contentType:false,
            data:f,
            processData:false,
            cache:false,
            success:function(data){
                data = data.replace("<textarea>","",data);
                data = data.replace("</textarea>","",data);
                data = JSON.parse(data);
                if(data.success){
                    $.ajax({
                            url:'<?= base_url("pedidos/admin/ventas/insert") ?>',
                            type:'POST',
                            contentType:false,
                            data:f,
                            processData:false,
                            cache:false,
                            success:function(data){
                                data = data.replace("<textarea>","",data);
                                data = data.replace("</textarea>","",data);
                                data = JSON.parse(data);
                                if(data.success){
                                    $("#cargarFactura").attr('disabled',false);
                                    $("#messagebox").removeClass('alert').removeClass('alert-danger');
                                    $("#messagebox").addClass('alert').addClass('alert-success').html("Su factura ha sido creada con éxito <a href='javascript:imprimir("+data.insert_primary_key+")'>Imprimir Factura</a> | <a href='javascript:imprimirTicket("+data.insert_primary_key+")'>Imprimir Ticket</a>");
                                    fillNroFactura();
                                    $(".pago").val(0);
                                    $("input[name='vuelto']").val(0);
                                    if(parseFloat($("#importeTotal").html())==parseFloat($("#importeAFacturar").html().replace(/\./g,''))){
                                        document.location.reload();
                                    }
                                    $(".ajax_refresh_and_loading").trigger('click');
                                }else{
                                    $("#cargarFactura").attr('disabled',false);
                                    $("#messagebox").addClass('alert').addClass('alert-danger').html(data.error_message);
                                }
                            }
                        });
                }else{
                    $("#cargarFactura").attr('disabled',false);
                    $("#messagebox").addClass('alert').addClass('alert-danger').html(data.error_message);
                }
            }
        });
        return false;
    }
    
    function imprimir(codigo){
            window.open('<?= base_url('movimientos/ventas/ventas/imprimir/') ?>/'+codigo);
     }
     function imprimir2(codigo){
            window.open('<?= base_url('movimientos/ventas/ventas/imprimir2/') ?>/'+codigo);
     }
      function imprimirTicket(codigo){
            window.open('<?= base_url('movimientos/ventas/ventas/imprimirticket/') ?>/'+codigo);
     }     
     function imprimirCuenta(){
         window.open('<?= base_url('pedidos/admin/imprimirCuenta/'.$pedido) ?>');
     }
     
     function searchProduct(){
         $('#productosModal').modal('show');
     }
     
     $(document).on('keyup','#filtering_form tbody input',function(e){                         
         if(e.which==13){ //Enter
             e.preventDefault();
             return false;
         }
     });
     
     $(window).keyup(function(e){  
         //alert(e.which);
         if(e.which==27){ //ESC
             $(".modal-footer button").trigger('click');
         }
         if(e.which==115){ //F4             
             $(".pro").parent().find('.chosen-container').addClass('chosen-with-drop');
             $(".pro").parent().find('.chosen-search input').focus();
         }
         if(e.which==119){ //F8
             imprimirCuenta(); 
         }
         if(e.which==113){ //F2
             searchProduct();
         }
     });
     var query = undefined;
     var keyboard = undefined
     $(document).on('keyup','.ajax_list .chosen-search input',function(e){
         if(e.which!==40 && e.which!==38){                
                if(typeof(query)!=='undefined'){
                   query.abort();
                }
                if(typeof(keyboard)!=='undefined'){
                   clearTimeout(keyboard);
                }
                var val = $(this).val();
                keyboard = setTimeout(function(){
                    query = $.post('<?= base_url('movimientos/productos/buscador_productos/json_list') ?>',{'search_field[]':'nombre_comercial','search_text[]':val},function(data){
                        var data = JSON.parse(data);        
                        str = '<option>Seleccione una opcion</option>';
                        for(var i in data){
                            str+= '<option value="'+data[i].id+'" data-precio_venta="'+data[i].precio_venta+'" data-precio_costo="'+data[i].precio_costo+'">'+data[i].nombre_comercial+'</option>';
                        }
                        $(".pro").html(str);
                        $(".pro").trigger('chosen:updated');         
                        $(".ajax_list .chosen-search input").val(val);
                    });
                },800);
        }
     });
     
     $(document).on('change','.pro',function(){
         $(".inp.producto").val($(this).val());
         $(".inp.producto").attr('data-precio_venta',$(this).find('option:selected').data('precio_venta'));
         $(".inp.producto").attr('data-precio_costo',$(this).find('option:selected').data('precio_costo'));
         $(".inp.producto").trigger("change");
     });
     
     function enviarBusqueda(id,valor,precio_venta,precio_costo){
         $(".inp.producto").val(id);
         $(".inp.producto").attr('data-precio_venta',precio_venta);
         $(".inp.producto").attr('data-precio_costo',precio_costo);
     }
     
     function seleccionarProducto(id,valor,precio_venta,precio_costo){        
         enviarBusqueda(id,valor,precio_venta,precio_costo);
         $(".inp.producto").trigger("change");
         $(".pro").val(valor);
         $("#productosModal .modal-footer button").trigger('click');
     }
     
     <?php if($pedidos->tipo_pedidos_id!=2): ?>
        function sumartodoPedido(){
            $("#totaldelivery").val('0');
            var x = 0;
            $(".flexigrid .total").each(function(){            
                var total = parseFloat($("#totaldelivery").val().replace(/\./g,''));
                total = total+parseFloat($(this).val());
                $("#totaldelivery").val(total.formatMoney(0,',','.'));
                x++;
            });
        }    
        sumartodoPedido();
     <?php endif ?>
</script>
