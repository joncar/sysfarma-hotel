<?= $output ?>
<script>
    var todos = $("#deliverys_id_field_box,#mozos_id_field_box,#barras_id_field_box,#mesas_id_field_box");
    var delivery = $("#deliverys_id_field_box");
    var mozos = $("#mozos_id_field_box");
    var barras = $("#barras_id_field_box");  
    var mesas = $("#mesas_id_field_box");  
    mostrar();
    $("#field-tipo_pedidos_id").on('change',function(){
        mostrar();
    });
    
    function mostrar(){
        switch($("#field-tipo_pedidos_id").val()){
            case '1': //Mozos
                todos.hide();
                mesas.show();
                mozos.show();
            break;
            case '2': //Delivery
                todos.hide();
                delivery.show();
            break;
            case '3': //Barras
                todos.hide();
                barras.show();
            break;
            default:
                todos.hide();
            break;
        }
        $(".chosen-container").css('width','100%');
    }
</script>