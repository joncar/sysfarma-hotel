<?php

require_once APPPATH . '/controllers/Panel.php';

class Admin extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }            
    }

    function pedidos() {
        $crud = $this->crud_function('', '');
        $crud->field_type('user_id','hidden',$this->user->id)
                 ->field_type('facturado','hidden',0)
                 ->field_type('cajadiaria','hidden',$_SESSION['cajadiaria'])
                 ->field_type('sucursales_id','hidden',$this->user->sucursal)
                ->field_type('clientes_id','Cliente')
                 ->field_type('fecha_pedido','hidden',date("Y-m-d H:i:s"));
        if($crud->getParameters()=='list'){
            $crud->field_type('facturado','true_false',array('0'=>'<span style="color:red">NO</span>','1'=>'<span style="color:green">SI</span>'));
        }
        $crud->unset_searchs('total');
        if($crud->getParameters()=='list'){
            $crud->set_relation('clientes_id','clientes','{nombres} {apellidos}');
        }
        $crud->columns('id','clientes_id','fecha_pedido','mesas_id','deliverys_id','barras_id','facturado','total');
        $crud->callback_field('mozos_id',function($val,$row){
            get_instance()->db->select('mozos.id, user.nombre');
            get_instance()->db->join('mozos','mozos.user_id = user.id');
            return form_dropdown_from_query('mozos_id','user','id','nombre',$val,'id="field-mozos_id"');
        });
        $crud->callback_column('total',function($val,$row){
            $total = $this->db->query('select FORMAT(SUM(total),2,"de_DE") as total from pedidos_detalles where pedidos_id = '.$row->id)->row()->total;
            return $total;
        });

        $crud->callback_before_delete(function($primary){
            $pedido = get_instance()->db->get_where('pedidos',array('id'=>$primary))->row();
            if($pedido->facturado==1){
                return false;
            }else{
                switch($pedido->tipo_pedidos_id){
                    case '1':
                        get_instance()->db->update('mesas',array('estado'=>0),array('id'=>$pedido->mesas_id));
                    break;
                    case '2':
                        get_instance()->db->update('deliverys',array('estado'=>0),array('id'=>$pedido->deliverys_id));
                    break;
                    case '3':
                        get_instance()->db->update('barras',array('estado'=>0),array('id'=>$pedido->barras_id));
                    break;
                }
                get_instance()->db->delete('pedidos_detalles',array('pedidos_id'=>$primary));
            }
        });
        
        $crud->callback_after_insert(function($post,$primary){
            if(!empty($post['mesas_id'])){
                get_instance()->db->update('mesas',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["mesas_id"]));
            }
            if(!empty($post['deliverys_id'])){
                get_instance()->db->update('deliverys',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["deliverys_id"]));
            }
            if(!empty($post['barras_id'])){
                get_instance()->db->update('barras',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["barras_id"]));
            }
        });
        if($crud->getParameters()=='edit'){
            $crud->required_fields('tipos_pedidos_id');
        }else{
            $crud->set_rules("tipo_pedidos_id","tipo de pedido",'required|callback_validate_tipo_pedido');        
        }
        $crud->set_relation('mesas_id','mesas','mesa_nombre',array('sucursales_id'=>$this->user->sucursal));
        $crud->set_lang_string('insert_success_message','Pedido almacenado con éxito <script>document.location.href="'.base_url('pedidos/admin/pedidos_detalles').'/{id}/add"</script>');
        $crud->add_action('<i class="fa fa-book"></I> Detalles','',base_url('pedidos/admin/pedidos_detalles').'/');
        $crud->where('pedidos.sucursales_id',$this->user->sucursal);
        $crud->unset_columns('user_id');
        
        $crud = $crud->render();
        $crud->output = $this->load->view('pedidos',array('output'=>$crud->output),TRUE);
        $crud->title = 'Pedidos';
        $this->loadView($crud);
    }       
    
    function validate_tipo_pedido(){
        switch($_POST['tipo_pedidos_id']){
            case '1':
                if($this->db->get_where('mesas',array('id'=>$_POST['mesas_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Mesa ya ocupada");
                    return false;
                }
            break;
            case '2':
                if($this->db->get_where('deliverys',array('id'=>$_POST['deliverys_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Delivery ya ocupada");
                    return false;
                }
            break;
            case '3':
                if($this->db->get_where('barras',array('id'=>$_POST['barras_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Barra ya ocupada");
                    return false;
                }
            break;
        }
        
        return true;
    }
    //Una vez realizado la vista de carga de facturas con sus operaciones logicas, usar esta funcion para almacenar las ventas
    function ventas($x, $unset_delete = FALSE){        
        $crud = new ajax_grocery_CRUD($this);
        $crud->set_table("ventas");
        $crud->set_subject("Facturas");
        $crud->set_theme("bootstrap2");
        $crud->where("pedidos_id",$x);
        $crud->required_fields_array();
        $crud->set_rules("cliente","Cliente","required|integer");
        $crud->set_rules("transaccion","Transaccion","required|integer");
        $crud->set_rules("pedidos_id","Pedido","required|callback_validate_pedido");
        //$crud->set_rules("nro_factura","#Factura","required|is_unique[ventas.nro_factura]");
        if($this->router->fetch_method()!='ventas'){
            $crud->unset_add();
        }
        if($unset_delete){
           $crud->unset_delete(); 
        }
        $crud->unset_edit()
                 /*->unset_delete()*/
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();
        $crud->columns("fecha","nro_factura","total_venta");
        $crud->add_action('<i class="fa fa-search"></i> Ver Venta','',base_url("movimientos/ventas/ventas/edit")."/");
        $crud->add_action('<i class="fa fa-print"></i> Factura Legal','',base_url("movimientos/ventas/ventas/imprimir")."/");
        $crud->add_action('<i class="fa fa-print"></i> Factura Lineas','',base_url("movimientos/ventas/ventas/imprimir2")."/");
        $crud->add_action('<i class="fa fa-print"></i> Ticket','',base_url("movimientos/ventas/ventas/imprimirticket")."/");
        $crud->callback_before_insert(function($post){
            $post['total_efectivo'] = str_replace('.','',$post['total_efectivo']);
            $post['vuelto'] = str_replace('.','',$post['vuelto']);
            $post['total_debito'] = str_replace('.','',$post['total_debito']);
            $post['total_cheque'] = str_replace('.','',$post['total_cheque']);
            $post['total_credito'] = str_replace('.','',$post['total_credito']);
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){            
            $codigos = $_POST['codigo'];
            $cantidades = $_POST["cantidad"];
            $totales = $_POST['total'];
            foreach($totales as $n=>$t){
                $totales[$n] = str_replace('.','',$t);
            }
            foreach($codigos as $n=>$v){
                $data = array('venta'=>$primary);
                $v = $this->db->get_where('productos',array('id'=>$v))->row()->codigo;
                $data['producto'] = $v;
                $data['lote'] = '';
                $data['cantidad'] = $cantidades[$n];
                $data['pordesc'] = 0;
                $data['precioventa'] = $totales[$n];
                $data['precioventadesc'] = $totales[$n];
                $data['totalsindesc'] = 0;
                $data['totalcondesc'] = $totales[$n];
                $data['iva'] = 0;
                $this->db->insert('ventadetalle',$data);     
                //Liquidar
                $producto = $this->db->get_where('productos',array('codigo'=>$v))->row()->id;
                $cantidadpagada = 0;
                $totalpagada = 0;
                get_instance()->db->select('SUM(ventadetalle.cantidad) as cantidad, SUM(ventadetalle.precioventa) as precioventa');
                get_instance()->db->join('ventas','ventas.id = ventadetalle.venta');
                $yapagados = get_instance()->db->get_where('ventadetalle',array('pedidos_id'=>$post["pedidos_id"],'producto'=>$v));
                if($yapagados->num_rows()>0){
                    $cantidadpagada = $yapagados->row()->cantidad;
                    $totalpagada = $yapagados->row()->precioventa;
                }
                
                get_instance()->db->select("pedidos_detalles.*");
                $pedido = get_instance()->db->get_where("pedidos_detalles",array('pedidos_id'=>$post['pedidos_id'],'productos_id'=>$producto));
                if($pedido->num_rows()>0){                                        
                    if($cantidadpagada == $pedido->row()->cantidad && $totalpagada >= $pedido->row()->total){
                        get_instance()->db->update("pedidos_detalles",array("liquidar"=>1),array('id'=>$pedido->row()->id));                        
                    }
                }
            }
            $caja = $this->db->get_where('cajadiaria',array('id'=>$_SESSION['cajadiaria']));
            $correlativo = $caja->row()->correlativo+1;
            $this->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));
            //Actualizar mesa
            $pedido = get_instance()->db->get_where('pedidos',array('id'=>$post['pedidos_id']));
            get_instance()->db->where("pedidos_id",$pedido->row()->id);
            get_instance()->db->where("(liquidar IS NULL OR liquidar = 0)",'',FALSE);
            if(
                    $pedido->num_rows()>0 &&                     
                    get_instance()->db->get_where("pedidos_detalles")->num_rows()==0
              ){
                get_instance()->db->update("mesas",array("estado"=>0,"pedidos_id"=>0),array("id"=>$pedido->row()->mesas_id));
                get_instance()->db->update("deliverys",array("estado"=>0,"pedidos_id"=>0),array("id"=>$pedido->row()->deliverys_id));
                get_instance()->db->update("barras",array("estado"=>0,"pedidos_id"=>0),array("id"=>$pedido->row()->barras_id));
                get_instance()->db->update("pedidos",array("facturado"=>1),array("id"=>$pedido->row()->id));
            }
        });
        $crud->callback_after_delete(function($primary){
            get_instance()->db->delete("ventadetalle",array("venta"=>$primary));
        });
        $crud->set_url("pedidos/admin/ventas/".$x."/");
        return $crud->render();
    }
    
    function validate_pedido(){
        if(!empty($_POST['codigo']) && !empty($_POST["cantidad"]) && !empty($_POST["total"])){
            $codigos = count($_POST['codigo']);
            $cantidades = count($_POST["cantidad"]);
            $totales = count($_POST['total']);
            if($codigos == $cantidades && $codigos==$totales){
                $total_venta = str_replace($_POST["total_venta"],'.','');
                $efectivo = str_replace($_POST['total_efectivo']-$_POST['vuelto'],'.','');
                $credito = str_replace($_POST['total_credito'],'.','');
                $debito = str_replace($_POST['total_debito'],'.','');
                $cheque = str_replace($_POST['total_cheque'],'.','');
                $total_pago = $efectivo+$credito+$cheque+$debito;
                if($total_pago!=$total_venta){
                    $this->form_validation->set_message("validate_pedido","Ha ocurrido un error al cargar el pago, verifique que el monto expedido en Pendiente por Cancelar sea igual a 0");
                    return false;
                }
            }else{
                $this->form_validation->set_message("validate_pedido","Los datos han llegado incompletos");
                return false;
            }
        }else{
            $this->form_validation->set_message("validate_pedido","Los datos ingresados estan incompletos");
            return false;
        }
    }
    
    function pedidos_detalles($x = '') {        
        if(is_numeric($x)){            
            $pedido = $this->db->get_where('pedidos',array('id'=>$x));
            $crud = $this->crud_function('', '');    
            $crud->set_subject("Detalle");
            $crud->where('pedidos_id',$x);
            $crud->field_type('pedidos_id','hidden',$x);            
            $crud->field_type('liquidar','hidden',0);
            $crud->field_type('precio_costo','hidden',0);
            $crud->unset_columns('pedidos_id');
            $crud->callback_after_insert(function($post){get_instance()->actualizar_stock($post);});
            
            $this->db->select("productos.id, productos.nombre_comercial, productos.precio_costo, productos.precio_venta");
            $this->db->join("productosucursal","productos.codigo = productosucursal.producto","left");
            $this->db->order_by("productos.nombre_comercial");
            $this->db->where("productosucursal.stock >",0);
            $this->db->limit(50);
            $this->db->or_where("productos.no_inventariable",1);
            $productos = $this->db->get('productos');
            $str = '<select name="pro" class="pro chosen-select">';
            $str.= '<option value="">Escriba el nombre del producto para buscarlo</option>';
            foreach($productos->result() as $p){
                $str.= "<option value='".$p->id."' data-precio_costo='".$p->precio_costo."' data-precio_venta='".$p->precio_venta."'>".$p->nombre_comercial."</option>";
            }
            $str.= '</select>'.$this->load->view('predesign/chosen',array(),TRUE);
            $this->fieldProducto = $str;
                
            $crud->callback_before_update(function($post,$primary){
                $data = get_instance()->db->get_where("pedidos_detalles",array("id"=>$primary))->row();
                get_instance()->reversar_stock($data);
                get_instance()->actualizar_stock($post);
            });
            $crud->callback_before_delete(function($primary){
                $data = get_instance()->db->get_where("pedidos_detalles",array("id"=>$primary))->row();
                get_instance()->reversar_stock($data);                
            });
            $crud->set_relation("productos_id","productos","{id}|{codigo}|{nombre_comercial}");            
            $crud->display_as("j4b04c546.nombre_comercial","Producto")
                     ->display_as("liquidar","Liquidado");
            $crud->columns("sel","j4b04c546.nombre_comercial","detalle","precio_venta","cantidad","total",'liquidar');    
            $crud->unset_searchs("sel");
            
            $crud->callback_column('sel',function($val,$row){
                $producto = explode('|',$row->s4b04c546)[0];
                get_instance()->db->select('SUM(ventadetalle.cantidad) as cantidad, SUM(ventadetalle.precioventa) as precioventa');
                get_instance()->db->join('ventas','ventas.id = ventadetalle.venta');
                $yapagados = get_instance()->db->get_where('ventadetalle',array('pedidos_id'=>$row->pedidos_id,'producto'=>$producto));
                $cantidadpagada = 0;
                if($yapagados->num_rows()>0){
                    $cantidadpagada = $yapagados->row()->cantidad;
                    $totalpagado = $yapagados->row()->precioventa;
                }
                $cantidad = $row->liquidar?0:$row->cantidad;
                $cantidad-= $cantidad>0?$cantidadpagada:0;
                $precio = $row->precio_venta;
                $total = $cantidad*$precio;
                $input =  "<input title='Marcar para facturar' type='checkbox' value='".$producto."' name='codigo[]' data-nombre='".$row->s4b04c546."' data-precio='".$precio."' data-cantidad='".$cantidad."' data-total='".$total."' id='a".$row->id."' class='chk'>";
                return $row->liquidar?'':$input;
            });
            if($pedido->row()->facturado==0){
                $crud->callback_column('j4b04c546.nombre_comercial',function($val,$row){
                    $val = explode('|',$row->s4b04c546)[0];
                    return  '<input type="hidden" name="productos_id" value="'.$val.'" class="producto">'.explode('|',$row->s4b04c546)[2];                
                });

                $crud->callback_column('detalle',function($val,$row){
                    return '<input type="text" name="detalle" class="detalle" value="'.$val.'" data-rowid="'.$row->id.'">';
                });

                $crud->callback_column('cantidad',function($val,$row){
                    return '<input type="text" id="c'.$row->productos_id.'" name="cantidad" class="cantidad" value="'.$val.'" data-rowid="'.$row->id.'">';
                });

                $crud->callback_column('precio_venta',function($val,$row){
                    return '<input type="text" name="precio_venta" class="precio_venta" value="'.$val.'" data-rowid="'.$row->id.'"><input type="hidden" name="precio_costo" class="precio_costo" value="'.$row->precio_costo.'">';
                }); 

                $crud->callback_column('total',function($val,$row){
                    return '<input type="hidden" name="total" class="total" value="'.$val.'">'.$val;
                });
            }else{
                $crud->callback_column('j4b04c546.nombre_comercial',function($val,$row){
                    return explode('|',$row->s4b04c546)[2];
                });
                $crud->unset_edit()
                         ->unset_delete();
            }
            $crud->callback_column('liquidar',function($val,$row){
                return $val?'SI':'<span class="label label-danger">NO</span>';
            });          
            if($crud->getParameters()=='add'){
                $crud->set_rules("pedidos_id","Pedido","required|callback_validate_pedido_detalle");            
            }
            $crud->unset_read()
                     ->unset_print()
                     ->unset_export();
            $accion = $crud->getParameters();
            if($pedido->row()->facturado!=1){
                $crud = $crud->render('','application/modules/pedidos/views/');
            }else{
                $crud = $crud->render('');
            }
            if($accion!='list'){
                $crud->output = $this->load->view('pedidos_detalles',array('pedido'=>$pedido->row(),'output'=>$crud->output),TRUE);
            }else{
                $unset_delete_factura = $pedido->row()->facturado?TRUE:FALSE;
                $crud->output = $this->load->view('facturar',array('pedidos'=>$pedido->row(),"pedido"=>$x,"facturas"=>$this->ventas($x,$unset_delete_factura),'output'=>$crud->output),TRUE);
            }
            $crud->title = 'Detalle del Pedido';
            $this->loadView($crud);
        }else{
            redirect('panel');
        }
    }   
        
    function validate_pedido_detalle(){
        $post = $_POST;
        
        $pedido = $this->db->get_where('pedidos_detalles',array('pedidos_id'=>$post['pedidos_id'],'productos_id'=>$post['productos_id']));
        if($pedido->num_rows()>0){
            $data = array();
            $data['cantidad'] = $pedido->row()->cantidad+$post['cantidad'];
            $data['total'] = $data['cantidad']*$pedido->row()->precio_venta;
            $this->db->update('pedidos_detalles',$data,array('id'=>$pedido->row()->id));
            $this->actualizar_stock($post);
            $this->form_validation->set_message("validate_pedido_detalle","Se ha añadido la cantidad asignada a otro item ya existente <script>$(\"input,textarea,select\").val('');</script>");
            return false;
        }
        return true;
    }
    
    function actualizar_stock($post){
        //Actualizar stock
        $this->db->select("productosucursal.*");
        $this->db->join("productos","productos.codigo = productosucursal.producto");
        $where = array('productos.id'=>$post['productos_id'],'productosucursal.sucursal'=>$_SESSION['sucursal'],'productosucursal.stock >'=>0,'productos.no_inventariable'=>0);        
        $stock = $this->db->get_where('productosucursal',$where);
        $st = $post['cantidad'];        
        foreach($stock->result() as $s){  
            if($st<0)$st = $st*-1;
            $st = $s->stock-$st;
            $st2 = $st<0?0:$st;
            $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$s->id));                            
        }
    }
    
    function reversar_stock($post){
        $post = (array)$post;
        //Actualizar stock
        $this->db->select("productosucursal.*");
        $this->db->join("productos","productos.codigo = productosucursal.producto");
        $where = array('productos.id'=>$post['productos_id'],'productosucursal.sucursal'=>$_SESSION['sucursal']);
        $stock = $this->db->get_where('productosucursal',$where);
        $st = $post['cantidad'];
        if($stock->num_rows()>0){
            $st2 = $stock->row()->stock+$st;
            $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$stock->row()->id));
        }
    }
    
    function imprimirCuenta($id){
        $this->db->select('pedidos.*,pedidos.fecha_pedido as fecha, sucursales.denominacion, sucursales.telefono, sucursales.direccion');        
        $this->db->join('sucursales', 'sucursales.id = pedidos.sucursales_id');
        $venta = $this->db->get_where('pedidos', array('pedidos.id' => $id));
        if($venta->num_rows()>0){
            $this->db->select('productos.codigo as producto, pedidos_detalles.cantidad, FORMAT(pedidos_detalles.precio_venta,0,"de_DE") as precioventa, FORMAT(pedidos_detalles.total,0,"de_DE") as totalcondesc, pedidos_detalles.total');            
            $this->db->join('productos','productos.id = pedidos_detalles.productos_id');
            $detalles = $this->db->get_where('pedidos_detalles',array('pedidos_id'=>$id));
            $this->load->view('reportes/imprimirCuenta', array('venta' => $venta->row(), 'detalles' => $detalles));
        }
    }

}

?>
