<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/Panel.php';    
class Reportes extends Panel {
        
        function __construct()
        {
                parent::__construct();  
                ini_set('memory_limit','200M');
        }

        function report($crud = '',$x ='',$y = '')
        {
                $this->loadView('reportes');
        }
        
        function loadView($view = ''){
            $output = $this->load->view($view,array(),TRUE);
            parent::loadView(array('view'=>'panel','crud'=>'user','output'=>$output));
        }
        
        public function resumen_compras(){            
            if(empty($_POST)){
                $this->loadView('reportes/resumen_compras');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_compras',array(),TRUE)));
                ob_clean();
                $html2pdf->Output('Resumen de compras.pdf');
            }
        }
        
        public function resumen_ventas(){
                if(empty($_POST)){
                    $this->loadView('reportes/resumen_ventas');
                }
                else{
                    $papel = 'A4';
                    $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                    $html2pdf->setDefaultFont('courier');
                    $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_ventas',array(),TRUE)));
                    ob_clean();
                    $html2pdf->Output('Resumen de ventas.pdf');

                    //$this->load->view('reportes/resumen_ventas');
                }
        }
        
        public function obligaciones_proveedor(){
                    if(empty($_POST)){
                        $this->loadView('reportes/obligaciones_proveedor');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/obligaciones_proveedor',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('Resumen de obligaciones proveedor.pdf');

                        //$this->load->view('reportes/obligaciones_proveedor');
                    }       
        }
        
        public function ventas_sucursal(){
            
                    if(empty($_POST)){
                        $this->loadView('reportes/ventas_sucursal');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/ventas_sucursal',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('Resumen de ventas por sucursal.pdf');

                        //$this->load->view('reportes/ventas_sucursal');
                    }            
           
        }
        
        public function vencimientos(){
                   
                    if(empty($_POST)){
                        $this->loadView('reportes/vencimientos');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/vencimientos',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('Resumen de vencimientos.pdf');

                        //$this->load->view('reportes/vencimientos');
                    }
            
        }
        
        public function saldos($x = '', $y = ''){
           
                    if(empty($_POST)){
                        $this->loadView('reportes/saldos');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/saldos',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('Resumen de saldo de clientes.pdf');

                        //$this->load->view('reportes/saldos');
                    }
            
        }
        
        public function resumen_vencimiento(){
                 
                    if(empty($_POST)){
                        $this->loadView('reportes/resumen_vencimiento');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_vencimiento',array(),TRUE)));
                        //$html2pdf->Output('Resumen de vencimiento.pdf');

                        $this->load->view('reportes/resumen_vencimiento');
                    }
            
        }
        
        public function resumen_descuentos(){
            
                    if(empty($_POST)){
                        $this->loadView('reportes/resumen_descuentos');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_descuentos',array(),TRUE)));
                        //$html2pdf->Output('Resumen de vencimiento.pdf');

                        $this->load->view('reportes/resumen_descuentos');
                    }
            
        }
        
        public function listado_inventario(){
            
                    if(empty($_POST)){
                        $this->loadView('reportes/listado_inventario');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_inventario',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('Listado de inventario.pdf');

                        //$this->load->view('reportes/listado_inventario');
                    }
            
        }
        
        public function listado_inventario_total(){
           
                    if(empty($_POST)){
                        $this->loadView('reportes/listado_inventario_total');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_inventario_total',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('Listado de inventario.pdf');

                        $this->load->view('reportes/listado_inventario_total');
                    }
            
        }
        public function listado_inventario_categoria(){
           
                if(empty($_POST)){
                    $this->loadView('reportes/listado_inventario_categoria');
                }
                else{
                    $papel = 'A4';
                    $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                    $html2pdf->setDefaultFont('courier');
                    $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_inventario_categoria',array(),TRUE)));
                    ob_clean();
                    $html2pdf->Output('Listado de inventario.pdf','D');

                    //$this->load->view('reportes/listado_inventario_categoria');
                }   
            
        }
        
        public function listado_ventas_clientes_creditos(){
            
                    if(empty($_POST)){
                        $this->loadView('reportes/listado_ventas_clientes_creditos');
                    }
                    else{
                        $papel = 'A4';
                        //$html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        //$html2pdf->setDefaultFont('courier');
                        //$html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_ventas_clientes_creditos',array(),TRUE)));
                        //$html2pdf->Output('Listado de ventas por cliente.pdf','D');

                        $this->load->view('reportes/listado_ventas_clientes_creditos');
                    }
            
        }
        
        public function listado_ventas_clientes(){
            
                    if(empty($_POST)){
                        $this->loadView('reportes/listado_ventas_clientes');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        //$html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_ventas_clientes',array(),TRUE)));
                        //$html2pdf->Output('Listado de ventas por cliente.pdf','D');

                        $this->load->view('reportes/listado_ventas_clientes');
                    }
            
        }
        
        public function detalle_ventas($cajadiaria = ''){
                $cajadiaria = empty($cajadiaria)?$_SESSION['cajadiaria']:$cajadiaria;
                $papel = 'A4';
                $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/detalle_ventas',array('cajadiaria'=>$cajadiaria),TRUE)));
                ob_clean();
                //$html2pdf->Output('Detalle-de-ventas-por-dia.pdf','D');

                $this->load->view('reportes/detalle_ventas',array('cajadiaria'=>$cajadiaria));
        }
        
        public function detalle_ventas2(){
            
                    if(empty($_SESSION['sucursal'])){
                        header("Location:".base_url('panel/selsucursal'));
                    }
                    if(empty($_SESSION['caja'])){
                        header("Location:".base_url('panel/selcaja'));
                    }
                    if(empty($_POST)){
                        $this->loadView('reportes/detalle_ventas2');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/detalle_ventas2',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('Detalle-de-ventas-por-dia.pdf','D');

                        //$this->load->view('reportes/detalle_ventas2');
                    }
           
        }
        
        public function resumen_ventas_clientes(){
           
                    if(empty($_POST)){
                        $this->loadView('reportes/resumen_ventas_clientes');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        //$html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_ventas_clientes',array(),TRUE)));
                        //$html2pdf->Output('resumen de ventas por clientes.pdf','D');

                        $this->load->view('reportes/resumen_ventas_clientes');
                    }
           
        }
        
        public function pagos_clientes(){
            
                    if(empty($_POST)){
                        $this->loadView('reportes/pagos_clientes');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/pagos_clientes',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('resumen de pagos por clientes.pdf','D');

                        //$this->load->view('reportes/pagos_clientes');
                    }
           
        }
        
        public function resumen_anual(){
            
                    if(empty($_POST) || empty($_POST['anio'])){
                        $this->loadView('reportes/resumen_anual');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_anual',array(),TRUE)));
                        ob_clean();
                        $html2pdf->Output('resumen anual de ventas.pdf','D');

                        //$this->load->view('reportes/resumen_anual');
                    }
            
        }
        
        public function inventario_valorizado(){
            
                    if(empty($_POST)){
                        $this->loadView('reportes/inventario_valorizado');
                    }
                    else{
                        $papel = 'A4';       
                        $where = !empty($_POST['sucursal'])?'ps.sucursal = '.$_POST['sucursal']:'';
                        $query = "
                            select ps.producto, ps.nombre_comercial, ps.cantidad, ps.precio as precio_venta, (ps.cantidad*ps.precio) as total
                            from  (
                                    select 
                                            ps.producto,
                                            p.nombre_comercial,
                                            (select SUM(stock) as cantidad from productosucursal where producto = ps.producto) as cantidad,
                                            (select precio_venta as precio from productosucursal where producto = ps.producto order by precio_venta DESC limit 1) as precio
                                            FROM productosucursal ps
                                            JOIN productos as p ON p.codigo = ps.producto 
                                            WHERE ".$where."
                                            group by ps.producto
                            )
                            as ps
                            where ps.cantidad > 0
                            UNION 
                            select '<b>Total</b>' as producto, ' ' as nombre_comercial, SUM(ps.cantidad) as cantidad, SUM(ps.precio) as precio_venta, SUM((ps.cantidad*ps.precio)) as total
                            from  (
                                    select 
                                            ps.producto,
                                            p.nombre_comercial,
                                            (select SUM(stock) as cantidad from productosucursal where producto = ps.producto) as cantidad,
                                            (select precio_venta as precio from productosucursal where producto = ps.producto order by precio_venta DESC limit 1) as precio
                                            FROM productosucursal ps
                                            JOIN productos as p ON p.codigo = ps.producto 
                                            WHERE ".$where."
                                            group by ps.producto
                            )
                            as ps
                            where ps.cantidad > 0
                            ";
                        $q = $this->db->query($query);
                        $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/inventario_valorizado',array('data'=>$q),TRUE)));
                        ob_clean();
                        $html2pdf->Output('inventario-valorizado.pdf','D');                        
                        //$this->load->view('reportes/inventario_valorizado',array('data'=>$q));
                    }
           
        }
        
        public function resumen_facturas_contador($x = '', $y = ''){
            
                    if(empty($_POST)){
                        $this->loadView('reportes/resumen_facturas_contador');
                    }
                    else{
                        if($_POST['tipo']=='pdf'){
                            $papel = 'A4';
                            $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                            $html2pdf->setDefaultFont('courier');
                            $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_facturas_contador',array(),TRUE)));
                            ob_clean();
                            $html2pdf->Output('Resumen de facturas para contador.pdf');
                        }else{
                            $this->load->view('reportes/resumen_facturas_contador');
                        }                        
                    }
           
        }
        
        public function movimientos_clientes($x = '', $y = ''){
            
                    if(empty($_POST) || empty($_POST['clientes'])){
                        $this->loadView('reportes/movimientos_clientes');
                    }
                    else{
                        $papel = 'A4';
                        $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                        $html2pdf->setDefaultFont('courier');
                        //$html2pdf->writeHTML(utf8_decode($this->load->view('reportes/movimientos_clientes',array(),TRUE)));
                        //$html2pdf->Output('Resumen de movimientos de  clientes.pdf');

                        $this->load->view('reportes/movimientos_clientes');
                    }
          
        }
        /*Cruds*/ 
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
