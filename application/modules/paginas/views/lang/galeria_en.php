<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb5.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Galeria</div>
                <div class="sub-title">Experimenta amb imatges</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="current"><a href="#">Galeria</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!-- Gallery -->
<section id="gallery">
    <div class="inner-container container">        

        <!-- Gallery Container -->
        <div class="gallery-container">
            <div class="sort-section">
                <div class="sort-section-container">
                    <div class="sort-handle">Filtres</div>
                    <ul class="list-inline">
                        <li><a href="#" data-filter=".main">Totes</a></li>
                        <?php foreach($this->db->get('categorias_fotos')->result() as $c): ?>                            
                            <li><a href="#" data-filter=".<?= $c->id ?>cat" class="<?= $c->id==1?'active':'' ?>"><?= $c->nombre ?></a></li>                            
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <ul class="image-main-box clearfix" data-default="1">                
                <?php foreach($this->db->get('categorias_fotos')->result() as $c): ?>     
                    <?php $this->db->where('idioma','en'); ?>
                    <?php foreach($this->db->get_where('fotos',array('categorias_fotos_id'=>$c->id))->result() as $f): ?>
                        <li class="item col-xs-6 col-md-4 <?= $c->id ?>cat main">
                            <figure>
                                <img src="<?= base_url('img/entorno/'.$f->foto) ?>" alt="11"/>
                                <?php if(empty($f->link)): ?>
                                    <a href="<?= base_url('img/entorno/'.$f->foto) ?>" class="more-details" data-title="<?= $f->titulo ?>"><?= $f->titulo ?></a>
                                <?php else: ?>
                                    <a href="<?= base_url('img/entorno/'.$f->foto) ?>" class="more-details" data-title="<a href='<?= $f->link ?>'><?= $f->titulo ?>"><?= $f->titulo ?></a>
                                <?php endif ?>
                                <figcaption>
                                    <h4><?= $f->titulo ?></h4>
                                </figcaption>
                            </figure>
                        </li>
                    <?php endforeach ?>
                <?php endforeach ?>
            </ul>            
        </div>
    </div>
</section>
<!-- End of Gallery -->
<!--Footer Section-->
<?php $this->load->view('includes/template/footer'); ?>
