<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Nosaltres</div>
                <div class="sub-title">Una casa amb molta història</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li class="current"><a href="#">Nosaltres</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="has-user">
    <div class="inner-container container">
        <div class="l-sec col-md-7">
            <div class="ravis-title-t-1">
                <div class="title"><span>Cal Fuster de la Plaça</span></div>
                <div class="sub-title">Allotjament amb encant i molta història</div>
            </div>
            <div class="content">
                Situada en mig del casc antic de Sant Martí de Tous, vila mil·lenària plena d’històries, llegendes i paratges encantadors, la casa recull l’encant i el confort que el lloc proporciona.

Pedra, fusta i materials nobles, s’hi combinen vestigis gòtics amb moderns detalls que la fan còmode i confortable.

En aquest web trobareu un parell de pinzellades de qui som i què oferim, el servei del que gaudireu a Cal Fuster de la Plaça va molt més enllà del d’una simple casa de turisme rural…
            </div>
        </div>
        <div class="r-sec col-md-5">
            <div class="user-img-box">
                <div class="inner-box">
                    <img src="<?= base_url() ?>img/hotel-manager.png" alt="Chris Coleman - General Manager">
                </div>
            </div>
        </div>
    </div>
</section>
<!--End of Welcome Section-->

<!-- Video Tour -->
<section id="video-tour" data-bg-img="<?= base_url() ?>img/video.jpg">
    <div class="inner-container container">
        <div class="row">
            <div class="l-sec col-md-6">
                <div class="ravis-title">
                    <div class="inner-box">
                        <div class="title">Vídeo Tour</div>
                        <div class="sub-title">Visita'ns a través d'aquest fantàstic vídeo Tour</div>
                    </div>
                </div>

            </div>
            <div class="r-sec col-md-6">
                Visita'ns virtualment a través d'aquest vídeo tour, coneix tots els racons de la casa, les sales, els serveis... Però la millor manera de gaudir-ho és venint a conèixe'ns! Estarem encantats d'allotjar-te amb nosaltres i oferir-te la millor estança a Cal Fuster de la plaça.
            </div>
        </div>
        <div class="row btn-box">
            <a href="http://www.youtube.com/watch?v=23BFVDEdi-0" class="play-btn video-url">
                <i class="fa fa-play"></i>
            </a>
        </div>
    </div>
</section>
<!-- End of Video Tour -->

<!-- Our History -->
<section id="history-section">
    <div class="inner-container container">
        <div class="ravis-title-t-2">
            <div class="title"><span>La nostra història</span></div>
            <div class="sub-title">Senyors, llegendes, història mil·lenària</div>
        </div>

        <div class="desc">
            La casa contempla la història de Sant Martí de Tous a través de les seves parets de pedra, i fa esment de les diferents llegendes i paratges de Tous, donant nom a habitacions, racons i diferents estances de l’immoble, així doncs podreu fer un recorregut llegendari dins la mateixa casa, que us traslladarà a un món ple d’història, confort i bon servei.
        </div>

        <!-- History Main Container -->
        <div class="history-timeline clearfix">
            <!-- History Box ( You can create History boxes as much as you want! But Do Not change the classes and attributes ) -->
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>880</i><!-- History's date (Do Not remove it) -->
                    <h5>La Conquesta</h5><!-- History's title -->
                    <!-- History's description -->
                    <div class="history-content">
                        Guifre el Pelós va conquerir el lloc de Tous vers el 880.
                    </div>
                    <!-- end of History's description -->
                </div>
            </div>
            <!-- End of History box -->
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInRight">
                <div class="history-boxes-inner">
                    <i>960</i>
                    <h5> Visvat de Vic</h5>
                    <div class="history-content">
                        Borrell I, comte de Barcelona i d’Osona, donà a l’Església de Vic el territori del castell de Tous, que es considerava aleshores part del terme del castell de Montbui, en una bona part erm i despoblat.                    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>1020</i>
                    <h5>Visita del bisbe i abat Oliba</h5>
                    <div class="history-content">
                         El país es començà a repoblar vers el 1015, però el bisbe i abat Oliba, a l’agost del 1020, va anar a Tous a endegar definitivament el nou repoblament i a assegurar la seva defensa encomanant-lo a Guillem de Mediona. El 1043 va tornar per a consagrar l’església de la Roqueta.
                    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInRight">
                <div class="history-boxes-inner">
                    <i>1032</i>
                    <h5>Domini dels cognominats Tous</h5>
                    <div class="history-content">
                        Mort Guillem d’Oló, el 1032, el castell fou confiat a uns castlans cognominats Tous, que sovint en pretengueren el domini absolut.
                    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>1341</i>
                    <h5>Bernat de Tous recupera protagonisme</h5>
                    <div class="history-content">
                        Bernat de Tous fou ambaixador a França el 1341; home de confiança del rei Pere III el Cerimoniós, aquest li vengué la jurisdicció del castell de Tous el 1347.              </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInRight">
                <div class="history-boxes-inner">
                    <i>1423</i>
                    <h5>El castell a subhasta</h5>
                    <div class="history-content">
                        La dita sobre la malgastadora senyora de Tous que recull mossèn Cinto Verdaguer en una poesia datada el 1897 (“Si jo hagués sabut que era menja tan bona / lo pa de mestall barrejat amb les nous, / ai! no envejaria del rei la corona / i seria encara senyora de Tous.”) sembla que es refereix a Beatriu de Vilanova, vídua de Bernat de Tous, quan el 1423 el castell fou posat a pública subhasta a instàncies dels creditors i adquirit per Ponç de Perellós. La seva germana, Joana de Perellós, va vendre (1441) el castell al noble Joan Saplana, i passà als seus successors. El 1505, Isabel Saplana decidí de fer donació pactada del castell al monestir de Sant Jeroni de la Murtra.                    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>1505</i>
                    <h5>Castell de Tous és el centre del règim senyorial</h5>
                    <div class="history-content">
                        Del 1505 al 1835, el castell de Tous fou el centre del règim senyorial que exerciren els religiosos de la comunitat del monestir de Sant Jeroni de la Murtra, situat als afores de Badalona. Precisament, per la distància que els separava, els frares delegaren les seves funcions judicials amb el nomenament de procuradors i, entre altres nobles, ho feren repetidament alguns membres del comtat de Santa Coloma de Queralt.              </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInRight">
                <div class="history-boxes-inner">
                    <i>1820</i>
                    <h5>Desamortitzacions</h5>
                    <div class="history-content">
                        En compliment de les disposicions desamortitzadores del 1820 i del 1821, el castell de Tous amb les seves terres, que comprenien 249 jornals de llaurar i dos molins fariners, fou posat a la subhasta pública i els frares jerònims hagueren de fugir, bé que, a la fi del 1833, l’orde de Sant Jeroni va recuperar el domini.    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>1835</i>
                    <h5>Transmissions i vendes del castell</h5>
                    <div class="history-content">
                        El 1835 fou aplicada la desamortització i els frares jerònims hagueren d’abandonar per sempre el lloc de Tous. El creditor, Ignasi Codina, el mateix 1835, féu venda del castell a Marçal Grau, que el 1864 el vengué a Ramon de Miquel i Recasens i posteriorment es feren altres transmissions. </div>
                </div>
            </div>
        </div>
        <!-- End of Services Main Container -->

    </div>
</section>
<!-- End of Our History -->
<!--Footer Section-->
<?php $this->load->view('includes/template/footer'); ?>
