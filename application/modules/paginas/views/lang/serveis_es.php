<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb2.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Nuestros servicios</div>
                <div class="sub-title">Espacios y servicios que encontraréis en nuestro alojamiento</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inicio</a></li>
                <li class="current"><a href="#">Servicios</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="simple">
    <div class="inner-container container">
        <div class="ravis-title-t-2">
            <div class="title"><span>Cal Fuster de la Plaça</span></div>
            <div class="sub-title">Encontrarás todo lo que buscas en un espacio único</div>
        </div>
        <div class="content">
            En Cal Fuster de la Plaça encontrarás todo lo que necesitas. Un alojamiento rural donde podrás disfrutar su encanto. su historia, su entorno único y espacios que se adecuarán a vuestras necesidades.
        </div>
    </div>
</section>
<!--End of Welcome Section-->

<!-- Hotel Sections -->
<section id="hotel-sections">
    <div class="section-row clearfix">
        <div class="desc animated-box" data-animation="fadeInUp" data-delay="400">
            <div class="ravis-title-t-1">
                <div class="title"><span>Colección de Albañilería</span></div>
                <div class="sub-title">Espacio dedicado al oficio del albañil</div>
            </div>
            <div class="content">
                En el recibidor de la casa podréis visitar el minucioso y variado museo dedicado al oficio del albañil. </br> Los antepasados familiares de Cal Fuster de la Plaça, como bien delata el nombre de la casa, se dedicaron a la albañilería. En este pequeño museo encontraréis las herramientos de los antepasado así como todo el material que ha pasado de generación en generación de Cal Fuster de Tous. <br> Si lo deseáis podéis pedir una pequeña visita guiada que os ofrecerá anécdotas y pequeñas explicaciones de este gran oficio.
            </div>
        </div>
        <div class="img-container animated-box" data-animation="fadeInUp"
             data-bg-img="<?= base_url() ?>img/hotel-section/1.jpg"></div>
    </div>
    <div class="section-row clearfix">
        <div class="img-container animated-box" data-animation="fadeInUp"
             data-bg-img="<?= base_url() ?>img/hotel-section/2.jpg"></div>
        <div class="desc animated-box" data-animation="fadeInUp" data-delay="400">
            <div class="ravis-title-t-1">
                <div class="title"><span>Sala dels Nobles</span></div>
                <div class="sub-title">Espacio reservado para encuentros, recreo, reuniones...</div>
            </div>
            <div class="content">
                A la parte baja de la casa podréis disfrutar de un espacio amplio para pequeñas reuniones, ver la televisión, jugar a juegos de mesa... </br> Se trata de una sala a pie de calle, es una zona común para todos nuestros clientes. </br> En caso de necesitar sillas, mesa para reuniones... no dudáis a solicitarlo, procuraremos hacer de vuestra estancia la mejor.
            </div>
        </div>
    </div>
    <div class="section-row clearfix">
        <div class="desc animated-box" data-animation="fadeInUp" data-delay="400">
            <div class="ravis-title-t-1">
                <div class="title"><span>Zona de aguas LA FOU</span></div>
                <div class="sub-title">Piscina con vistas de lujo</div>
            </div>
            <div class="content">
                Si buscáis desconectar un rato del día a día y disfrutar de un baño tranquilo con vistas inmejorables sólo hará falta que subís en lo alto de la casa, la zona llamada La Fou. </br> Zona de descanso, solarium, piscina y muchos servicios para el descanso y tranquilidad de nuestros clientes. </br> En caso de necesitar toallas y otro material no dudáis a solicitarlo.
            </div>
        </div>
        <div class="img-container animated-box" data-animation="fadeInUp"
             data-bg-img="<?= base_url() ?>img/hotel-section/3.jpg"></div>
    </div>
</section>
<!-- End of Hotel Sections -->

<!--Our Services Section-->
<section id="our-services">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Otros servicios</div>
                <div class="sub-title">Trabajamos para hacer tu estancia inolvidable</div>
            </div>
        </div>

        <div id="services-box" class="owl-carousel owl-theme">
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/1.jpg" alt="6">
                <div class="title">Celler El Pagna</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/2.jpg" alt="2">
                <div class="title">Sauna</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/3.jpg" alt="3">
                <div class="title">Cocina y servicio de desayuno</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/4.jpg" alt="4">
                <div class="title">Sala La Fou</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/5.jpg" alt="5">
                <div class="title">Terraza</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/6.jpg" alt="1"><!-- Change the URL section based on your image's name -->
                <div class="title">Piscina La Fou</div>
            </div>
        </div>



    </div>
</section>
<!--End of Our Services Section-->

<!-- Testimonials Section -->
<!--<section id="testimonials-section" data-parallax="scroll" data-image-src="<?/*= base_url() */?>img/testimonails.jpg">
    <div class="inner-container container">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="guest">
                    <div class="ravis-title">
                        <div class="inner-box">
                            <div class="title">Christopher Robinson</div>
                            <div class="sub-title">Accountant in Apple Inc.</div>
                        </div>
                    </div>
                </div>
                <div class="text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
                </div>
            </div>
            <div class="item">
                <div class="guest">
                    <div class="ravis-title">
                        <div class="inner-box">
                            <div class="title">Jeniffer Chapman</div>
                            <div class="sub-title">Freelance Photographer</div>
                        </div>
                    </div>
                </div>
                <div class="text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
                </div>
            </div>
            <div class="item">
                <div class="guest">
                    <div class="ravis-title">
                        <div class="inner-box">
                            <div class="title">Jeremy Brown</div>
                            <div class="sub-title">Mechanical Engineer</div>
                        </div>
                    </div>
                </div>
                <div class="text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
                </div>
            </div>
            <div class="item">
                <div class="guest">
                    <div class="ravis-title">
                        <div class="inner-box">
                            <div class="title">Jack Norman</div>
                            <div class="sub-title">Web Designer in RavisTheme</div>
                        </div>
                    </div>
                </div>
                <div class="text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- End of Testimonials Section -->

<!--Client Sections-->
<section id="client-section">
    <div class="inner-container container">
        <ul class="client-list clearfix">
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/altria1.png" alt="Client Logo">
                </a>
            </li>
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp" data-delay="400">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/bluehost.png" alt="Client Logo">
                </a>
            </li>
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp" data-delay="800">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/cube.png" alt="Client Logo">
                </a>
            </li>
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp" data-delay="1200">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/erikaschesonis1.png" alt="Client Logo">
                </a>
            </li>
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp" data-delay="1200">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/modernart.png" alt="Client Logo">
                </a>
            </li>
        </ul>
    </div>
</section>
<!--End of Client Sections-->
<?php $this->load->view('includes/template/footer'); ?>

<!--End of Footer Section-->
