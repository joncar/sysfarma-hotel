<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb4.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Eventos</div>
                <div class="sub-title">Disfruta de las actividades de la comarca</div>
            </div>
        </div>
        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Inicio</a></li>
                <li class="current"><a href="#">Eventos</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!-- Upcoming Events -->
<section id="gallery">
    <div class="inner-container container">

        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Próximos Eventos</div>
                <div class="sub-title">No sabrás cual escoger</div>
            </div>
        </div>

        <!-- Gallery Container -->
        <div class="gallery-container">
            <div class="sort-section">
                <div class="sort-section-container">
                    <div class="sort-handle">Filters</div>
                    <ul class="list-inline">
                        <li><a href="#" data-filter="*" class="active">Todos</a></li>
                        <?php foreach($this->db->get('eventos_tipos')->result() as $c): ?>                            
                                <li><a href="#" data-filter=".<?= $c->id ?>cat"><?= $c->nombre ?></a></li>                            
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <div class="event-container">
                <ul class="event-main-box clearfix">                                    
                                   
                    <?php $this->db->order_by('fecha','ASC'); ?>
                    <?php $this->db->where('fecha >=',date("Y-m-d")); ?>
                    <?php $this->db->where('idioma','es'); ?>
                    <?php 
                        $eventos = array();
                        $eventoslist = $this->db->get_where('eventos');
                        foreach($eventoslist->result() as $e){
                            $eventos[] = $e;
                        }
                        if(count($eventos)>1){
                            $aux = $eventos[0];
                            $eventos[0] = $eventos[1];
                            $eventos[1] = $aux;
                        }
                    ?>
                    <?php foreach($eventos as $n=>$c): ?>
                        <li class="item col-xs-6 col-md-<?= $n==1?8:4 ?> <?php echo $c->categoria_eventos_id ?>cat">                               
                            <figure>
                                <img src="<?= base_url('img/eventos/'.$c->foto) ?>" alt="<?= $c->id ?>"/>
                                <figcaption>
                                    <?php if(!empty($c->link)): ?>
                                        <a href="<?= $c->link ?>">
                                    <?php endif ?>
                                        <span class="title-box">
                                            <span class="fechaevento"><?= utf8_encode(strftime("%d %B %Y",strtotime($c->fecha))) ?></span>
                                            <span class="title"><?= $c->eventos_nombre ?></span>
                                            <span class="sub-title"> <?= $c->autor ?></span>
                                        </span>
                                        <span class="desc">
                                            <?= $c->descripcion ?>
                                        </span>
                                    <?php if(!empty($c->link)): ?>
                                        </a>
                                    <?php endif ?>
                                </figcaption>
                            </figure>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
<!--            <span style="font-size:12px;color: white;margin-bottom: -11px">-->
        </div>
    </div>
</section>
<!-- End of Upcoming Events -->

<!-- Past Events -->
<section id="past-events">
    <div class="inner-container container">

        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Events Pasados</div>
                <div class="sub-title">Espero que hayas disfrutado de algunos</div>
            </div>
        </div>

        <div class="event-container">
            <ul class="event-main-box clearfix">                
                <?php $this->db->order_by('fecha','DESC'); ?>
                <?php $this->db->where('fecha <=',date("Y-m-d")); ?>
                <?php $this->db->where('idioma','es'); ?>
                <?php foreach($this->db->get_where('eventos')->result() as $n=>$c): ?>
                    <li class="item col-xs-6 col-md-4 <?php echo $c->categoria_eventos_id ?>cat">
                        <figure>
                            <img src="<?= base_url('img/eventos/'.$c->foto) ?>" alt="11"/>
                            <figcaption>
                                <a href="<?= base_url('img/eventos/'.$c->foto) ?>" class="more-details" data-title="<?= $c->eventos_nombre ?> - <?= utf8_encode(strftime("%d %B %Y",strtotime($c->fecha))) ?>">
                                    <span class="title-box">
                                        <span class="fechaevento"><?= utf8_encode(strftime("%d %B %Y",strtotime($c->fecha))) ?></span>
                                        <span class="title"><?= $c->eventos_nombre ?></span>
                                        <span class="sub-title"> <?= $c->autor ?></span>
                                    </span>
                                    <span class="desc">
                                        <?= $c->descripcion ?>
                                    </span>
                                </a>
                            </figcaption>
                        </figure>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>       

    </div>
</section>
<!-- End of Past Events -->
<!--Footer Section-->
<?php $this->load->view('includes/template/footer'); ?>
<!--End of Footer Section-->
