
<div class="slider-available-sec">
    <!-- Main Slider -->
    <section id="main-slider">
        <div class="items">
            <div class="img-container" data-bg-img="img/slider/1.jpg"></div>
            <!-- Change the URL section based on your image\'s name -->
            <div class="slide-caption">
                <div class="inner-container clearfix">
                    <div class="up-sec">Disfruta de tu estancia en Cal Fuster de la plaça</div>
                    <div class="down-sec">Alojamientos & Servicios</div>
                </div>
            </div>
        </div>
        <div class="items">
            <div class="img-container" data-bg-img="img/slider/2.jpg"></div>
            <div class="slide-caption">
                <div class="inner-container clearfix">
                    <div class="up-sec">Estarem encantados de recibirte</div>
                    <div class="down-sec">Visítanos</div>
                </div>
            </div>
        </div>
        <div class="items">
            <div class="img-container" data-bg-img="img/slider/3.jpg"></div>
            <div class="slide-caption">
                <div class="inner-container clearfix">
                    <div class="up-sec">Disfruta de tu estancia</div>
                    <div class="down-sec">Suits de lujo</div>
                </div>
            </div>
        </div>
        <div class="items">
            <div class="img-container" data-bg-img="img/slider/4.jpg"></div>
            <div class="slide-caption">
                <div class="inner-container clearfix">
                    <div class="up-sec">Pasa una buena estancia con nosotros</div>
                    <div class="down-sec">Alojamiento privado y acogedor</div>
                </div>
            </div>
        </div>
    </section>
    <!-- End of Main Slider -->

    <!--Main Booking form-->
    <section id="main-availability-form">
        <div class="inner-container container">
            <div class="l-sec col-md-4">
                <div class="ravis-title">
                    <div class="inner-box">
                        <div class="title">Busca habitación</div>
                        <div class="sub-title">¿Cuando quieres alojarte con nosotros?</div>
                    </div>
                </div>
            </div>
            <div class="r-sec col-md-8">
                <form class="booking-form clearfix" action="<?= base_url('iniciar-reserva') ?>"><!-- Do Not remove the classes -->
                    <div class="col-md-10">
                        <div class="input-daterange row">
                            <div class="booking-fields col-md-6">
                                <input placeholder="Entrada" class="datepicker-fields check-in" type="text"
                                       name="start"/>
                                <!-- Date Picker field ( Do Not remove the "datepicker-fields" class ) -->
                                <i class="fa fa-calendar"></i><!-- Date Picker Icon -->
                            </div>
                            <div class="booking-fields col-md-6">
                                <input placeholder="Sortida" class="datepicker-fields check-out" type="text"
                                       name="end"/>
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="booking-fields col-md-4">
                                <!-- Select boxes ( you can change the items and its value based on your project's needs ) -->
                                <select name="room-type">
                                    <option value="">Nº Adultos</option>
                                    <!-- Select box items ( you can change the items and its value based on your project's needs ) -->
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                                <!-- End of Select boxes -->
                                <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="booking-fields col-md-4">
                                <select name="guest">
                                    <option value="">Nº Niños</option>
                                    <option value="1">1</option>
                                </select>
                                <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="booking-fields col-md-4">
                                <select name="bebes">
                                    <option value="">Nº Bebés</option>
                                    <option value="1">1</option>
                                </select>
                                <i class="fa fa-caret-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="ravis-btn btn-type-1">
                            <span class="inner-box">
                                Reservar ahora
                            </span>
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </section>
    <!--End of Main Booking form-->
</div>

<!--Welcome Section-->
<section id="welcome-section">
    <div class="inner-container container">
        <div class="l-sec col-md-7">
            <div class="ravis-title-t-1">
                <div class="title"><span>Cal fuster de la Plaça</span></div>
                <div class="sub-title">Podría ser una casa com cualquier otra, pero no es así…</div>
            </div>
            <div class="content">
                Situada en la antigua Plaza Mayor de Sant Martí de Tous, entre la Anoia, la Segarra y la Conca de Barberà, en el corazón de Cataluña, la casa tiene tanta historia como la que explican las arcadas góticas de su interior que van ligadas a la historia del municipio de Sant Martí de Tous. <br>
Tous, es un pueblo milenario del cual se tienen las primeras noticias en documentos y crónicas datados del 960. <br>

                La casa contempla la historia del pueblo a través de sus paredes de piedra, y hace mención de las diferentes leyendas y parajes de Tous, dando nombre a habitaciones, rincones y diferentes alojamientos del inmueble, así pues podréis hacer un recorrido legendario dentro de la misma casa, que os trasladará a un mundo lleno de historia, confort y buen servicio.
            </div>
            <a href="p/nosaltres.html" class="ravis-btn btn-type-2">Nosotros</a>
        </div>
        <div class="r-sec col-md-5">
            <img src="img/welcome.jpg" alt="Colosseum Hotel">
        </div>
    </div>
</section>
<!--End of Welcome Section-->

<!--Luxury Room Section-->
<section id="luxury-rooms" class="clearfix">

    <?php foreach($this->db->get('habitaciones')->result() as $h): ?>
        <div class="room-boxes col-sm-6 col-md-3 col-md-20">
            <a href="<?= site_url('habitacion/'.$h->id.'-'.$h->habitacion_nombre) ?>" class="inner-container" data-bg="<?= base_url('img/habitaciones/'.$h->foto_main) ?>">
                <span class="ravis-title">
                    <span class="inner-box">
                        <span class="title"><?= $h->habitacion_nombre ?></span>
                        <span class="sub-title"><?= $h->slogan ?></span>
                    </span>
                </span>
            </a>
        </div>
    <?php endforeach ?>
</section>
<!--End of Luxury Room Section--><br>

<!-- Upcoming Events -->
<!-- Gallery -->
<section id="gallery">
    <div class="inner-container container">

        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Nuestro entorno</div>
                <div class="sub-title">Conoce una comarca llena de encanto</div>
            </div>
        </div>

        <!-- Gallery Container -->
        <div class="gallery-container">
            <div class="sort-section">
                <div class="sort-section-container">
                    <div class="sort-handle">Filters</div>
                    <ul class="list-inline">
                        <?php foreach($this->db->get('categorias_fotos')->result() as $l=>$c): ?>
                            <?php if($l==0): ?>
                            <li><a href="#" data-filter=".main">Tot</a></li>
                            <?php endif ?>
                            <li><a href="#" data-filter=".<?= $c->id ?>cat"><?= $c->nombre ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <ul class="image-main-box clearfix">
                <?php foreach($this->db->get('categorias_fotos')->result() as $l=>$c): ?> 
                    <?php $this->db->limit('6'); $this->db->order_by('priority','ASC'); ?> 
                    <?php foreach($this->db->get_where('fotos',array('categorias_fotos_id'=>$c->id))->result() as $f): ?>
                        <li class="item col-xs-6 col-md-4 <?= $c->id ?>cat <?= $f->mostrar_en_tot==1?'main':'' ?>">
                            <figure>
                                <img src="<?= base_url('img/entorno/'.$f->foto) ?>" alt="11"/>
                                <a href="<?= base_url('img/entorno/'.$f->foto) ?>" class="more-details" data-title="Great View"><?= $f->titulo ?></a>
                                <figcaption>
                                    <h4><?= $f->titulo ?></h4>
                                </figcaption>
                            </figure>
                        </li>
                    <?php endforeach ?>
                <?php endforeach ?>
            </ul>
            <a href="<?= base_url('p/galeria') ?>" class="gallery-more-btn ravis-btn btn-type-2">Ver Más</a>
        </div>
    </div>
</section>
<!-- End of Gallery -->
<!-- End of Upcoming Events -->

<!-- Hotel Sections -->
<section id="hotel-sections">
    <?php $this->db->limit(3); ?>
    <?php $this->db->order_by('fecha','ASC'); ?>
    <?php $this->db->where('fecha_desde <=',date("Y-m-d")); ?>
    <?php $this->db->where('fecha_fin >=',date("Y-m-d")); ?>
    <?php foreach($this->db->get_where('noticias')->result() as $n=>$b): ?>
        <div class="section-row clearfix">
            <?php if(($n%2)!=0): ?>
                <div class="img-container animated-box" data-animation="fadeInUp" data-bg-img="<?= base_url('img/blog/'.$b->foto) ?>"></div>
            <?php endif ?>
            <div class="desc animated-box" data-animation="fadeInUp" data-delay="400">
                <div class="ravis-title-t-1">
                    <div class="title"><span><?= $b->titulo ?></span></div>
                    <div class="sub-title"><?= $b->subtitulo ?></div>
                </div>
                <div class="content">
                    <p><?= strip_tags($b->texto) ?></p>
                </div>
            </div>
            <?php if(($n%2)==0): ?>
                <div class="img-container animated-box" data-animation="fadeInUp" data-bg-img="<?= base_url('img/blog/'.$b->foto) ?>"></div>
            <?php endif ?>
        </div>
    <?php endforeach ?>
</section>
<!-- End of Hotel Sections -->

<!-- Video Tour -->
<section id="video-tour" data-bg-img="img/video.jpg">
    <div class="inner-container container">
        <div class="row">
            <div class="l-sec col-md-6">
                <div class="ravis-title">
                    <div class="inner-box">
                        <div class="title">Vídeo Tour</div>
                        <div class="sub-title">Visítanos a través de este fantástico video Tour</div>
                    </div>
                </div>

            </div>
            <div class="r-sec col-md-6">
                Visítanos virtualmente a través de este vídeo tour, conoce todos los rincones de la casa, las salas, los servicios... Pero la mejor manera de disfrutarlo es viniendo a conocernos! Estaremos encantados de alojarte con nosotros y ofrecerte el mejor servicio en Cal Fuster de la Plaça.
            </div>
        </div>
        <div class="row btn-box">
            <a href="http://www.youtube.com/watch?v=OD7aL9cG4mw" class="play-btn video-url">
                <i class="fa fa-play"></i>
            </a>
        </div>
    </div>
</section>
<!-- End of Video Tour -->

<!-- Special Offers -->
<section id="special-offers">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Ofertas Especiales</div>
                <div class="sub-title">Escoge el pack que más te convenga</div>
            </div>
        </div>

        <div class="packages-container clearfix">
            
            <?php foreach($this->db->get('ofertas')->result() as $o): ?>
                <div class="package-box col-md-4">
                    <div class="main-inner-box" data-bg-img="<?= base_url() ?>img/entorno/<?= $o->fondo ?>">
                        <div class="title-box">
                            <div class="title"><?= $o->titulo ?></div>
                            <div class="sub-title"><?= $o->subtitulo ?></div>
                        </div>
                        <div class="price-box">
                            <div class="price"><?= str_replace('.00','',$o->precio) ?>€</div>
                            <div class="type"><?= $o->descripcion_paquete ?></div>
                        </div>
                        <div class="detail-box">
                            <ul>
                                <?php foreach(explode(',',$o->detalle) as $d): ?>
                                    <li>
                                        <div class="inner-box"><?= ucwords($d) ?></div>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <a href="<?= base_url('iniciar-oferta/'.$o->id) ?>" class="ravis-btn btn-type-2">Escoger</a>
                    </div>
                </div>
            <?php endforeach ?>
            
        </div>
    </div>
</section>
<!-- End of Special Offers -->
