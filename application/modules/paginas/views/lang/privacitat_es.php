<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb2.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Política de Privacidad</div>

            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inicio</a></li>
                <li class="current"><a href="#">Privacidad</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="simple">
    <div class="inner-container container">
        <div class="ravis-title-t-2">


        </div>
        <div class="content" style=" text-align: justify">

            <div class="h4"><span>POLÍTICA DE PRIVACIDAD</span></div>
            Maria Carme Rica Llinares informa a sus usuarios del sitio web sobre su política sobre el tratamiento y protección de los datos de carácter personal de los usuarios y clientes que puedan ser recaudados por la navegación o contratación de servicios a través de su sitio web.
            En este web, Maria Carme Rica Llinares garantiza el cumplimiento de la normativa vigente en materia de protección de datos personales, reflejado en la Ley Orgánica 15/1999 de 13 de deciembre, de Protección de Datos de Carácter Personal y al Real Decreto 1720/2007, de 21 diciembre, por el que se aprueba el Reglamento de Desarrollo de la LOPD.
            L'utilització d'aquest web implica l'acceptació d'aquesta política de privacitat.<br><br>



            <div class="h4"><span>RECOGIDA, FINALIDAD Y TRATAMIENTOS DE DATOS</span></div>
             Maria Carme Rica Llinares tiene el deber de informar a los usuarios de su sitio web sobre la recogida de datos de carácter personal que pueden llevarse a cabo, bien sea mediante el envío de correo electrónico o al rellenar los formularios incluidos en el sitio web. En este sentido, Maria Carme Rica Llinares será considerada como responsable de los datos recaudados mediante los medios anteriormente descritos.
A su vez Maria Carme Rica Llinares informa a los usuarios que la finalidad del tratamiento de los datos recaudados contempla: La atención de solicitudes realizadas por los usuarios, la inclusión en la agenda de contactos, la prestación de servicios y la gestión de la relación comercial.
Las operaciones, gestiones y procedimientos técnicos que se realicen de forma automatizada o no automatizada y que posibiliten la recogida, el almacenamiento, la modificación, la transferencia y otras acciones sobre datos de carácter personal, tienen la consideración de tratamiento de datos personales.
Todos los datos personales, que sean recogidos a través del sitio web Cal Fuster de la Plaça, y por lo tanto tenga la consideración de tratamiento de datos de carácter personal, serán incorporados en los ficheros declarados ante la Agencia Española de Protección de Datos por Maria Carme Rica Llinares.<br>
            <br>



            <div class="h4"><span>COMUNICACIÓN DE INFORMACIÓN A TERCEROS</span></div>
            Maria Carme Rica Llinares informa als usuaris que les seves dades personals no seran cedides a terceres organitzacions, amb l'excepció que aquesta cessió de dades estigui emparada en una obligació legal o quan la prestació d'un servei impliqui la necessitat d'una relació contractual amb un encarregat de tractament. En aquest últim cas, solament es durà a terme la cessió de dades al tercer quan Maria Carme Rica Llinares disposi del consentiment exprés de l'usuari.


            <br><br>
            <div class="h4"><span>DRETS DELS USUARIS</span></div>
            La Ley Orgánica 15/1999, de 13 de desembre, de Protecció de Dades de Caracter Personal concedeix als interessats la possibilitat d'exercir una sèrie de drets relacionats amb el tractament de les seves dades personals.
Mentre les dades de l'usuari són objecte de tractament per part de Maria Carme Rica Llinares. Els usuaris podran exercir els drets d'accés, rectificació, cancel·lació i oposició d'acord amb el previst en la normativa legal vigent en matèria de protecció de dades personals.
Per fer ús de l'exercici d'aquests drets, l'usuari haurà de dirigir-se mitjançant comunicació escrita, aportant documentació que acrediti la seva identitat (DNI o passaport), a la següent adreça: Maria Carme Rica Llinares, Plaça Manel Maria Girona, 3 08712 Sant Martí de Tous, Barcelona o l'adreça que sigui substituïda en el Registre General de Protecció de Dades. Aquesta comunicació haurà de reflectir la següent informació: Nom i cognoms de l'usuari, la petició de sol·licitud, el domicili i les dades acreditatives.
L'exercici de drets haurà de ser realitzat pel propi usuari. No obstant això, podran ser executats per una persona autoritzada com a representant legal de l'autoritzat. En tal cas, s'haurà d'aportar la documentació que acrediti aquesta representació de l'interessat.           <br>

            <br>

        </div>
    </div>
</section>






    </div>

<?php $this->load->view('includes/template/footer'); ?>

<!--End of Footer Section-->
