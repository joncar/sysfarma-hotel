<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb2.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Els nostres serveis</div>
                <div class="sub-title">Espais i serveis que trobareu al nostre allotjament</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li class="current"><a href="#">Serveis</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="simple">
    <div class="inner-container container">
        <div class="ravis-title-t-2">
            <div class="title"><span>Cal Fuster de la Plaça</span></div>
            <div class="sub-title">Trobaràs tot allò que necessites en un espai únic</div>
        </div>
        <div class="content">
            Al Cal Fuster de la Plaça trobaràs tot allò que necessites. Un allotjament rural on podràs gaudir del seu encant, la seva història, el seu entorn únic i espais que s'adecuaran a les vostres necessitats.
        </div>
    </div>
</section>
<!--End of Welcome Section-->

<!-- Hotel Sections -->
<section id="hotel-sections">
    <div class="section-row clearfix">
        <div class="desc animated-box" data-animation="fadeInUp" data-delay="400">
            <div class="ravis-title-t-1">
                <div class="title"><span>Col·lecció de Fusteria</span></div>
                <div class="sub-title">Espai dedicat a l'ofici de la fusteria</div>
            </div>
            <div class="content">
                Al rebedor de la casa podreu visitar el minuciós i nombrós museu dedicat a l'ofici de la fusteria. </br> Els antepassats familiar de Cal Fuster de la Plaça, com bé delata el nom de la casa, es dedicaran a la fusteria. En aquest petit museu trobareu les eines dels avantpassats així com tot el material que ha passat de generació en generació de Cal Fuster de Tous. <br> Si ho desitgeu podeu demanar una petita visita guiada que us deleitarà amb anècdotes i petites explicacions d'aquest gran ofici.
            </div>
        </div>
        <div class="img-container animated-box" data-animation="fadeInUp"
             data-bg-img="<?= base_url() ?>img/hotel-section/1.jpg"></div>
    </div>
    <div class="section-row clearfix">
        <div class="img-container animated-box" data-animation="fadeInUp"
             data-bg-img="<?= base_url() ?>img/hotel-section/2.jpg"></div>
        <div class="desc animated-box" data-animation="fadeInUp" data-delay="400">
            <div class="ravis-title-t-1">
                <div class="title"><span>Sala dels Nobles</span></div>
                <div class="sub-title">Espai reservat per a trobades, esbarjo, reunions...</div>
            </div>
            <div class="content">
                A la part baixa de la casa podreu gaudir d'un espai ampli per a petites reunions, veure la televisió, jugar a jocs de taula... </br> Es tracta d'una sala a peu de carrer, és una zona comú per a tots els nostres clients. </br> En cas de necessitar cadires, taula per a reunions... no dubteu a sol·licitar-ho, procurarem fer de la vostra estada la millor. 
            </div>
        </div>
    </div>
    <div class="section-row clearfix">
        <div class="desc animated-box" data-animation="fadeInUp" data-delay="400">
            <div class="ravis-title-t-1">
                <div class="title"><span>Zona d’aigües LA FOU</span></div>
                <div class="sub-title">Piscina amb vistes de luxe</div>
            </div>
            <div class="content">
                Si busqueu desconnectar una estona del dia a dia i gaudir d'un bany tranquil amb vistes immillorables només caldrà que pugeu a la part alta de la casa, la zona anomenada La Fou. </br> Zona de descans, solarium, piscina i molts serveis per al descans i tranquil·litat dels nostres clients. </br> En cas de necessitar tovalloles i altre material no dubteu a sol·licitar-ho.
            </div>
        </div>
        <div class="img-container animated-box" data-animation="fadeInUp"
             data-bg-img="<?= base_url() ?>img/hotel-section/3.jpg"></div>
    </div>
</section>
<!-- End of Hotel Sections -->

<!--Our Services Section-->
<section id="our-services">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Altres serveis</div>
                <div class="sub-title">Treballem per a fer la teva estada inoblidable</div>
            </div>
        </div>

        <div id="services-box" class="owl-carousel owl-theme">
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/1.jpg" alt="6">
                <div class="title">Celler El Pagna</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/2.jpg" alt="2">
                <div class="title">Sauna</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/3.jpg" alt="3">
                <div class="title">Cuina i servei d'esmorzar</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/4.jpg" alt="4">
                <div class="title">Sala La Fou</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/5.jpg" alt="5">
                <div class="title">Terrassa</div>
            </div>
            <div class="item">
                <img src="<?= base_url() ?>img/gallery/6.jpg" alt="1"><!-- Change the URL section based on your image's name -->
                <div class="title">Piscina La Fou</div>
            </div>
        </div>



    </div>
</section>
<!--End of Our Services Section-->

<!-- Testimonials Section -->
<!--<section id="testimonials-section" data-parallax="scroll" data-image-src="<?/*= base_url() */?>img/testimonails.jpg">
    <div class="inner-container container">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="guest">
                    <div class="ravis-title">
                        <div class="inner-box">
                            <div class="title">Christopher Robinson</div>
                            <div class="sub-title">Accountant in Apple Inc.</div>
                        </div>
                    </div>
                </div>
                <div class="text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
                </div>
            </div>
            <div class="item">
                <div class="guest">
                    <div class="ravis-title">
                        <div class="inner-box">
                            <div class="title">Jeniffer Chapman</div>
                            <div class="sub-title">Freelance Photographer</div>
                        </div>
                    </div>
                </div>
                <div class="text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
                </div>
            </div>
            <div class="item">
                <div class="guest">
                    <div class="ravis-title">
                        <div class="inner-box">
                            <div class="title">Jeremy Brown</div>
                            <div class="sub-title">Mechanical Engineer</div>
                        </div>
                    </div>
                </div>
                <div class="text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
                </div>
            </div>
            <div class="item">
                <div class="guest">
                    <div class="ravis-title">
                        <div class="inner-box">
                            <div class="title">Jack Norman</div>
                            <div class="sub-title">Web Designer in RavisTheme</div>
                        </div>
                    </div>
                </div>
                <div class="text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- End of Testimonials Section -->

<!--Client Sections-->
<section id="client-section">
    <div class="inner-container container">
        <ul class="client-list clearfix">
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/altria1.png" alt="Client Logo">
                </a>
            </li>
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp" data-delay="400">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/bluehost.png" alt="Client Logo">
                </a>
            </li>
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp" data-delay="800">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/cube.png" alt="Client Logo">
                </a>
            </li>
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp" data-delay="1200">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/erikaschesonis1.png" alt="Client Logo">
                </a>
            </li>
            <li class="col-xs-6 col-md-3 animated-box" data-animation="fadeInUp" data-delay="1200">
                <a href="#">
                    <img src="<?= base_url() ?>img/client-logo/modernart.png" alt="Client Logo">
                </a>
            </li>
        </ul>
    </div>
</section>
<!--End of Client Sections-->
<?php $this->load->view('includes/template/footer'); ?>

<!--End of Footer Section-->
