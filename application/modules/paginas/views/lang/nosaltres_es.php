<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Nosotros</div>
                <div class="sub-title">Una casa con mucha historia</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inicio</a></li>
                <li class="current"><a href="#">Nosotros</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="has-user">
    <div class="inner-container container">
        <div class="l-sec col-md-7">
            <div class="ravis-title-t-1">
                <div class="title"><span>Cal Fuster de la Plaça</span></div>
                <div class="sub-title">Alojamiento con encanto y mucha historia</div>
            </div>
            <div class="content">
                Situada en medio del casco antiguo de Sant Martí de Tous, villa milenaria llena de historias, leyendas y parajes encantadores, la casa recoge el encanto y el confort que el lugar proporciona.

Piedra, madera y materiales nobles, se combinan vestigios góticos con modernos detalles que la hacen cómodo y confortable.

En este web encontraréis un par de pinceladas de quién somos y que ofrecemos, el servicio del que disfrutaréis en Cal Fuster de la Plaça va mucho más allá del de una simple casa de turismo rural…
            </div>
        </div>
        <div class="r-sec col-md-5">
            <div class="user-img-box">
                <div class="inner-box">
                    <img src="<?= base_url() ?>img/hotel-manager.png" alt="Chris Coleman - General Manager">
                </div>
            </div>
        </div>
    </div>
</section>
<!--End of Welcome Section-->

<!-- Video Tour -->
<section id="video-tour" data-bg-img="<?= base_url() ?>img/video.jpg">
    <div class="inner-container container">
        <div class="row">
            <div class="l-sec col-md-6">
                <div class="ravis-title">
                    <div class="inner-box">
                        <div class="title">Vídeo Tour</div>
                        <div class="sub-title">Visítanos a través de este fantástico vídeo Tour</div>
                    </div>
                </div>

            </div>
            <div class="r-sec col-md-6">
                Visítanos virtualmente a través de este vídeo tour, conoce todos los rincones de la casa, las salas, los servicios... Pero la mejor manera de disfrutarlo es viniendo a conocernos! Estaremos encantados de alojarte con nosotros y ofrecerte estancia en Cal Fuster de la plaça.
            </div>
        </div>
        <div class="row btn-box">
            <a href="http://www.youtube.com/watch?v=23BFVDEdi-0" class="play-btn video-url">
                <i class="fa fa-play"></i>
            </a>
        </div>
    </div>
</section>
<!-- End of Video Tour -->

<!-- Our History -->
<section id="history-section">
    <div class="inner-container container">
        <div class="ravis-title-t-2">
            <div class="title"><span>Nuestra historia</span></div>
            <div class="sub-title">Señores, leyendas, historia milenaria</div>
        </div>

        <div class="desc">
            La casa contempla la historia de Sant Martí de Tous a través de sus paredes de piedra, y hace mención de las diferentes leyendas y parajes de Tous, dando nombre a habitaciones, rincones y diferentes aposentos del inmueble, así pues podréis hacer un recorrido legendario dentro de la misma casa, que os trasladará a un mundo lleno de historia, confort y buen servicio.
        </div>

        <!-- History Main Container -->
        <div class="history-timeline clearfix">
            <!-- History Box ( You can create History boxes as much as you want! But Do Not change the classes and attributes ) -->
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>880</i><!-- History's date (Do Not remove it) -->
                    <h5>La Conquista</h5><!-- History's title -->
                    <!-- History's description -->
                    <div class="history-content">
                        Guifre el Pelós conquistó el lugar de Blandos sobre el 880.
                    </div>
                    <!-- end of History's description -->
                </div>
            </div>
            <!-- End of History box -->
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInRight">
                <div class="history-boxes-inner">
                    <i>960</i>
                    <h5> Bisbado de Vic</h5>
                    <div class="history-content">
                        Borrell I, conde de Barcelona y de Osona, dio a la Iglesia de Vic el territorio del castillo de Tous, que se consideraba entonces parte del término del castillo de Montbui, en una buena parte baldío y despoblado.                    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>1020</i>
                    <h5>Visita del obispo y abad Oliba</h5>
                    <div class="history-content">
                         El país se empezó a repoblar verso el 1015, pero el obispo y abad Oliba, en agosto del 1020, fue a Tous a emprender definitivamente la nueva forestación y a asegurar la suya defiende encomendándolo a Guillem de Mediona. El 1043 volvió para consagrar la iglesia de la Roqueta.
                    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInRight">
                <div class="history-boxes-inner">
                    <i>1032</i>
                    <h5>Dominio de los Tous</h5>
                    <div class="history-content">
                        Muerto Guillem d’Oló, el 1032, el castillo fue confiado a unos catalanes apellidados Tous, que a menudo pretendieron el dominio absoluto.
                    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>1341</i>
                    <h5>Bernat de Tous recupera protagonismo</h5>
                    <div class="history-content">
                        Bernat de Tous fue embajador en Francia el 1341; hombre de confianza del rey Pere III el Ceremonioso, este le vendió la jurisdicción del castillo de Tous el 1347.              </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInRight">
                <div class="history-boxes-inner">
                    <i>1423</i>
                    <h5>El castillo a subasta</h5>
                    <div class="history-content">
                        El dicho sobre la malgastadora señora de Tous que recoge padre Cinto Verdaguer en una poesía fechada el 1897 (“Si yo hubiera sabido que era comida tan buena / lo pan de mestall mezclado con las nueces, / ay! no envidiaría del rey la corona / y sería todavía señora de Tous.”) parece que se refiere a Beatriu de Vilanova, viuda de Bernat de Tous, cuando el 1423 el castillo fue puesto a pública subasta a instancia de los acreedores y adquirido por Ponç de Perellós. Su hermana, Joana de Perellós, vendió (1441) el castillo al noble Joan Saplana, y pasó a sus sucesores. El 1505, Isabel Saplana decidió de hacer donación pactada del castillo al monasterio de Santo Jeroni del Arrayán.
Neteja Ressalta les paraules desconegudes                     </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>1505</i>
                    <h5>Castillo de Tous es el centro del régimen señorial</h5>
                    <div class="history-content">
                        Del 1505 al 1835, el castillo de Tous fue el centro del régimen señorial que ejercieron los religiosos de la comunidad del monasterio de Santo Jeroni del Arrayán, situado en las afueras de Badalona. Precisamente, por la distancia que los separaba, los frailes delegaron sus funciones judiciales con el nombramiento de procuradores y, entre otros nobles, lo hicieron repetidamente algunos miembros del condado de Santa Coloma de Queralt.              </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInRight">
                <div class="history-boxes-inner">
                    <i>1820</i>
                    <h5>Desamortizaciones</h5>
                    <div class="history-content">
                        En cumplimiento de las disposiciones desamortizadoras del 1820 y del 1821, el castillo de Tous con sus tierras, que comprendían 249 jornales de labrar y dos molinos de harina, fue puesto a la subasta pública y los frailes jerònims tuvieran que huir, bien que, a finales del 1833, la orden de Santo Jeroni recuperó el dominio.    </div>
                </div>
            </div>
            <div class="history-boxes col-md-6 col-xs-6 animated-box" data-animation="fadeInLeft">
                <div class="history-boxes-inner">
                    <i>1835</i>
                    <h5>Transmisiones y ventas del castillo</h5>
                    <div class="history-content">
                        El 1835 fue aplicada la desamortización y los frailes jerònims tuvieran que abandonar por siempre jamás el lugar de Tous. El acreedor, Ignasi Codina, el mismo 1835, hizo venta del castillo a Marçal Grau, que el 1864 lo vendió a Ramon de Miquel y Recasens y posteriormente se hicieron otras transmisiones. </div>
                </div>
            </div>
        </div>
        <!-- End of Services Main Container -->

    </div>
</section>
<!-- End of Our History -->
<!--Footer Section-->
<?php $this->load->view('includes/template/footer'); ?>
