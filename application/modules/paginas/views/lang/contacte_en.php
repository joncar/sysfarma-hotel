<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb6.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Contacte</div>
                <div class="sub-title">Estem al teu servei pel que necessitis</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li class="current"><a href="#">Contacte</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Contact Section-->
<section id="contact-section">
    <div class="inner-container container">
        <div class="t-sec">
            <div class="ravis-title-t-2">
                <div class="title"><span>Contacte</span></div>
                <!-- <div class="sub-title">Do not hesitate to contact me if you have any further questions</div> -->
            </div>
            <div class="content">
                Pots posar-te en contacte amb nosaltres a través del següent formulari
            </div>

            <div class="contact-info">
                <div class="contact-inf-box">
                    <div class="icon-box">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="text">
                        <a href="https://www.google.com/maps/dir/41.5598075,1.5240244/41.5598196,1.5240205/@41.5599276,1.5239612,171m/data=!3m1!1e3!4m2!4m1!3e0?hl=es-ES"> Pl. Manel Mª Girona, 3 Sant Martí de Tous (Barcelona) </a>
                    </div>
                </div>
                <div class="contact-inf-box">
                    <div class="icon-box">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="text">
                        <a href="mailto:fuster@calfusterdetous.com"> fuster@calfusterdetous.com</a>
                    </div>
                </div>
                <div class="contact-inf-box">
                    <div class="icon-box">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="text">
                        <a href="tel:938 096 428">938096428</a>  /
                        <a href="tel:651 807 304  ">651807304  </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="b-sec clearfix" id='contacto'>
            <div class="contact-form col-md-6">
                <?php
                    if(!empty($_SESSION['msj'])){
                        echo $_SESSION['msj'];
                        unset($_SESSION['msj']);
                    }
                ?>
                <form action="<?= base_url('paginas/frontend/contacto') ?>" method="post">
                    <div class="field-row">
                        <input type="text" name="name" id="contact-name" placeholder="Nom:" required>
                    </div>
                    <div class="field-row">
                        <input type="text" name="telefono" id="contact-phone" placeholder="Telèfon:">
                    </div>
                    <div class="field-row">
                        <input type="email" name="email" id="contact-email" placeholder="Email:" required>
                    </div>
                    <div class="field-row">
                        <textarea placeholder="El teu missatge" name="message" id="contact-message" required></textarea>
                    </div>
                    <div class="g-recaptcha" data-sitekey="6LcO_jwUAAAAAPCXx3Q5ngnMxgRTF91-l0rG1LV_" style=" margin-bottom: 15px;"></div>
                    <div class="message-box"></div>
                    <div class="field-row">
                        <input type="submit" value="Enviar">
                    </div>
                </form>
            </div>
            <div id="google-map" class="col-md-6" data-lat="41.5598196" data-lon="1.5240205" data-marker="<?= base_url() ?>img/marker.png"></div>
        </div> 
    </div>
</section>
<!--Contact Section-->
<section id="contact-section">
    <div class="inner-container container">
        <div class="t-sec">
            <div class="ravis-title-t-2">
                <div class="title"><span>Com arribar?</span></div>
                <!-- <div class="sub-title">Do not hesitate to contact me if you have any further questions</div> -->
            </div>
            <div class="content">
                Aquí et donem informació dels transports públics per poder arribar a Cal Fuster de la plaça
            </div>

            <div class="contact-info">
                <div class="contact-inf-box"style=" margin-right: 20px;">
                    <div class="icon-box"style=" vertical-align: top;">
                        <i class="fa fa-bus"></i>
                    </div>
                    <div class="text" >
                        BUS <br><br>
                        <span style=" color: #c9c6c6; font-size: 14px;line-height: 20px"> HISPANO IGUALADINA<br>
                        Línea Guimerà-Tous<br>
                        Línea Barcelona-Igualada </span><br>     <br>
                        <span style=" color: #c9c6c6; font-size: 14px; line-height: 20px"> Web:<a href="http:// www.igualadina.com"> www.igualadina.com</a>   <br> Telèfon:  <a href="tel:902292900">902 29 29 00</a>
                            <div class="ravis-title-t-2"style=" margin-bottom: 2px; margin-top: -23px">
                                <div class="title"><span></span></div>
                            </div>
                            <span style=" color: #c9c6c6; font-size: 14px;line-height: 20px"> ALSINA GRAELLS<br>
                        Línea Lleida-Igualada<br>
                       </span><br>
                        <span style=" color: #c9c6c6; font-size: 14px; line-height: 20px"> Web:<a href="http://www.reserbus.es/movelia/reservar-billetes-tickets-bus-online-alsina-graells.aspx"> www.reserbus.es</a>   <br> Telèfon:  <a href="tel:938210048">938 210 048</a><br>
                    </div>
                </div>
                <div class="contact-inf-box"style=" margin-right: 20px;">
                    <div class="icon-box" style=" vertical-align: top;">
                        <i class="fa fa-train"></i>
                    </div>
                    <div class="text">
                        TREN <br><br>
                        <span style=" color: #c9c6c6; font-size: 14px;line-height: 20px"> FERROCARRILS DE LA GENERALITAT<br>
                        Línia R6 Llobregat-Anoia<br>
                        </span><br>
                        <span style=" color: #c9c6c6; font-size: 14px; line-height: 20px"> Web:<a href="http://www.fgc.cat/cat/llobregat-anoia.asp?linia=R6"> www.fgc.cat</a>   <br> Telèfon:  <a href="tel:900901515"> 900 901 515</a><br>
                    </div>
                </div>
                <div class="contact-inf-box"style=" margin-right: 20px;">
                    <div class="icon-box"style=" vertical-align: top;">
                        <i class="fa fa-car"></i>
                    </div>
                    <div class="text">
                        TAXI <br><br>
                        <span style=" color: #c9c6c6; font-size: 14px;line-height: 20px"> TAXIS IGUALADA<br>

                        </span><br>
                        <span style=" color: #c9c6c6; font-size: 14px; line-height: 20px"> Web:<a href=" http://www.taxiservicio.es">  www.taxiservicio.es</a>   <br> Telèfon:  <a href="tel:938045353"> 938 045 353</a><br>
                    </div>
                </div>
                <div class="contact-inf-box"style=" margin-right: 20px;">
                    <div class="icon-box"style=" vertical-align: top;">
                        <i class="fa fa-plane"></i>
                    </div>
                    <div class="text">
                        AEROPORT <br><br>
                        <span style=" color: #c9c6c6; font-size: 14px;line-height: 20px"> METRO<br>
                        L9 Sud que connecta amb Barcelona. A la parada Europa Fira enllaça amb els FGC linia R6 que va a Igualada.
                        </span>
                        <div class="ravis-title-t-2"style=" margin-bottom: 2px; margin-top: -23px"">
                            <div class="title"><span></span></div>
                        </div>
                        <span style=" color: #c9c6c6; font-size: 14px;line-height: 20px"> AEROBUS<br>
                        Amb parada a Plaça Espanya (enllaç amb FGC línia R6 cap a Igualada)
                        </span>
                        <div class="ravis-title-t-2"style=" margin-bottom: 2px; margin-top: -23px">
                            <div class="title"><span></span></div>
                        </div>
                        <span style=" color: #c9c6c6; font-size: 14px;line-height: 20px"> BAIXBUS<br>
                        Línia L77 amb parada a Sant Boi de Llobregat Estació de Ferrocarrils que enllaça amb el ferrocarril R6 directe a Igualada.
                        </span><br>

                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
