<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb3.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Habitacions</div>
                <div class="sub-title">Descripció de les nostres estances</div>
            </div>
        </div>
        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li class="current"><a href="#">Habitacions</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Room Section-->
<section id="rooms-section" class="row-view">
    <div class="inner-container container">
        <div class="ravis-title-t-2">
            <div class="title"><span>Les nostres habitacions</span></div>
        </div>
        <div class="desc">
            La casa consta de 5 habitacions. Cada dormitori és diferent, té una història i unes particularitats.<br> Troba aquella que s'adequa més al que tu busques i gaudeix la teva estança.
        </div>

        <div class="room-container clearfix">
            <div class="room-box row animated-box" data-animation="fadeInUp">
                <div class="col-md-4 room-img" data-bg-img="<?= base_url() ?>img/luxury-rooms/a.jpg">
                    <a href="room-details.html" class="more-info-url"></a>
                </div>
                <div class="r-sec col-md-8">
                    <div class="col-md-6 m-sec">
                        <div class="title-box">
                            <div class="title">Senyora de Tous</div>
                            <div class="price">
                                <div class="title">Des de:</div>
                                <div class="value">105€</div>
                            </div>
                        </div>
                        <div class="amenities">
                            <ul class="list-inline clearfix">
                                <li class="col-md-6">
                                    <div class="title">Esmorzar:</div>
                                    <div class="value">Inclòs</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Tamany:</div>
                                    <div class="value">30 m2</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Capacitat:</div>
                                    <div class="value">3</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Vistes:</div>
                                    <div class="value">Població</div>
                                </li>
                                <li class="col-md-12">
                                    <div class="title">Serveis:</div>
                                    <div class="value">Wifi gratuït, Minibar, Lavabo interior</div>
                                </li>
                            </ul>
                        </div>
                        <a href="room-details.html" class="more-info">Més informació</a>
                    </div>
                    <div class="col-md-6 desc">
                        Hi havia al castell de Tous una senyora capriciosa i llaminera, molt difícil d'acontentar; qualsevol refinament li semblava poc. La bona taula era la seva perdició. Només volia exquisitats. En concret només volia os de bè negre. Tan difícil es feia de trobar-los, que la senyora del castell va acabar tots els seus recursos, va haver de vendre moltes possessions i empenyorar-se per tots costats. Aviat es va arruïnar...
                    </div>
                </div>
            </div>
            <div class="room-box row animated-box" data-animation="fadeInUp">
                <div class="col-md-4 room-img" data-bg-img="<?= base_url() ?>img/luxury-rooms/b.jpg">
                    <a href="room-details.html" class="more-info-url"></a>
                </div>
                <div class="r-sec col-md-8">
                    <div class="col-md-6 m-sec">
                        <div class="title-box">
                            <div class="title">Cérvola Blanca</div>
                            <div class="price">
                                <div class="title">Des de:</div>
                                <div class="value">111€</div>
                            </div>
                        </div>
                        <div class="amenities">
                            <ul class="list-inline clearfix">
                                <li class="col-md-6">
                                    <div class="title">Esmorzar:</div>
                                    <div class="value">Inclòs</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Tamany:</div>
                                    <div class="value">50 m2</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Capacitat:</div>
                                    <div class="value">2</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Vistes:</div>
                                    <div class="value">Població</div>
                                </li>
                                <li class="col-md-12">
                                    <div class="title">Serveis:</div>
                                    <div class="value">Wifi gratuït, Minibar, Lavabo interior</div>
                                </li>
                            </ul>
                        </div>
                        <a href="room-details.html" class="more-info">Més informació</a>
                    </div>
                    <div class="col-md-6 desc">
                        Al castell de Tous vivien els senyors feudals i no tenien fills. Un dia, en una cacera,
trobant-se a la Fou, veieren entre l’herbatge la silueta d’una cérvola blanca però s’escapava. Un cop el senyor la va poder acorralar i quan
creia tenir-la va trobar una dolça criatura i sel’a fillà. Era ‘atracció dels joves nobles de la rodalia però ella els defugia
dient que només es casaria amb el galant que aconseguís caçar la Cérvola Blanca...
                    </div>
                </div>
            </div>
            <div class="room-box row animated-box" data-animation="fadeInUp">
                <div class="col-md-4 room-img" data-bg-img="<?= base_url() ?>img/luxury-rooms/c.jpg">
                    <a href="room-details.html" class="more-info-url"></a>
                </div>
                <div class="r-sec col-md-8">
                    <div class="col-md-6 m-sec">
                        <div class="title-box">
                            <div class="title">Sol de Tous</div>
                            <div class="price">
                                <div class="title">Des de:</div>
                                <div class="value">87€</div>
                            </div>
                        </div>
                        <div class="amenities">
                            <ul class="list-inline clearfix">
                                <li class="col-md-6">
                                    <div class="title">Esmorzar:</div>
                                    <div class="value">Inclòs</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Tamany:</div>
                                    <div class="value">30 m2</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Capacitat:</div>
                                    <div class="value">2</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Vistes:</div>
                                    <div class="value">Plaça</div>
                                </li>
                                <li class="col-md-12">
                                    <div class="title">Serveis:</div>
                                    <div class="value">Wifi gratuït, Lavabo compartit</div>
                                </li>
                            </ul>
                        </div>
                        <a href="room-details.html" class="more-info">Més informació</a>
                    </div>
                    <div class="col-md-6 desc">
                        «El Sol de Tous» es va publicar per primera vegada el 8 de novembre de 1914. Darrera hi havia en Camil Riba i Compte, un jove tousenc amant de la cultura i la lectura que va decidir impulsar un periòdic per apropar la cultura als seus veïns. Així, sota la capçalera del diari s’hi podia llegir «Periódico mensual, defensor de los intereses del pueblo y su comarca». I a les seves pàgines hi havia notícies i curiositats no només de Tous sinó també dels pobles veïns.
                    </div>
                </div>
            </div>
            <div class="room-box row animated-box" data-animation="fadeInUp">
                <div class="col-md-4 room-img" data-bg-img="<?= base_url() ?>img/luxury-rooms/d.jpg">
                    <a href="room-details.html" class="more-info-url"></a>
                </div>
                <div class="r-sec col-md-8">
                    <div class="col-md-6 m-sec">
                        <div class="title-box">
                            <div class="title">Torrent Cavaller</div>
                            <div class="price">
                                <div class="title">Des de:</div>
                                <div class="value">90€</div>
                            </div>
                        </div>
                        <div class="amenities">
                            <ul class="list-inline clearfix">
                                <li class="col-md-6">
                                    <div class="title">Esmorzar:</div>
                                    <div class="value">Inclòs</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Tamany:</div>
                                    <div class="value">50 m2</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Capacitat:</div>
                                    <div class="value">3</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Vistes:</div>
                                    <div class="value">Plaça</div>
                                </li>
                                <li class="col-md-12">
                                    <div class="title">Serveis:</div>
                                    <div class="value">Wifi gratuït, Lavabo compartit</div>
                                </li>
                            </ul>
                        </div>
                        <a href="room-details.html" class="more-info">Més informació</a>
                    </div>
                    <div class="col-md-6 desc">
                        És d’indret on una noia es veia amb un   cavaller l’amor que tants cors omple...  
E.V.
*Dos nois  enamorats, i els pares de la noia no el volien. La família de la noia marxà del poble. El noi anava cada dia al torrent per veure si tornava. Ell morir de tant espera i de tristesa.
                    </div>
                </div>
            </div>
            <div class="room-box row animated-box" data-animation="fadeInUp">
                <div class="col-md-4 room-img" data-bg-img="<?= base_url() ?>img/luxury-rooms/e.jpg">
                    <a href="room-details.html" class="more-info-url"></a>
                </div>
                <div class="r-sec col-md-8">
                    <div class="col-md-6 m-sec">
                        <div class="title-box">
                            <div class="title">Sentfores</div>
                            <div class="price">
                                <div class="title">Des de:</div>
                                <div class="value">95€</div>
                            </div>
                        </div>
                        <div class="amenities">
                            <ul class="list-inline clearfix">
                                <li class="col-md-6">
                                    <div class="title">Esmorzar:</div>
                                    <div class="value">Inclòs</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Tamany:</div>
                                    <div class="value">50 m2</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Capacitat:</div>
                                    <div class="value">2</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Vistes:</div>
                                    <div class="value">Plaça</div>
                                </li>
                                <li class="col-md-12">
                                    <div class="title">Serveis:</div>
                                    <div class="value">Wifi gratuït, Lavabo interior</div>
                                </li>
                            </ul>
                        </div>
                        <a href="room-details.html" class="more-info">Més informació</a>
                    </div>
                    <div class="col-md-6 desc">
                        Mare de Déu de Sentfores és un edifici religiós protegida com a bé cultural d'interès local.

Tenint vora set segles d'història, ha estat un punt de de devoció pels habitants del poble de Tous i dels municipis propers. La llegenda diu que la construcció de l'ermita va ser deguda a la tossudesa de la Mare de Déu de Sentfores, trobada per un pastor, a romandre en aquella muntanya.
                    </div>
                </div>
            </div>
            <!--<div class="room-box row animated-box" data-animation="fadeInUp">
                <div class="col-md-4 room-img" data-bg-img="<?/*= base_url() */?>img/gallery/6.jpg">
                    <a href="room-details.html" class="more-info-url"></a>
                </div>
                <div class="r-sec col-md-8">
                    <div class="col-md-6 m-sec">
                        <div class="title-box">
                            <div class="title">King Suit</div>
                            <div class="price">
                                <div class="title">Starting from :</div>
                                <div class="value">$660</div>
                            </div>
                        </div>
                        <div class="amenities">
                            <ul class="list-inline clearfix">
                                <li class="col-md-6">
                                    <div class="title">Breakfast :</div>
                                    <div class="value">Included</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Room Size :</div>
                                    <div class="value">60 sqm</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">Max People :</div>
                                    <div class="value">3</div>
                                </li>
                                <li class="col-md-6">
                                    <div class="title">View :</div>
                                    <div class="value">Sea</div>
                                </li>
                                <li class="col-md-12">
                                    <div class="title">Facilities :</div>
                                    <div class="value">Free Wifi, Free Mini Bar, Room Security</div>
                                </li>
                            </ul>
                        </div>
                        <a href="room-details.html" class="more-info">More Info</a>
                    </div>
                    <div class="col-md-6 desc">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. At cum eius eos fugiat illo
                        illum
                        provident, quas qui quisquam quod sint velit voluptate voluptatum. Ad aperiam aspernatur
                        atque aut consequatur distinctio earum ex fuga fugit hic impedit iure libero minima
                        molestiae nihil obcaecati officia perferendis perspiciatis possimus, quam qui quibusdam
                        quidem reiciendis rem repellendus, sint tempora veniam vero voluptas voluptatum.
                    </div>
                </div>
            </div>-->
        </div>
        <!-- Pagination -->
   <!--     <div class="pagination-box">
            <ul class="list-inline">
                <li class="active"><a href="#"><span>1</span></a></li>
                <li><a href="#"><span>2</span></a></li>
                <li><a href="#"><span>3</span></a></li>
            </ul>
        </div>-->
        <!-- End of Pagination -->

    </div>
</section>
<!--Footer Section-->
<footer id="main-footer">
    <div class="inner-container container">
        <div class="t-sec clearfix">
            <div class="widget-box col-sm-6 col-md-3">
                <a href="#" id="f-logo">
                    <!--                    <span class="title">Colosseum</span>-->
                    <!--                    <span class="desc">Luxury Hotel</span>-->
                    <div class="">
                        <img src="img/logo.png" style=" margin-top: -24px; width: 180px"alt="cal fuster de la plaça">
                    </div>
                </a>

                <div class="widget-content text-widget">
                    Situada en mig del casc antic de Sant Martí de Tous, vila mil·lenària plena d’històries, llegendes i paratges encantadors, la casa recull l’encant i el confort que el lloc proporciona. Pedra, fusta i materials nobles, s’hi combinen vestigis gòtics amb moderns detalls que la fan còmode i confortable.
                </div>
            </div>
            <div class="widget-box col-sm-6 col-md-3">
                <h4 class="title">Newsletter</h4>
                <div class="widget-content newsletter">
                    <div class="desc">
                        Rep al teu correu electrònic totes les novetats i ofertes de Cal Fuster de la Plaça
                    </div>
                    <form class="news-letter-form">
                        <input type="text" class="email" placeholder="Email">
                        <button type="submit" class="ravis-btn btn-type-2">Registra't</button>
                    </form>
                </div>
            </div>
            <div class="widget-box col-sm-6 col-md-3">
                <h4 class="title">Últimes notícies</h4>
                <div class="widget-content latest-posts">
                    <ul>
                        <li class="clearfix">
                            <div class="img-container col-xs-4">
                                <a href="pages/blog-details.html">
                                    <img src="assets/img/gallery/5.jpg" alt="Room Image">
                                </a>
                            </div>
                            <div class="desc-box col-xs-8">
                                <a href="pages/blog-details.html" class="title">Toronto High End Market</a>
                                <div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Atque
                                    dicta exercitationem hic laudantium nemo quis quo reprehenderit repudiandae
                                    ut
                                    vel?
                                </div>
                                <a href="pages/blog-details.html" class="read-more">Read More</a>

                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="img-container col-xs-4">
                                <a href="pages/blog-details.html">
                                    <img src="assets/img/gallery/6.jpg" alt="Room Image">
                                </a>
                            </div>
                            <div class="desc-box col-xs-8">
                                <a href="pages/blog-details.html" class="title">Drop Everything Now</a>
                                <div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusamus consequuntur corporis dolorem esse fugiat in magnam nostrum qui
                                    rerum,
                                    sapiente.
                                </div>
                                <a href="pages/blog-details.html" class="read-more">Read More</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="widget-box col-sm-6 col-md-3">
                <h4 class="title">Contacta'ns</h4>
                <div class="widget-content contact">
                    <ul class="contact-info">
                        <li>
                            <i class="fa fa-home"></i>
                            Plaça Manel Mª Girona, 3 Sant Martí de Tous (Barcelona)
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            938096428 - 651807304
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            fuster@calfusterdetous.com
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="b-sec clearfix">
            <div class="copy-right">
                Amb <i class="fa fa-heart"></i> per <a href="http://hipo.tv" target="_blank">Hipo</a> © 2017. Tots els drets reservats.
            </div>


            <ul class="social-icons list-inline">
                <li> <img src="<?= base_url() ?>img/client-logo/altria1.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li> <img src="<?= base_url() ?>img/client-logo/bluehost.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li> <img src="<?= base_url() ?>img/client-logo/cube.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li> <img src="<?= base_url() ?>img/client-logo/erikaschesonis1.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li> <img src="<?= base_url() ?>img/client-logo/modernart.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>

        </div>
    </div>
</footer>
<!--End of Footer Section-->

<!--End of Room Section-->
