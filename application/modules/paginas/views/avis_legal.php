<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb2.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Avís Legal</div>

            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li class="current"><a href="#">Avís Legal</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="simple">
    <div class="inner-container container">
        <div class="ravis-title-t-2">


        </div>
        <div class="content" style=" text-align: justify">

            <div class="h4"><span></span></div>
            Aquesta pàgina és propietat de Maria Carme Rica Llinares, amb domicili a Plaça Manel Maria Girona 3. 08712 Sant Martí de Tous i NIF 77106359D.
            <br><br>Tots els continguts, textos, imatges i codis font són de propietat nostra o de la propietat de tercers que han adquirit els seus drets d’explotació i estan protegits pels drets de Propietat Intel.lectual i Industrial.
            En virtut del establert en la Llei Orgànica 15/1999 de Protecció de Dades de Caràcter Personal, l’informem que les dades personals que ens proporciona a través de la present pàgina seran tractats amb confidencialitat absoluta
            i s’incorporaran en els nostres fitxers amb la finalitat de prestar-li els serveis sol.licitats, respondre a les seves peticions d’informació i així com enviar-li informació referent als nostres productes i serveis, inclús per correu electrònic.
            <br><br>En qualsevol cas, podrà exercir els drets d’accés, de rectificació, cancel.lació i oposició previstos en la llei dirigint-se a Maria Carme Rica Llinares - Responsable Protecció Dades - Plaça Manel Maria Girona 3, 08712 Sant Martí de Tous, adjuntant una còpia del seu DNI.<br><br>



        </div>
    </div>
</section>






    </div>

<?php $this->load->view('includes/template/footer'); ?>

<!--End of Footer Section-->
