<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb2.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Condicions de Reserva</div>

            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li class="current"><a href="#">Condicions de Reserva</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="simple">
    <div class="inner-container container">
        <div class="ravis-title-t-2">


        </div>
        <div class="content" style=" text-align: justify">

            <div class="h4"><span>PEL QUE FA A LA SEVA RESERVA</span></div>
            Per a formalitzar la seva reserva cal fer un paga i senyal del 50% dins de les 48 hores següents. Una setmana abans de la seva estada al nostre allotjament caldrà efectuar el pagament del 50% restant.
            <br><br>



            <div class="h4"><span>ANUL.LACIÓ DE LA RESERVA</span> <br><br>
            <span class="read-more" style=" text-decoration: underline">En cas de reserva de només un allotjament:</span></div>
            La cancel·lació efectuada per la persona usuària dins dels 28 dies
            anteriors a la data d’arribada dóna lloc a les següents penalitzacions:<br>
            a) Reserva per 2 o menys dies, el 30% del preu total de l’estada.<br>
            b) Reserva per més de 2 dies i fins a 7 dies, el 35% del preu total de l’estada.<br>
            c) Reserva per més de 7 dies, el 25% del preu total de l’estada.<br>
            La persona usuària té dret a cancel·lar la reserva confirmada, sense cap penalització, sempre que es faci abans dels 28 dies anteriors a la data d’arribada.            <br><br>



            <div class="h4" style=" text-decoration: underline"><span>En cas de reserva de més d’un allotjament</span></div>
            a) En cap cas es retornarà la paga i senyal.<br>
            b) La resta pendent d’abonar s’ha de liquidar 28 dies abans de l’arribada<br>
            El pagament de la reserva confirma l’acceptació de les condicions de lloguer.<br>

            <br><br>
            <div class="h4"><span>MOLT IMPORTANT</span></div>
            <a class="read-more" style=" text-decoration: underline; font-weight: 600">Registre de viatgers: </a><br>Segons l’Ordre IRP/352/2009 de 3 de Juliol de la Generalitat de Catalunya és d’obligat compliment registrar i fer signar les corresponents fitxes o comunicats d’entrada de totes les persones allotjades majors de 16 anys i fer la comunicació de les dades als mossos d’esquadra dins el termini de les 24 hores següents a l’inici de l’allotjament de cada persona viatgera. Per tant, és molt important que totes les persones majors de 16 anys vinguin amb el seu corresponent D.N.I., ja que, en cas contrari, s’els hauria de denegar l’entrada. Els menors de 16 anys hauran de constar a la fitxa d’un dels adults al seu càrrec.

            <br><br>
            <a class="read-more" style=" text-decoration: underline; font-weight: 600">Menors d’edat:</a> <br>Els menors d’edat que no vagin acompanyats pels seus progenitors o tutors legals han de venir amb una autorització explícita autoritzant-los a estar a Can Gat Vell. Aquesta autorització ha d’estar degudament signada pels representants dels menors.

            <br><br>
            <a class="read-more" style=" text-decoration: underline; font-weight: 600">A la seva arribada:</a><br>
            1- En primer lloc, li ensenyarem l’allotjament que té reservat per comprovar que tot estigui correcte i al seu gust.<br>
            2- Ens haurà de signar l’acceptació de les condicions de lloguer i normes de l’establiment i entregar-nos els document d’identitat de tots els majors de 16 anys perquè puguem complimentar les fitxes del registre de viatgers, esmentat en l’apartat anterior.<br>
            3- Abonar la totalitat de la quantitat pendent (en reserves en las que intervenen més d’una família, haurà d’abonar la quantitat pendent la primera família que arribi). En el cas de reserva de més d’un allotjament, el pagament ja s’haurà efectuat 28 dies abans.<br>
            4- Taxa turística a Catalunya: segons el decret 159/2012 d’establiments d’allotjaments d’ús turístics i d’habitatges d’ús turístics, s’abonarà a CAN GAT VELL l’import de 0,50 € o 1€ per persona i dia, amb un màxim de 7 dies d’estància. Els menors de 16 anys queden exempts de pagar
            5- Els entregarem les claus.
            <br><br>

            <a class="read-more" style=" text-decoration: underline; font-weight: 600">Horaris:</a><br>
            la normativa del Departament de Comerç, Consum i Turisme diu que, el dret a l’ocupació de l’allotjament comença a les 18 h. del dia d’arribada i s’acaba a les 12 h. del dia de sortida. En dies anomenats “de canvi”, quan el mateix dia surten uns clients i n’entren uns altres, la qual cosa sol passar sobretot a l’estiu, ens veiem obligats a seguir aquest horari sense concessions. Fora dels dies de canvi podem ser més flexibles i es demana un suplement proporcional al preu per a la sortida fins a les 17h.  Ens adaptarem als seus desitjos sempre que ens sigui possible, però sempre tenint en compte el nostre propi horari de recepció (cal que, amb anterioritat a la seva arribada, ens facin saber l’hora aproximada, per tal que ens puguem assegurar que hi haurà algú per a rebre’ls).
            <br><br>
                <a class="read-more" style=" text-decoration: underline; font-weight: 600"> Aforament:</a><br> En cap cas s’admetran més persones (ni adults, ni nens) que les places que consten en la descripció de l’allotjament reservat. En cas que ocupeu tota la finca i que desitgeu rebre convidats, es cobrarà un suplement de 5 euros/dia per convidat per l’ús de les instal.lacions.
            <br><br>
                    <a class="read-more" style=" text-decoration: underline; font-weight: 600"> GOS/GAT i altres mascotes:</a><br> Acceptem que els nostres hostes portin els seus animals de companyia (només en dues cases, confirmeu amb nosaltres), excepte els gossos de races molt gran o perilloses. En el moment de la reserva, haureu d’especificar tot el que es refereix a la mascota (tipus de mascota, raça, pes etc.). No estan permesos ni a la piscina ni a les zones comunes interiors i exteriors, tot i ser lligats.
            <br><br>
                        <a class="read-more" style=" text-decoration: underline; font-weight: 600">  Fiança:</a> <br>Tenint en compte les experiències viscudes, ens veiem obligats a retenir una fiança de 100, 150 o bé 200€ (depenent de la casa), que es retornarà sempre i quan es deixi tot tal com us ho heu trobat a l’arribada. Preguem que deixeu sempre i en tot moment les zones a compartir en bon estat de neteja i ordre tant a la sala de jocs com a les zones comunitàries (barbacoa, piscina etc.), escombraries als contenidors exteriors corresponents etc.  S’esbrinarà que tot estigui en l’estat en què s’ho han trobat per tal de tornar la fiança per transferència bancària el dia feiner consecutiu al dia de sortida.

            <br><br>
            <div class="h4"><span>RESPECTE I CONVIVÈNCIA</span></div>
            Cal Fuster de Tous és una casa rural on hem cuidat tots els detalls i ens preocupem que tot estigui en molt bon estat. Demanem que totes les persones que formin part del grup estiguin informades de les normes que detallem a la pestanya corresponent. Demanem respecte per a les coses i que cuideu la nostra casa com si fos la vostra.
            <br><br>En cas que la vostra reserva sigui de tota la finca, tingueu en compte que nosaltres vivim aquí, en la part extrema del jardí. Per tant entenem que tots els espais comuns són a compartir amb nosaltres. Us demanem que a la zona comunitària hi hagi un bon estat de neteja i ordre. A més per tal de mantenir tots els espais en perfectes condicions, netegem cada dia piscina, zones com a terrasses, jardins, recepció etc. Agraïm la vostra comprensió.
        </div>
    </div>
</section>






    </div>

<?php $this->load->view('includes/template/footer'); ?>

<!--End of Footer Section-->
