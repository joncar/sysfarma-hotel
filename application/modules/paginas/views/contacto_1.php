<!-- MAIN CONTENT-->
<div class="main-content">
    <section class="page-banner contact-page">
        <div class="patterndestino"></div>
        <div class="container">
            <div class="page-title-wrapper">
                <div class="page-title-content">
                    <ol class="breadcrumb">
                        <li><a href="index.html" class="link home">Home</a></li>
                        <li class="active"><a href="#" class="link">contacte</a></li>
                    </ol>
                    <div class="clearfix"></div>
                    <h2 class="captions">Aquí estem</h2></div>
            </div>
        </div>
    </section>
    <section class="padding-top padding-bottom contact-organization">
        <div class="container">
            <h3 class="title-style-2">El nostre equip</h3>
            <div class="row">
                <div class="wrapper-organization">
                    <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                        <div class="content-organization">
                            <div class="wrapper-img"><img src="<?= base_url() ?>img/homepage/avatar-contact-1.jpg" alt="" class="img img-responsive"></div>
                            <div class="main-organization">
                                <div class="organization-title"> <a href="#" class="title">Anna Riba</a>
                                    <p class="text"> Controller administrativa</p>
                                </div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
<!--                                            <li class="main-list"><i class="icons fa fa-map-marker"></i><a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a></li>-->
                                            <li class="main-list"><i class="icons fa fa-phone"></i><a href="tel:693806249" class="link">693 806 249</a></li>
                                            <li class="main-list"><i class="icons fa fa-envelope-o"></i><a href="mailto:a.riba@byebyegroup.com" class="link">a.riba@byebyegroup.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                        <div class="content-organization">
                            <div class="wrapper-img"><img src="<?= base_url() ?>img/homepage/avatar-contact-2.jpg" alt="" class="img img-responsive"></div>
                            <div class="main-organization">
                                <div class="organization-title"> <a href="#" class="title">Anabel Mateos</a>
                                    <p class="text">Controller administrativa de expansión Espanya</p>
                                </div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
<!--                                            <li class="main-list"><i class="icons fa fa-map-marker"></i><a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a></li>-->
                                            <li class="main-list"><i class="icons fa fa-phone"></i><a href="tel:693806251" class="link">693 806 251</a></li>
                                            <li class="main-list"><i class="icons fa fa-envelope-o"></i><a href="mailto:a.mateos@byebyegroup.com " class="link">a.mateos@byebyegroup.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                        <div class="content-organization">
                            <div class="wrapper-img"><img src="<?= base_url() ?>img/homepage/avatar-contact-3.jpg" alt="" class="img img-responsive"></div>
                            <div class="main-organization">
                                <div class="organization-title"> <a href="#" class="title">Monica Martinez</a>
                                    <p class="text">Directora comercial de ventas</p>
                                </div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <!--                                            <li class="main-list"><i class="icons fa fa-map-marker"></i><a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a></li>-->
                                            <li class="main-list"><i class="icons fa fa-phone"></i><a href="tel:640179021" class="link"> 640 179 021</a></li>
                                            <li class="main-list"><i class="icons fa fa-envelope-o"></i><a href="mailto:m.martinez@byebyegroup.com " class="link">m.martinez@byebyegroup.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><br><br><br>
            <div class="row">
                <div class="wrapper-organization" style="text-align: center;">
                    <div class="col-md-4 col-sm-4 col-xs-4 md-organization col-sm-offset-2">
                        <div class="content-organization">
                            <div class="wrapper-img"><img src="<?= base_url() ?>img/homepage/avatar-contact-4.jpg" alt="" class="img img-responsive"></div>
                            <div class="main-organization">
                                <div class="organization-title"> <a href="#" class="title">Montse Raja</a>
                                    <p class="text">Comercial de ventas</p>
                                </div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <!--                                            <li class="main-list"><i class="icons fa fa-map-marker"></i><a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a></li>-->
                                            <li class="main-list"><i class="icons fa fa-phone"></i><a href="tel:693 800 214" class="link">693800214</a></li>
                                            <li class="main-list"><i class="icons fa fa-envelope-o"></i><a href="mailto:comercial@byebyegroup.com" class="link">comercial@byebyegroup.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                        <div class="content-organization">
                            <div class="wrapper-img"><img src="<?= base_url() ?>img/homepage/avatar-contact-5.jpg" alt="" class="img img-responsive"></div>
                            <div class="main-organization">
                                <div class="organization-title"> <a href="#" class="title">EMMA SOLER</a>
                                    <p class="text">Controller administrativa</p>
                                </div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <!--                                            <li class="main-list"><i class="icons fa fa-map-marker"></i><a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a></li>-->
                                            <li class="main-list"><i class="icons fa fa-phone"></i><a href="tel:693806292" class="link">693 806 292</a></li>
                                            <li class="main-list"><i class="icons fa fa-envelope-o"></i><a href="mailto:administracio@byebyegroup.com" class="link">administracio@byebyegroup.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="contact style-1 page-contact-form padding-top padding-bottom" id='contactenosform'>
        <div class="container">
            <div class="wrapper-contact-form">
                <div data-wow-delay="0.5s" class="contact-wrapper wow fadeInLeft">
                    <div class="contact-box">
                        <?= @$_SESSION['msj'] ?>
                        <?php $_SESSION['msj'] = ''; ?>
                        <h5 class="title" style="color: #222;">FORMULARI DE CONTACTE</h5>
                        <p class="text" style="color: #222;">Si vols contacta amb nosaltres no dubtis en fer-ho.</p>
                        <form class="contact-form" method="post" action="<?= base_url('paginas/frontend/contacto') ?>">
                            <input name="name" type="text" placeholder="El teu nom" class="form-control form-input">
                            <input name='email' type="email" placeholder="El teu Email" class="form-control form-input">
                            <textarea name="message" placeholder="Missatge" class="form-control form-input"></textarea>
                            <div class="contact-submit">
                                <button type="submit" data-hover="SEND NOW" class="btn btn-slide">
                                    <span class="text">ENVIAR ARA</span>
                                    <span class="icons fa fa-long-arrow-right"></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div data-wow-delay="0.5s" class="wrapper-form-images wow fadeInRight"><img src="<?= base_url() ?>img/background/bg-banner-contact-form.jpg" alt="" class="img-responsive"></div>
            </div>
        </div>
    </section>
    <section class="page-contact-map">
        <div class="map-block">
            <div class="wrapper-info">
                <div class="map-info">
                    <h3 class="title-style-2">ON ESTEM</h3>
                    <p class="address"><i class="fa fa-map-marker"></i> Carrer Girona, 34. <br>08700 IGUALADA. Barcelona</p>
                    <p class="phone"><i class="fa fa-phone"></i> +34-938 030 694</p>
                    <p class="mail">
                        <a href="mailto:info@byebyegroup.com"> <i class="fa fa-envelope-o"></i>info@byebyegroup.com</a>
                    </p>
                    <div class="footer-block"><a class="btn btn-open-map" <a href="https://www.google.es/maps/place/GRUPO+FINALIA/@41.5855626,1.6241467,16.94z/data=!4m5!3m4!1s0x0:0x122bed82c9971130!8m2!3d41.5859947!4d1.6237342">Veure Mapa</a></div>

                </div>
            </div>
            <div id="googleMap"></div>
        </div>
    </section>
</div>