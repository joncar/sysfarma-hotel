<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb2.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Ús de les cookies</div>

            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li class="current"><a href="#">Cookies</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="simple">
    <div class="inner-container container">
        <div class="ravis-title-t-2">


        </div>
        <div class="content" style=" text-align: justify">
En cumpliment amb lo disposat a l'article 22.2 de la Ley 34/2002, de 11 de juliol, de Serveis de la Societat de la Informació
i de Comerço Electrònic, Cal Fuster de la Plaça informa, en aquesta secció, sobre la <a class="read-more" style=" text-decoration: underline; font-weight: 600"> política de recollida i tractament de cookies.</a><br><br>

Al entrar a aquesta web has sigut informat sobre la utilizació de les cookies a través d'un avís o alerta. Si has accedit i segueixes navegant, estàs acceptant la instal·lació d'aquestes cookies.<br><br>


            <div class="h4"><span>¿QUÈ SÓN LES COOKIES?</span></div>
Una cookie és un fitxer que es descarrega en el teu ordinador o equip en accedir a determinades pàgines web. Les cookies permeten a una pàgina web, entre
altres coses, emmagatzemar i recuperar informació sobre els hàbits de navegació de l'Usuari o del seu equip i, depenent de la informació que continguin
i de la forma en què utilitzi el teu equip, poden utilitzar-se per reconèixer a l'Usuari.<br><br>



            <div class="h4"><span>¿QUIN TIPUS DE COOKIES UTILITZA AQUESTA PÀGINA WEB?</span></div>
            Cal Fuster de la Plaça utilitza els següents tipus de cookies:<br>
            <br>
            <a class="read-more" style=" text-decoration: underline; font-weight: 600">Cookies d'anàlisis:</a> Són aquelles que ben tractades per nosaltres o per tercers, ens permeten quantificar el nombre d'usuaris i així realitzar el mesurament i anàlisi estadística de la utilització que fan els usuaris del servei ofert. Per a això s'analitza la seva navegació a la nostra pàgina web amb la finalitat de millorar l'oferta de productes o serveis que t'oferim.
            <br><br>
            <a class="read-more" style=" text-decoration: underline; font-weight: 600">Cookies tècniques:</a>  Són aquelles que permeten a l'usuari la navegació a través de l'àrea restringida i la utilització de les seves diferents funcions, com per exemple, portar a canvi el procés de compra d'un article.
            <br><br>
            <a class="read-more" style=" text-decoration: underline; font-weight: 600">Cookies de personalització:</a> Són aquelles que permeten a l'Usuari accedir al servei amb algunes característiques de caràcter general predefinides en funció d'una sèrie de criteris en el terminal de l'usuari com per exemple serien l'idioma o el tipus de navegador a través del com es connecta al servei.
            <br><br>
            <a class="read-more" style=" text-decoration: underline; font-weight: 600">Cookies publicitàries.</a>  Són aquelles que, ben tractades per aquesta web o per tercers, permeten gestionar de la forma més eficaç possible l'oferta dels espais publicitaris que hi ha a la pàgina web, adequant el contingut de l'anunci al contingut del servei sol·licitat o a l'ús que realitzis de la nostra pàgina web. Per a això podem analitzar els teus hàbits de navegació en Internet i podem mostrar-te publicitat relacionada amb el teu perfil de navegació.
            <br><br>
            <a class="read-more" style=" text-decoration: underline; font-weight: 600">Cookies de publicitat comportamental:</a> Són aquelles que permeten la gestió, de la forma més eficaç possible, dels espais publicitaris que, si escau, l'editor hagi inclòs en una pàgina web, aplicació o plataforma des de la qual presta el servei sol·licitat. Aquest tipus de cookies emmagatzemen informació del comportament dels visitants obtinguda a través de l'observació continuada dels seus hàbits de navegació, la qual cosa permet desenvolupar un perfil específic per mostrar avisos publicitaris en funció del mateix.
            <br><br>


            <div class="h4"><span>DESACTIVAR LES COOKIES</span></div>
            L'Usuari pot permetre, bloquejar o eliminar les cookies instal·lades en el seu equip mitjançant la configuració de les opcions del navegador instal·lat en el seu ordinador.
            <br>
            En la majoria dels navegadors web s'ofereix la possibilitat de permetre, bloquejar o eliminar les cookies instal·lades en el seu equip.
            <br>
            A continuació pots accedir a la configuració dels navegadors webs més freqüents per acceptar, instal·lar o desactivar les cookies:<br>
            <a href="https://support.google.com/chrome/answer/95647?hl=es" class="read-more">Configurar cookies a Google Chrome</a>
            <br>
            <a href="http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9" class="read-more">Configurar cookies a Microsoft Internet Explorer</a>
            <br>
            <a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we" class="read-more">Configurar cookies en Mozilla Firefox
            <br>
             <a href="https://support.apple.com/es-es/HT201265" class="read-more">Configurar cookies a Safari (Apple)

            <br><br>
            <div class="h4"><span>COOKIES DE TERCERS</span></div>
            Aquesta pàgina web utilitza serveis de tercers per recopilar informació amb finalitats estadístiques i d'ús de la web. S'usen cookies de DoubleClick per millorar la publicitat que s'inclou en el lloc web. Són utilitzades per orientar la publicitat segons el contingut que és rellevant per a un usuari, millorant així la qualitat d'experiència en l'ús del mateix.
            <br>
            En concret, usem els serveis de Google Adsense i de Google Analytics para nostres estadístiques i publicitat. Algunes cookies són essencials per al funcionament del lloc, per exemple el cercador incorporat.
            <br>
            El nostre lloc inclou altres funcionalitats proporcionades per tercers. Vostè pugues fàcilment compartir el contingut en xarxes socials com Facebook, Twitter o Google +, amb els botons que hem inclòs a aquest efecte.


            <br><br>
            <div class="h4"><span>ADVERTÈNCIA SOBRE ELIMINAR COOKIES.</span></div>
            Vostè pot eliminar i bloquejar totes les cookies d'aquest lloc, però part del lloc no funcionarà o la qualitat de la pàgina web pot veure's afectada.
            <br>
            Si té qualsevol dubte sobre la nostra política de cookies, pot contactar amb aquesta pàgina web a través dels nostres canals de Contacte.
        </div>
    </div>
</section>






    </div>

<?php $this->load->view('includes/template/footer'); ?>

<!--End of Footer Section-->
