<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Noticia extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function noticias(){
            $crud = $this->crud_function('','');    
            $crud->set_field_upload('foto','img/blog');
            $crud->unset_columns('texto');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>


