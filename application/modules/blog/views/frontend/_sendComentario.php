<div id="respond" class="comment-respond">
    <?php if(!empty($_SESSION['mensaje'])){
       echo $_SESSION['mensaje'];
       unset($_SESSION['mensaje']);
    }?>
    <h3 id="reply-title" class="comment-reply-title">Deja tu comentario
        <small>
            <a rel="nofollow" id="cancel-comment-reply-link" href="#respond" style="display:none;">Cancelar respuesta</a>
        </small>
    </h3>
    <form action="<?= base_url('blog/frontend/comentarios') ?>" method="post" id="commentform" class="comment-form">
        <p class="comment-form-author">
            <label for="author">Nombre</label>
            <input class="span5 required" name="autor" size="40" required="" type="text">
        </p>
        <p class="comment-form-email">
            <label for="email">Email</label>
            <input class="span5 required" name="email" size="60" required="" type="email">
        </p>
        <p class="comment-form-subject">
            <label for="subject">Título</label>
            <input class="span5" name="titulo" size="100" type="text">
        </p>
        <p class="comment-form-comment">
            <label for="comment">Comentario</label>
            <textarea id="comment" class="span9 span9" name="texto" cols="45" rows="8" aria-required="true"></textarea>
        </p>
        <p class="form-submit">
            <input id="submit" value="Enviar comentari" type="submit">
            <input name="blog_id" value="<?= $detail->id ?>" type="hidden">            
        </p>
    </form>
</div><!-- #respond -->