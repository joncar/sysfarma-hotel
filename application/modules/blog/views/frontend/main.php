<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb7.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Noticies</div>
                <div class="sub-title">Gaudeix de les noticies de la comarca</div>
            </div>
        </div>
        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="current"><a href="<?= site_url('blog') ?>">Noticies</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Blog Container-->
<section id="blog-section">
    <div class="inner-container container">
        <div class="post-main-container clearfix">
            <!-- Post boxes -->
            <?php foreach ($detail->result() as $d): ?>
                <div class="post-outer-box col-md-6">
                    <div class="post-box">
                        <a class="post-img-box" href="<?= $d->link ?>">
                            <img src="<?= $d->foto ?>" alt="<?= $d->titulo ?>" class="post-img">
                        </a>
                        <div class="post-b-sec">
                            <div class="post-title-box">
                                <a href="<?= $d->link ?>" class="post-title"><?= $d->titulo ?></a>
                            </div>
                            <div class="post-meta clearfix">
                                <div class="post-date"><i class="fa fa-calendar"></i> <?= strftime("%d , %b , %Y",strtotime($d->fecha)) ?></div>
                                <div class="post-author"><i class="fa fa-edit"></i> By : <a href="#"><?= $d->user ?></a></div>                                
<!--                                <div class="post-comment"><i class="fa fa-comments-o"></i> --><?//= $d->comentarios ?><!-- <a href="#">Comentaris</a></div>-->
                            </div>
                            <div class="post-desc">
                                <?= cortar_palabras($d->texto,155) ?>
                            </div>
                            <div class="read-more-container">
                                <a href="<?= $d->link ?>" class="btn btn-default read-more">Veure Més</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>

        </div>
        <!-- Pagination -->
        <div class="pagination-box">
            <ul class="list-inline">                
                <li><a href="<?= base_url('blog') ?>?page=1"><i class="fa fa-angle-double-left"></i></a></li>
                <?php for ($i = 1; $i <= $total_pages; $i++): ?>
                    <li><a href="<?= base_url('blog') ?>?page=<?= $i ?>"><?= $i ?></a></li>
                <?php endfor ?>
                <li><a href="<?= base_url('blog') ?>?page=<?= !empty($_GET['page']) ? $_GET['page'] + 1 : 2 ?>"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </div>
        <!-- End of Pagination -->
    </div>
</section>
<!--End of Blog Container-->
