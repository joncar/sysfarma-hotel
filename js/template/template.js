"use strict";
jQuery(document).ready(function () {

	// Clone the main menu to the mobile menu
	jQuery('#main-menu').clone().removeClass().appendTo('#mobile-menu-container');

	// Enable Menu menu toggling
	jQuery('#main-menu-handle').on('click', function () {
		jQuery(this).toggleClass('active').parents('#main-header').toggleClass('mobile-menu-active');
		jQuery('#mobile-menu-container').fadeToggle();
	});

	var sortHandle = jQuery('.sort-handle');
	if (sortHandle.length > 0) {
		sortHandle.on('click', function () {
			jQuery(this).next('ul').slideToggle();
		});
	}


	jQuery('.animated-box').on('inview', function (event, isInView) {
		if (isInView) {
			var _this          = jQuery(this),
				animationType  = _this.data('animation'),
				animationDelay = _this.data('delay');
			animationDelay ? setTimeout(function () {
					_this.addClass(animationType);
				}, animationDelay) : _this.addClass(animationType);
		}
	});

	if (jQuery.isFunction(jQuery.fn.select2)) {
		// Change all the select boxes to select2
		jQuery("select:not(.disable-select2)").select2({
			minimumResultsForSearch: 10
		});
	}

	jQuery('[data-bg-img]').each(function () {
		var _this = jQuery(this);
		_this.css('background-image', 'url(' + _this.data('bg-img') + ')');
	});

	var mainSlider = jQuery("#main-slider, #event-slider, #room-slider");
	if (mainSlider.length > 0 && jQuery.fn.owlCarousel) {
		// Main Slider
		mainSlider.owlCarousel({
			navigation:     !0,
			singleItem:     !0,
			addClassActive: !0,
			autoPlay:       !0,
			pagination:     !1,
			navigationText: ['<span>Prev</span>', '<span>Next</span>']
		});
	}


	// Booking datepicker
	jQuery.fn.datepicker && jQuery("#main-availability-form, #room-booking-form, #room-information-form").find('.input-daterange').datepicker({
		format:    "dd-mm-yyyy",
		autoclose: true,
		startDate: new Date(),
                weekStart: 1,
                beforeShowDay: function(date){                    
                    if(typeof(reservas)!=='undefined'){
                        var da = date;
                        var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                        var d = da.getFullYear()+'-'+(da.getMonth()+1)+'-'+dia;
                        return reservas.indexOf(d)>=0?{enabled:false}:date;
                    }
                    return date;
                },
                beforeShowMonth:function(date){
                    var months = [''];
                    var month = date.getMonth()+1;
                    return 'ENERO';
                }
	});

	var luxuryRooms = jQuery('#luxury-rooms');
	if (luxuryRooms.length > 0) {
		luxuryRooms.children('.room-boxes').each(function () {
			var _this = jQuery(this).children('.inner-container');
			_this.css('background-image', 'url(' + _this.data('bg') + ')');
		});
	}

	var mainImgContainer = jQuery(".image-main-box");
	// Enable the magnificPopup
	jQuery.fn.magnificPopup && mainImgContainer.magnificPopup({
		delegate:     '.item:not(":hidden") .more-details',
		type:         'image',
		removalDelay: 600,
		mainClass:    'mfp-fade',
		gallery:      {
			enabled:            true,
			navigateByImgClick: true,
			preload:            [0, 1] // Will preload 0 - before current, and 1 after the current image
		},
		image:        {
			titleSrc: 'data-title',
			tError:   '<a href="%url%">The image #%curr%</a> could not be loaded.'
		}
	});


	jQuery.fn.magnificPopup && jQuery('.video-url').magnificPopup({
		disableOn:       700,
		type:            'iframe',
		mainClass:       'mfp-fade',
		removalDelay:    600,
		preloader:       false,
		fixedContentPos: false
	});

	// Enable isotop for gallery
	if (jQuery.fn.magnificPopup && jQuery.fn.imagesLoaded) {
            var filtro = mainImgContainer.data('default')?'.'+mainImgContainer.data('default')+'cat':'.main';            
		mainImgContainer.isotope({
			transitionDuration: "0.7s",
                        filter: filtro
		});
		mainImgContainer.imagesLoaded(function () {
			mainImgContainer.isotope("layout");
			jQuery(".sort-section-container").on("click", "a", function (e) {
				e.preventDefault();
				jQuery(".sort-section-container a").removeClass("active");
				jQuery(this).addClass("active");
				var filterValue = jQuery(this).attr("data-filter");
				mainImgContainer.isotope({filter: filterValue});
			});
		});
	}

	// Enable isotop for Events
	jQuery.fn.imagesLoaded && jQuery(".event-main-box, .dishes-main-box").imagesLoaded(function () {
		var eventContainer = jQuery(".event-main-box, .dishes-main-box");
		eventContainer.isotope({
			transitionDuration: "0.7s"
		});
		eventContainer.imagesLoaded(function () {
			eventContainer.isotope("layout");
			jQuery(".sort-section-container").on("click", "a", function (e) {
				e.preventDefault();
				jQuery(".sort-section-container a").removeClass("active");
				jQuery(this).addClass("active");
				var filterValue = jQuery(this).attr("data-filter");
				eventContainer.isotope({filter: filterValue});
			});
		})
                
                jQuery.fn.magnificPopup && eventContainer.magnificPopup({
                        delegate:     '.item:not(":hidden") .more-details',
                        type:         'image',
                        removalDelay: 600,
                        mainClass:    'mfp-fade',
                        gallery:      {
                                enabled:            true,
                                navigateByImgClick: true,
                                preload:            [0, 1] // Will preload 0 - before current, and 1 after the current image
                        },
                        image:        {
                                titleSrc: 'data-title',
                                tError:   '<a href="%url%">The image #%curr%</a> could not be loaded.'
                        }
                });
	});

	// Enable isotop for Guest Book
	if (jQuery.fn.isotope && jQuery("#guest-book").length > 0) {

		jQuery('.inner-container', "#guest-book").isotope({
			itemSelector: ".guest-book-item"
		});
	}

	var bodyElement = jQuery("body");
	// Enable isotop for Guest Book
	if (jQuery.fn.isotope && bodyElement.hasClass('blog-masonry')) {
		var eventContainer = jQuery('.post-main-container', "#blog-section");
		eventContainer.isotope({
			transitionDuration: "0.7s"
		});
		eventContainer.imagesLoaded(function () {
			itemSelector : ".post-outer-box"
		});
	}

	// Testimonials slider
	jQuery.fn.owlCarousel && jQuery('.owl-carousel', "#testimonials-section").owlCarousel({
		navigation:     !0,
		singleItem:     !0,
		addClassActive: !0,
		pagination:     !1,
		navigationText: ['<span>Prev</span>', '<span>Next</span>'],
		autoPlay:       8000
	});

	jQuery.fn.owlCarousel && jQuery("#services-box").owlCarousel({
		items:             3,
		itemsTablet:       [980, 2],
		itemsMobile:       [480, 1],
		navigation:        !1,
		pagination:        !0,
		paginationNumbers: !0
	});


	//	 Booking Page js

	if (bodyElement.hasClass('booking')) {
		var mainBookingForm    = jQuery('#room-information-form'),
			roomFieldContainer = mainBookingForm.find('.room-field-container');

		mainBookingForm.find('.room-count').on('change', function () {
			var _thisVal = 1;
			roomFieldContainer.empty();
			if (_thisVal) {
				for (var i = 1; i <= _thisVal; i++) {
					var cloneTmpl = jQuery('#room-field-tmpl').clone();
					cloneTmpl.attr('id', '');
					cloneTmpl.html(function (j, cloneTmplOld) {
						return cloneTmplOld.replace('{{id}}', i);
					});
					cloneTmpl.appendTo(roomFieldContainer);
				}
			}

			mainBookingForm.find('select').select2({
				minimumResultsForSearch: 10
			});
                        
                        //Buscar nuevas limitaciones
                        jQuery.post('habitacion/reservaFrontend/iniciar_reserva/'+parseInt(jQuery(this).val()),{},function(data){                            
                            reservas = JSON.parse(data);                            
                            init_date_picker(bookingInlineDatepicker,mainBookingForm);
                        });
		});
                
                var cloneTmpl = jQuery('#room-field-tmpl').clone();
                cloneTmpl.attr('id', '');
                cloneTmpl.html(function (j, cloneTmplOld) {
                        return cloneTmplOld.replace('{{id}}',1);
                });
                cloneTmpl.appendTo(roomFieldContainer);
                mainBookingForm.find('select').select2({
                        minimumResultsForSearch: 10
                });

		var bookingInlineDatepicker = jQuery('#booking-date-range-inline');
                init_date_picker(bookingInlineDatepicker,mainBookingForm);
                
		

		jQuery('.price-breakdown').magnificPopup({
			type:         'inline',
			preloader:    false,
			mainClass:    'mfp-fade',
			removalDelay: 600
		});

		jQuery('input[name="payment-method"]').on('change', function () {
			var _this = jQuery(this);
			if (_this.val() == 1) {
				_this.parents('.payment-method').next('.deposit-price').slideUp();
			} else {
				_this.parents('.payment-method').next('.deposit-price').slideDown();
			}
		})
	}

	// Google Map
	function initialize() {
                var mapBox     = document.getElementById("google-map");
                if(mapBox!==null){
                var lat = mapBox.getAttribute("data-lat");
                var lon = mapBox.getAttribute("data-lon");
		var myLatLng   = new google.maps.LatLng(lat,lon);
		var mapOptions = {
			zoom:               15,
			center:             myLatLng,
			// This is where you would paste any style found on Snazzy Maps.
			styles:             [{
				"featureType": "all",
				"elementType": "labels.text.fill",
				"stylers":     [{"saturation": 36}, {"color": "#000000"}, {"lightness": 40}]
			}, {
				"featureType": "all",
				"elementType": "labels.text.stroke",
				"stylers":     [{"visibility": "on"}, {"color": "#000000"}, {"lightness": 16}]
			}, {
				"featureType": "all",
				"elementType": "labels.icon",
				"stylers":     [{"visibility": "off"}]
			}, {
				"featureType": "administrative",
				"elementType": "geometry.fill",
				"stylers":     [{"color": "#000000"}, {"lightness": 20}]
			}, {
				"featureType": "administrative",
				"elementType": "geometry.stroke",
				"stylers":     [{"color": "#000000"}, {"lightness": 17}, {"weight": 1.2}]
			}, {
				"featureType": "landscape",
				"elementType": "geometry",
				"stylers":     [{"color": "#000000"}, {"lightness": 20}]
			}, {
				"featureType": "poi",
				"elementType": "geometry",
				"stylers":     [{"color": "#000000"}, {"lightness": 21}]
			}, {
				"featureType": "road.highway",
				"elementType": "geometry.fill",
				"stylers":     [{"color": "#000000"}, {"lightness": 17}]
			}, {
				"featureType": "road.highway",
				"elementType": "geometry.stroke",
				"stylers":     [{"color": "#000000"}, {"lightness": 29}, {"weight": 0.2}]
			}, {
				"featureType": "road.arterial",
				"elementType": "geometry",
				"stylers":     [{"color": "#000000"}, {"lightness": 18}]
			}, {
				"featureType": "road.local",
				"elementType": "geometry",
				"stylers":     [{"color": "#000000"}, {"lightness": 16}]
			}, {
				"featureType": "transit",
				"elementType": "geometry",
				"stylers":     [{"color": "#000000"}, {"lightness": 19}]
			}, {
				"featureType": "water",
				"elementType": "geometry",
				"stylers":     [{"color": "#000000"}, {"lightness": 17}]
			}],
			// Extra options
			scrollwheel:        false,
			mapTypeControl:     false,
			panControl:         false,
			zoomControlOptions: {
				style:    google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.LEFT_BOTTOM
			}
		};
		
		var map        = new google.maps.Map(mapBox, mapOptions);

		var image = mapBox.getAttribute("data-marker");

		var marker = new google.maps.Marker({
			position: myLatLng,
			map:      map,
			icon:     image,
                        title: 'Mostrar en google maps'
		});
                
                google.maps.event.addDomListener(marker,'click',function(){
                    document.location.href="https://www.google.co.ve/maps/place/41%C2%B033'35.4%22N+1%C2%B031'26.5%22E/@41.5598677,1.5223683,17z/data=!4m5!3m4!1s0x0:0x0!8m2!3d41.5598196!4d1.5240205?hl=es-419";
                });
            }
	}

	if (typeof google != 'undefined') {
		google.maps.event.addDomListener(window, "load", initialize);
	}

	var restaurantMenus = jQuery('#restaurant-menus');
	if (restaurantMenus.length > 0) {
		var menuTabs   = restaurantMenus.find('.tab-box'),
			tabContent = restaurantMenus.find('.tab-pane');

		menuTabs.on('click', function (e) {
			e.preventDefault();
			var _this          = jQuery(this),
				target         = _this.addClass('active').attr('href').slice(1),
				targetSelector = jQuery('#' + target),
				bgImg          = targetSelector.data('img-name');

			_this.siblings().removeClass('active');
			tabContent.removeClass('active');
			targetSelector.addClass('active');

			tabContent.parent('.tab-content').css('background-image', 'url(' + bgImg + ')');
		})
	}

	// Send contact information
	var contactFormBox = jQuery('#contact-form-box');
	if (contactFormBox.length > 0) {
		contactFormBox.on('submit', function (e) {
			e.preventDefault();

			var _this = jQuery(this),
				messageBox = _this.find('.message-box');

			jQuery.ajax({
				type:     'POST',
				dataType: 'json',
				url:      '../forms/contact.php',
				data:     {
					'name':    _this.find('#contact-name').val(),
					'phone':   _this.find('#contact-phone').val(),
					'email':   _this.find('#contact-email').val(),
					'message': _this.find('#contact-message').val()
				},
				success:  function (data) {
					if(data.status == true){
						messageBox.addClass('alert alert-success').text(data.message);
					} else {
						messageBox.addClass('alert alert-danger').text(data.message);
					}
				}
			});
		})
	}

});
var mainHeader = jQuery("#main-header");
jQuery(window).on('scroll', function () {
	jQuery(document).scrollTop() > 30 && mainHeader.addClass("sticky");
	jQuery(document).scrollTop() < 30 && mainHeader.removeClass("sticky");
});

function init_date_picker(bookingInlineDatepicker,mainBookingForm){
    bookingInlineDatepicker.datepicker('remove');
    var checkInDate             = null,
        checkOutDate            = null,
        oneDay                  = 24 * 60 * 60 * 1000;
    bookingInlineDatepicker.datepicker({
            format:    "dd-mm-yyyy",
            autoclose: true,
            startDate: new Date(),            
            weekStart: 1,
            inputs:    jQuery('#booking-date-range-inline .check-in, #booking-date-range-inline .check-out'),
            beforeShowDay: function(date){
                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var d = da.getFullYear()+'-'+(da.getMonth()+1)+'-'+dia;
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            },
            beforeShowMonth:function(date){
                var months = [''];
                var month = date.getMonth()+1;
                return 'ENERO';
            }
    }).on('changeDate', function (e) {
            var newDate    = new Date(e.date),
                    newDateStr = ("0" + newDate.getDate()).slice(-2) + '-' + ("0" + (newDate.getMonth() + 1)).slice(-2) + '-' + newDate.getFullYear();
            if (e.target.className.search('check-in') < 0) {
                    checkInDate = newDate.getTime();
                    mainBookingForm.find('.check-out').children('input').val(newDateStr);
                    mainBookingForm.find('.check-out').find('.value').text(newDateStr);
            }
            else {
                    checkOutDate = newDate.getTime();
                    mainBookingForm.find('.check-in').children('input').val(newDateStr);
                    mainBookingForm.find('.check-in').find('.value').text(newDateStr);
            }
            var diffDays = Math.round(Math.abs((checkInDate - checkOutDate) / (oneDay)));
            mainBookingForm.find('.duration').find('.value').text((diffDays >= 1 ? diffDays + ' Nits' : diffDays + ' Nits'));
            if(typeof(max_nits)!=='undefined' && diffDays>max_nits){
                $("#messagecontainer").html('<div class="alert alert-danger">Disculpe, pero la oferta solo es válida para '+max_nits+' Noches</div>');
                $("#realizarreserva").attr('disabled',true);
            }else if(diffDays==0){
                $("#messagecontainer").html('');
                $("#realizarreserva").attr('disabled',true);
            }else{
                $("#messagecontainer").html('');
                $("#realizarreserva").attr('disabled',false);
            }
    });
}
